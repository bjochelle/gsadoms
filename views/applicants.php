<style type="text/css">
    
    @media print{
        @page{
            font-size: 25px !important;
        }
       
    }
</style><div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Applicants</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                   <div class='row'>
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class='row'>
                                        <div class='col-md-6'>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Type: </div>
                                                    <select id='sortingType' class='form-control' onchange="applicantRegDate()">=
                                                        <option value='N'> New </option>
                                                        <option value='A'> Archived </option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- <div class='col-md-6'>
                                            <div class="form-group">
                                                <div class="input-group">
                                                    <div class="input-group-addon">Register Date:</div>
                                                    <input type="date" id="regDate" class="form-control" value='<?php echo date("Y-m-d", strtotime(getCurrentDate()))?>' onchange='applicantRegDate()'>
                                                </div>
                                            </div>
                                        </div> -->
                                        <div class='col-md-6'>
                                            <button class='btn btn-sm btn-danger' onclick="archiveApplicants()"><span class='fa fa-archive'></span> Archive </button>
                                            <button class='btn btn-sm btn-success' onclick="viewScheduledExam()"><span class='fa fa-eye'></span> Scheduled Exam</button>
                                            <button class='btn btn-sm btn-primary' onclick="setSchModal()"><span class='fa fa-edit'></span> Set Schedule</button>
                                            <!-- <button style='margin-top: 5px;' id='btn_enroll' class='btn btn-sm btn-primary' onclick="enrollApplicant()"><span class='fa fa-registered'></span> Enrolled </button> -->
                                        </div>
                                        <div class='col-md-12' style='margin-top:10px;overflow: auto;'>
                                            <table id='students' class="table table-bordered table-hover" style='margin-top:10px;'>
                                                <thead style='background-color: #343940;color: white;'>
                                                    <tr>
                                                        <th></th>
                                                        <th>#</th>
                                                        <th>APPLICANT NAME</th>
                                                       <!--  <th>ADDRESS</th>
                                                        <th>CONTACT #</th>
                                                        <th>REGISTER DATE</th> -->
                                                        <th>EXAM DATE</th>
                                                        <th>SCORE</th>
                                                        <th>STANINE</th>
                                                        <th>STATUS</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>     
                                </div>
                            </div>
                        </div>
                   </div>
                </div>
            </div>
        </div>   
    </div>
</div>


<script>
    $(document).ready( function(){
        var sortingType = $("#sortingType").val();
        applicantRegDate(sortingType);
        examScheduleDate();
    });
    function enrollApplicant(){
        var count_checked = $('input[name="checkbox_std"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        applicant_array = [];
        if(count_checked == ''){
            alert("Please Select Student");
        }else{
            var conf = confirm("Are you sure you want to enroll selected applicants?");
            if(conf == true){
          
                    
                    $.post("ajax/applicantChecker.php", {
                        applicant_array: count_checked
                    }, function(data){
                        if(data > 0){
                            alert("Selected Applicant can't be enrolled.");
                        }else{
                            enrolledApplicant(count_checked);
                        }
                    })
                
            }
        }
        
        
    }
    function enrolledApplicant(count_checked){
        var sortingType = $("#sortingType").val();
        $("#btn_enroll").prop("disabled", true);
        $("#btn_enroll").html("<span class='fa fa-spin fa-spinner'></span> Loading");
       array_applicant = [];
       if(count_checked == ''){
        alert("Please select Student");
       }else{
            $.post("ajax/enrollApplicant.php", {
                array_applicant: count_checked
            }, function(data){
                if(data > 0){
                    alert("Applicant Successfully added as Student");
                }else{
                    alert("Error")
                }
                applicantLists(sortingType);
                $("#btn_enroll").prop("disabled", false);
                $("#btn_enroll").html("<span class='fa fa-registered'></span> Enroll");
            });
       }
       
    }
    function examScheduleDate(){
        var curdate = '<?php echo date("Y-m-d", strtotime(getCurrentDate()))?>';
        var seldate = $("#examDate").val();
        //if(seldate < curdate){
           // alert("Please Choose Currnet Date onwards");
       // }else{
            applicantLists_stanine(seldate);
       // }
    }
    function stnnVal(applicant_id){
        var score = $("#stnnScore"+applicant_id).val();
        var stanine = $("#stnnReturn"+applicant_id).val();

        $("#valuetosave"+applicant_id).val(applicant_id+"-"+score+"-"+stanine);

    }
    function stnnValResult(applicant_id){
        var score = $("#stnnScore"+applicant_id).val();
        var stanine = $("#stnnReturn"+applicant_id).val();

        $("#valuetosave"+applicant_id).val(applicant_id+"-"+score+"-"+stanine);
    }
    function archiveApplicants(){
        var count_checked = $('input[name="checkbox_std"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        applicant_array = [];
        if(count_checked == ''){
            alert("Please Select Student")
        }else{
            var sortingType = $("#sortingType").val();
            var conf = confirm("Are you sure you want to archive selected applicants?");
            if(conf == true){
                $.post("ajax/archive_applicants.php", {
                    applicant_array: count_checked
                }, function(data){
                    if(data > 0){
                        alert("Successfully Archived");
                    }else{
                        alert("Failed");
                    }
                    applicantLists(sortingType);
                })
            }else{

            }
        }
        
        
    }
    function saveExamScore(){
        var list_scores = $('input[name="valuetosave"]').map(function() {
                                        return this.value;
                                    }).get();

        array_list = [];
        //alert(list_scores)
        var sortingType = $("#sortingType").val();
        $("#btn_save_exam").prop("disabled", true);
        $("#btn_save_exam").html("<span class='fas fa-spin fa-spinner'></span> Loading...");
        $.post("ajax/save_applicants_exam.php", {
            array_list: list_scores
        }, function(data){
            if(data > 0){
                $("#selModal").modal('hide');
                alert("Successfully Saved");
            }else{
                alert("Error");
            }
            applicantLists(sortingType);
        });
    }

    function stanine_Checker(applicant_id, score){
        $.post("ajax/stanine_checker.php", {
            applicant_id: applicant_id,
            score: score
        }, function(data){
            $("#stnnResult"+applicant_id).html(data);
        });
    }   
    function applicantRegDate(){
        var sortingType = $("#sortingType").val();
        applicantLists(sortingType);

    }
    function setSchModal(){
        var count_checked = $('input[name="checkbox_std"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        if(count_checked == ''){
            alert("Please select student.");
        }else{
            $("#setSchModal").modal('show');
        }
    }
    function viewScheduledExam(){
        $("#selModal").modal('show');
    }
    function setSCH(){
        var count_checked = $('input[name="checkbox_std"]:checked').map(function() {
                                return this.value;
                            }).get();
        checked_array = [];
        var sortingType = $("#sortingType").val();
        var schDate = $("#schDate").val();
        var schTime = $("#schTime").val();
        var regDate = $("#regDate").val();
        if(schDate == '' || schTime == ''){
            alert("Please Fill All Fields.");
        }else{
            $("#btn_set").prop("disabled", true);
            $("#btn_set").html("<span class='fas fa-spin fa-spinner'></span> Loading");
            $.post("ajax/setSchedule.php", {
                checked_array: count_checked,
                schDate: schDate,
                schTime: schTime
            }, function(data){
                $("#setSchModal").modal('hide');
                if(data > 0){
                    alert("Success");
                }else{
                    alert("Error");
                }
                applicantLists(sortingType);
            });
        }
    }
    function applicantLists(sortingType){
        $("#students").DataTable().destroy();
        $('#students').dataTable({
        "processing":true,
        "ajax":{
          "method":"POST",
          "url":"ajax/datatables/applicants.php",
          "data":{
            sortingType: sortingType
          },
          "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    //if(row.stat == 1s){
                        return "<input type='checkbox' name='checkbox_std' value='"+row.applicant_id+"'>";
                    // }else{
                    //     return "";
                    // }
                    		
                }
            },
            {
                "data":"count"
            },
            {
                "data":"fullname"
            },
            // {
            //     "data":"address"
            // },
            // {
            //     "data":"contact"
            // },
            // {
            //     "data":"date_reg"
            // },
            {
                "data":"date_exam"
            },
            {
                "data":"score"
            },
            {
                "data":"stanine"
            },
            {
                "data":"status"
            }
            
        ]   
        });
    }
    function applicantLists_stanine(examDate){
        $("#students_stnn").DataTable().destroy();
        $('#students_stnn').dataTable({
        "processing":true,
        "ajax":{
            "method":"POST",
            "url":"ajax/datatables/applicant_stanine.php",
            "data":{
              examDate:examDate
            },
            "dataSrc":"data"
        },
        "columns":[
            {
                "data":"count"
            },
            {
                "data":"fullname"
            },
            {
                "data":"inptScr"
            },
            {
                "data":"stanineVal"
            }
            
        ]   
        });
    }
</script>