<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            NOTIFICATIONS
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <div class='row'>
                        <div class='col-md-12'>
                            <button class='btn btn-sm btn-success pull-right' onclick='markasRead()' id='markasRead'><span class='fa fa-check-circle'></span> Mark as Read</button>
                        </div>
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='notifications' class="table table-bordered table-hover" style='margin-top:10px;'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th></th>
                                        <th>#</th>
                                        <th>CONTENT</th>
                                        <th>ADDED BY</th>
                                        <th>DATE</th>
                                        <th>STATUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                   </div>
                </div>
            </div>
        </div> 
    </div>
</div>
<script>
$(document).ready( function(){
    notifLists();
});
function markasRead(){
    var count_checked = $('input[name="checkbox_notif"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        $.post("ajax/read_notif.php", {
            checked_array: count_checked
        }, function(data){
            if(data > 0){
                alert("Successfully Marked as read")
                
            }else{
                alert("Failed")
            }

            notifLists();
        })
}
function notifLists(){
    $("#notifications").DataTable().destroy();
    $('#notifications').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/notifications.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "mRender": function(data,type,row){
                return "<input type='checkbox' name='checkbox_notif' value='"+row.notif_id+"'>";		
            }
        },
        {
            "data":"count"
        },
        {
            "data":"notif_content"
        },
        {
            "data":"added_by"
        },
        {
            "data":"notif_date"
        },
        {
            "data":"status"
        }
        
    ]   
    });
}
</script>