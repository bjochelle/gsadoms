<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Track and Strand</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                   <div class='row'>
                        <div class='col-md-12'>
                            <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target="#TSModal"><span class='fa fa-plus-circle'></span> Add</button>
                            <button class='btn btn-sm btn-danger pull-right' onclick='deletePrgm()' id='deletePrgm'><span class='fa fa-trash'></span> Delete</button>
                        </div>
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='trackAndstrand' class="table table-bordered table-hover" style='margin-top:10px;'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th></th>
                                        <th>#</th>
                                        <th></th>
                                        <th>NAME</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                   </div>
                </div>
            </div>
        </div>   
    </div>
</div>


<script>
    $(document).ready( function(){
        trackAndstrandLists();
    });
    function deletePrgm(){
        var count_checked = $('input[name="checkbox_appType"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        var action = 'delete';
        var conf = confirm("Are you sure you want to delete this?");
        if(conf == true){
            $("#deletePrgm").prop("disabled", true);
            $("#deletePrgm").html("<span class='fas fa-spin fa-spinner'></span> Loading");
            $.post("ajax/apptype_functions.php", {
                action: action,
                checked_array: count_checked
            }, function(data){
                if(data > 0){
                    alert("Successfully Deleted");
                }else{
                    alert("Error");
                }
                applicantTypeLists();
                $("#deletePrgm").prop("disabled", false);
                $("#deletePrgm").html("<span class='fas fa-trash'></span> Delete");
            });
        }
    
    }
    function viewRecord(TSid, TSName){
        $("#TSModalU").modal('show');
        $("#tAsName").val(TSName);
        $("#tAsID").val(TSid);
    }
    function updateTS(){
        var TSName = $("#tAsName").val();
        var TSId = $("#tAsID").val();
        var action = 'update';
        $("#btn_update_ts").prop("disabled", true);
        $("#btn_update_ts").html("<span class='fas fa-spin fa-spinner'></span> Loading");
        $.post("ajax/track_strand_functions.php", {
            TSName: TSName,
            TSId: TSId,
            action: action
        }, function(data){
            $("#TSModalU").modal('hide');
            if(data == 1){
                alert("Successfully Updated");
            }else{
                alert("Error");
            }
            trackAndstrandLists();
            $("#btn_update_ts").prop("disabled", false);
            $("#btn_update_ts").html("<span class='fas fa-check-circle'></span> Continue");
        });
    }
    function addNewTS(){
        var tsName = $("#ts_name").val();
        var action = 'add';
        $("#btn_add_ts").prop('disabled', true);
        $("#btn_add_ts").html("<span class='fas fa-spin fa-spinner'></span> Loading ");
        $.post("ajax/track_strand_functions.php", {
            tsName: tsName,
            action: action
        }, function(data){
            $("#TSModal ").modal('hide');
            if(data == 1){
                alert("Success");
            }else{
                alert("Failed");
            }
            $("#btn_add_ts").prop('disabled', false);
            $("#btn_add_ts").html("<span class='fas fa-check-circle'></span> Continue ");
            $("#ts_name").val("");
            trackAndstrandLists();
        }); 
    }
    function trackAndstrandLists(){
        $("#trackAndstrand").DataTable().destroy();
        $('#trackAndstrand').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/track_and_strand_list.php",
            "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_appType' value='"+row.ts_id+"'>";		
                }
            },
            {
                "data":"count"
            },
            {
                "data":"action"
            },
            {
                "data":"tsName"
            }
            
        ]   
        });
    }
</script>