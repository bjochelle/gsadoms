<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1"> Qualified Applicants</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <div class='row'>
                        <input type="hidden" value='<?php echo $programID ?>' id='prgmID' name="">
                        <input type="hidden" value='<?php echo $user_id ?>' id='userid' name="">
                       <!--   <div class='col-md-10'>
                            <div class="form-group">
                                <div class="input-group"  style="width: 50%">
                                    <div class="input-group-addon">Type: </div>
                                       <select id='sortingType' class='form-control' onchange="sortingType()">=
                                          <option value='A'> All </option>
                                          <option value='P'> Passed </option>
                                          <option value='F'> Failed </option>
                                          <option value='AC'> Archived </option>
                                       </select>
                                </div>
                            </div>
                        </div> -->
                        <div class='col-md-12' style='margin-top:10px;'>

                            <table id='students' class="table table-bordered table-hover" style='margin-top:10px;'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th>#</th>
                                        <th>NAME</th>
                                        <th>TRACK AND STRAND</th>
                                        <th>SCORE</th>
                                        <th>STANINE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>
<script>
$(document).ready( function(){
    var prgmID = $("#prgmID").val();
    studentList(prgmID);
});
// function sortingType(){
//     var sortingType = $("#sortingType").val();
//     var prgmID = $("#prgmID").val();
//     studentList(sortingType, prgmID);
// }
function studentList(course){
    $("#students").DataTable().destroy();
    $('#students').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/applicants_per_program.php",
        "dataSrc":"data",
        "data":{
          course: course
        },
        "type": "POST"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"fullname"
        },
        {
            "data":"ts"
        },
        {
            "data":"score"
        },
        {
            "data":"stanineVal"
        }
        
    ]   
    });
}
</script>