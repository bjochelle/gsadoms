<?php 
$st_id = $_GET['stdID'];
$stProfile = mysql_fetch_array(mysql_query("SELECT * FROM tbl_students WHERE student_id = '$st_id'"));
$header = family_head();
$body = family_body();

$deceasedWho = $stProfile['deceased'];
if($deceasedWho == 'M'){
    $date = $stProfile['date_deceased'];
    $cause = $stProfile['cause_deceased'];
}else{
    $date = "";
    $cause = "";
}

if($deceasedWho == 'F'){
    $Fdate = $stProfile['date_deceased'];
    $Fcause = $stProfile['cause_deceased'];
}else{
    $Fdate = "";
    $Fcause = "";
}

$getMotherData = mysql_fetch_array(mysql_query("SELECT * FROM tbl_student_family_background WHERE student_id = '$st_id' AND m_f = 1"));
$getFatherData = mysql_fetch_array(mysql_query("SELECT * FROM tbl_student_family_background WHERE student_id = '$st_id' AND m_f = 2"));

$getAoneData = mysql_fetch_array(mysql_query("SELECT * FROM tbl_student_interest_hobbies WHERE student_id = '$st_id' AND cat = 'A' AND `number` = '1'"));
$getAtwoData = mysql_fetch_array(mysql_query("SELECT * FROM tbl_student_interest_hobbies WHERE student_id = '$st_id' AND cat = 'A' AND `number` = '2'"));
$getAthreeData = mysql_fetch_array(mysql_query("SELECT * FROM tbl_student_interest_hobbies WHERE student_id = '$st_id' AND cat = 'A' AND `number` = '3'"));

$getE1Data = mysql_fetch_array(mysql_query("SELECT * FROM tbl_student_interest_hobbies WHERE student_id = '$st_id' AND cat = 'E' AND `number` = '1'"));
?>
<style>
.bootstrap-tagsinput .tag {
    margin-right: 2px;
    color: white;
    background-color: #4fc580;
    border-radius: 5px;
}
.bootstrap-tagsinput {
    background-color: #fff;
    border: 1px solid #ccc;
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    display: inline-block;
    padding: 4px 6px;
    color: #555;
    vertical-align: middle;
    border-radius: 4px;
    min-width: 100%;
    line-height: 22px;
    cursor: text;
}

</style>
<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1"> Student Profile</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                   <div class='row'>
                   <input type='hidden' id='stid' value='<?php echo $st_id ?>'>
                   <div class='col-md-12'>
                        <img id="img_wrap" alt='Recent 2x2 Picture' class="previewImage01 image-wrap" src="assets/images/<?php echo $stProfile['avatar']?>" style="object-fit: cover;border:3px solid #b7b1b1;width:2in;height:2in;float: right;">
                   </div>
                   <div class='col-md-12'><label for="" style='font-weight: bolder;'> I. Personal Information: </label></div>
                   
                   <div class='col-md-4' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Firstname: </div>
                                    <input type="text" class='form-control' name="fname" id='fname' value="<?php echo $stProfile['student_fname']?>">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-4' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Middlename: </div>
                                    <input type="text" class='form-control' name="mname" id='mname' value="<?php echo $stProfile['student_mname']?>">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-4' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Lastname: </div>
                                    <input type="text" class='form-control' name="lname" id='lname' value="<?php echo $stProfile['student_lname']?>">
                            </div>
                        </div>
                    </div>
                   
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Course, Year and Section: </div>
                                    <select id='program' class='form-control'>
                                        <option value=""> &mdash; Select Course &mdash; </option>
                                        <?php 
                                            $course = mysql_query("SELECT * FROM tbl_program");
                                            while($course_row = mysql_fetch_array($course)){
                                                $selected = ($stProfile['course'] == $course_row['program_id'])?"selected":"";
                                                echo "<option $selected value='".$course_row['program_id']."'>".$course_row['program_name']."</option>";
                                            }
                                        ?>
                                    </select>
                                    <select id='year_level' class='form-control' onchange="getSections()">
                                        <option value=""> &mdash; Select Year &mdash;</option>
                                        <option <?php echo ($stProfile['level'] == '1')?"selected":""; ?> value='1'> First Year </option>
                                        <option <?php echo ($stProfile['level'] == '2')?"selected":""; ?> value='2'> Second Year</option>
                                        <option <?php echo ($stProfile['level'] == '3')?"selected":""; ?> value='3'> Third Year</option>
                                        <option <?php echo ($stProfile['level'] == '4')?"selected":""; ?> value='4'> Fourth Year</option>
                                        <option <?php echo ($stProfile['level'] == '5')?"selected":""; ?> value='5'> Fifth Year</option>
                                    </select>
                                    <input type="text" name='section' id='section' class='form-control' placeholder='Section' value='<?php echo $stProfile['section']?>'>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-5'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Sex at Birth: </div>
                                <div class="input-group-addon"> <input type="checkbox" id="stSex" placeholder='' name="stSex" class="stSex" <?php echo ($stProfile['sex'] == 'M')?"checked":""?> value='M'> Male </div>
                                <div class="input-group-addon"> <input type="checkbox" id="stSex" placeholder='' name="stSex" class="stSex" <?php echo ($stProfile['sex'] == 'F')?"checked":""?> value='F'> Female </div>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-2'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Age: </div>
                                    <input type="number" class='form-control' name="stAge" id='stAge' value="<?php echo $stProfile['age']?>">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-4'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Contact #: </div>
                                    <input type="text" class='form-control' name="stcontact" id='stcontact' value="<?php echo $stProfile['contact_num']?>">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-6'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Civil Status: </div>
                                    <select name="civil_stat" id="civil_stat" class='form-control'>
                                            <option value="">&mdash; Please Choose &mdash; </option>
                                            <option <?php echo ($stProfile['civil_stat'] == 'Single')?"selected":""?> value="Single">Single</option>
                                            <option <?php echo ($stProfile['civil_stat'] == 'Married')?"selected":""?> value="Married">Married</option>
                                            <option <?php echo ($stProfile['civil_stat'] == 'Widowed')?"selected":""?> value="Widowed">Widowed</option>
                                    </select>
                            </div>
                        </div>
                    </div>

                    <div class='col-md-6'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Solo Parent: </div>
                                <div class="input-group-addon"> <input type="checkbox" id="soloP" placeholder='' name="soloP" class="soloP" <?php echo ($stProfile['solo_parent'] == 'Yes')?"checked":""?> value='Yes'> Yes </div>
                                <div class="input-group-addon"> <input type="checkbox" id="soloP" placeholder='' name="soloP" class="soloP" <?php echo ($stProfile['solo_parent'] == 'No')?"checked":""?> value='No'> No </div>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Sex Orientation: </div>
                                <div class="input-group-addon"> <input type="checkbox" id="sexOrient" placeholder='' name="sexOrient" class="sexOrient" <?php echo ($stProfile['sex_orient'] == 'Heterosexual')?"checked":""?> value='Heterosexual'> Heterosexual </div>
                                <div class="input-group-addon"> <input type="checkbox" id="sexOrient" placeholder='' name="sexOrient" class="sexOrient" <?php echo ($stProfile['sex_orient'] == 'Lesbian')?"checked":""?> value='Lesbian'> Lesbian </div>
                                <div class="input-group-addon"> <input type="checkbox" id="sexOrient" placeholder='' name="sexOrient" class="sexOrient" <?php echo ($stProfile['sex_orient'] == 'Transgender')?"checked":""?> value='Transgender'> Transgender </div>
                                <div class="input-group-addon"> <input type="checkbox" id="sexOrient" placeholder='' name="sexOrient" class="sexOrient" <?php echo ($stProfile['sex_orient'] == 'Gay')?"checked":""?> value='Gay'> Gay </div>
                                <div class="input-group-addon"> <input type="checkbox" id="sexOrient" placeholder='' name="sexOrient" class="sexOrient" <?php echo ($stProfile['sex_orient'] == 'Bisexual')?"checked":""?> value='Bisexual'> Bisexual </div>
                                <div class="input-group-addon"> <input type="checkbox" id="sexOrient" placeholder='' name="sexOrient" class="sexOrient" <?php echo ($stProfile['sex_orient'] == 'Others')?"checked":""?> value='Others'> Others </div>
                            </div>
                        </div>
                    </div>

                    <div class='col-md-4'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Height: </div>
                                    <input type="text" class='form-control' name="height" id='height' value="<?php echo $stProfile['height']?>">
                            </div>
                        </div>
                    </div>

                    <div class='col-md-4'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Weight: </div>
                                    <input type="text" class='form-control' name="weight" id='weight' value="<?php echo $stProfile['weight']?>">
                            </div>
                        </div>
                    </div>

                    <div class='col-md-4'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Birthday: </div>
                                    <input type="date" class='form-control' name="bday" id='bday' value="<?php echo $stProfile['dob']?>">
                            </div>
                        </div>
                    </div>

                    <div class='col-md-6'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Birthplace: </div>
                                    <input type="text" class='form-control' name="bplace" id='bplace' value="<?php echo $stProfile['birthplace']?>">
                            </div>
                        </div>
                    </div>

                    <div class='col-md-6'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Nationality: </div>
                                    <input type="text" class='form-control' name="nationlity" id='nationlity' value="<?php echo $stProfile['nationality']?>">
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Home Address: </div>
                                    <input type='text' class='form-control' id='homeAdd' value="<?php echo $stProfile['home_address']?>"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Email Address: </div>
                                    <input type="email" class='form-control' name="emailAdd" id='emailAdd' value="<?php echo $stProfile['email_address']?>">
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> City Address: </div>
                                    <input type="text" class='form-control' name="cityAdd" id='cityAdd' value="<?php echo $stProfile['city_address']?>">
                            </div>
                        </div>
                    </div>

                    <div class='col-md-6'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> General Average (SH/COLLEGE): </div>
                                    <input type="number" class='form-control' name="genAdd" id='genAdd' value="<?php echo $stProfile['gen_ave']?>">
                            </div>
                        </div>
                    </div>

                    <div class='col-md-6'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Strand/Course: </div>
                                <select name="stTS" id="stTS" class='form-control'>
                                <option value=''> &mdash; Please Choose &mdash; </option>
                                <?php 
                                    $query = mysql_query("SELECT * FROM tbl_track_and_strand");
                                    while($row = mysql_fetch_array($query)){
                                        $id = $row['ts_id'];
                                        $name = $row['ts_name'];
                                        $selected = ($row['ts_id'] == $stProfile['strand_course'])?"selected":"";
                                        echo "<option $selected value='$id'>$name</option>";
                                    }
                                ?>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-6'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Religion: </div>
                                    <input type="text" class='form-control' name="rel" id='rel' value="<?php echo $stProfile['religion']?>">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-6'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Last School Attended: </div>
                                    <textarea style='resize:none' row='5' class='form-control' name="lsa" id='lsa' value=""><?php echo $stProfile['last_sch_att']?></textarea>
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Person to be contacted, in case of emergency: </div>
                                    <input type='text' class='form-control' name="ioe_person" id='ioe_person' value="<?php echo $stProfile['ioe_person']?>">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Address: </div>
                                    <input type='text' class='form-control' name="ioe_address" id='ioe_address' value="<?php echo $stProfile['ioe_address']?>">
                            </div>
                        </div>
                    </div>

                    <div class='col-md-6'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Contact Number: </div>
                                    <input type='text' class='form-control' name="ioe_contact" id='ioe_contact' value="<?php echo $stProfile['ioe_contact']?>">
                            </div>
                        </div>
                    </div>

                    <div class='col-md-6'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Relationship: </div>
                                    <input type='text' class='form-control' name="ioe_relation" id='ioe_relation' value="<?php echo $stProfile['relationship']?>">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12 ' id='divider' style='border: 1px solid #ced4da;'></div>                   
                    <div class='col-md-12'><label for="" style='font-weight: bolder;'> II. Educational Background: </label></div>
                    <div class='col-md-12'>
                        <table id='students' class="table table-bordered table-hover" style='margin-top:10px;'>
                            <thead style='background-color: #343940;color: white;'>
                                <tr>
                                    <th>LEVEL</th>
                                    <th>SCHOOL</th>
                                    <th>YEAR COVERED</th>
                                    <th>PUBLIC/PRIVATE</th>
                                    <th>HONOR RECEIVED</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php tableContent($st_id); ?>
                            </tbody>
                        </table>          
                    </div>

                    <div class='col-md-12' id='divider' style='border: 1px solid #ced4da;'></div>                   
                    <div class='col-md-12'><label for="" style='font-weight: bolder;'> III. Home and Family Background: </label></div>
                    <div class='col-md-8'>
                        <table id='bgfam' class="table table-bordered table-hover" style='margin-top:10px;'>
                            <thead style='background-color: #343940;color: white;'>
                                <tr>
                                    <th></th>
                                   <th>Mother</th>
                                   <th>Father</th>
                                </tr>
                            </thead>
                            <tbody>
                               <tr>
                                    <td>Name</td>
                                    <td><textarea style='resize: none' name='Mname' id='Mname' row='5' class='form-control'><?php echo $getMotherData['name']; ?></textarea></td>
                                    <td><textarea style='resize: none' name='Fname' id='Fname' row='5' class='form-control'><?php echo $getFatherData['name']; ?></textarea></td>
                               </tr>
                               <tr>
                                    <td>Contact</td>
                                    <td><textarea style='resize: none' name='Mcontact' id='Mcontact' row='5' class='form-control'><?php echo $getMotherData['contact']; ?></textarea></td>
                                    <td><textarea style='resize: none' name='Fcontact' id='Fcontact' row='5' class='form-control'><?php echo $getFatherData['contact']; ?></textarea></td>
                               </tr>
                               <tr>
                                    <td>Birthplace</td>
                                    <td><textarea style='resize: none' name='Mbplace' id='Mbplace' row='5' class='form-control'><?php echo $getMotherData['birthplace']; ?></textarea></td>
                                    <td><textarea style='resize: none' name='Fbplace' id='Fbplace' row='5' class='form-control'><?php echo $getFatherData['birthplace']; ?></textarea></td>
                               </tr>
                               <tr>
                                    <td>Religion</td>
                                    <td><textarea style='resize: none' name='Mrel' id='Mrel' row='5' class='form-control'><?php echo $getMotherData['religion']; ?></textarea></td>
                                    <td><textarea style='resize: none' name='Frel' id='Frel' row='5' class='form-control'><?php echo $getFatherData['religion']; ?></textarea></td>
                               </tr>
                               <tr>
                                    <td>Nationality</td>
                                    <td><textarea style='resize: none' name='Mnatl' id='Mnatl' row='5' class='form-control'><?php echo $getMotherData['nationality']; ?></textarea></td>
                                    <td><textarea style='resize: none' name='Fnatl' id='Fnatl' row='5' class='form-control'><?php echo $getFatherData['nationality']; ?></textarea></td>
                               </tr>
                               <tr>
                                    <td>Living or Deceased</td>
                                    <td><textarea style='resize: none' name='Mlord' id='Mlord' row='5' class='form-control'><?php echo $getMotherData['l_d']; ?></textarea></td>
                                    <td><textarea style='resize: none' name='Flord' id='Flord' row='5' class='form-control'><?php echo $getFatherData['l_d']; ?></textarea></td>
                               </tr>
                               <tr>
                                    <td>Educational Attainment</td>
                                    <td><textarea style='resize: none' name='Meduc' id='Meduc' row='5' class='form-control'><?php echo $getMotherData['educ_attainment']; ?></textarea></td>
                                    <td><textarea style='resize: none' name='Feduc' id='Feduc' row='5' class='form-control'><?php echo $getFatherData['educ_attainment']; ?></textarea></td>
                               </tr>
                               <tr>
                                    <td>Occupation</td>
                                    <td><textarea style='resize: none' name='Moccu' id='Moccu' row='5' class='form-control'><?php echo $getMotherData['occupation']; ?></textarea></td>
                                    <td><textarea style='resize: none' name='Foccu' id='Foccu' row='5' class='form-control'><?php echo $getFatherData['occupation']; ?></textarea></td>
                               </tr>
                               <tr>
                                    <td>Name of Employer</td>
                                    <td><textarea style='resize: none' name='Mne' id='Mne' row='5' class='form-control'><?php echo $getMotherData['name_of_employer']; ?></textarea></td>
                                    <td><textarea style='resize: none' name='Fne' id='Fne' row='5' class='form-control'><?php echo $getFatherData['name_of_employer']; ?></textarea></td>
                               </tr>
                               <tr>
                                    <td>Address of Employer</td>
                                    <td><textarea style='resize: none' name='MaddE' id='MaddE' row='5' class='form-control'><?php echo $getMotherData['address_of_employer']; ?></textarea></td>
                                    <td><textarea style='resize: none' name='FaddE' id='FaddE' row='5' class='form-control'><?php echo $getFatherData['address_of_employer']; ?></textarea></td>
                               </tr>
                            </tbody>
                        </table>          
                    </div>
                    <div class='col-md-4'>
                        <table id='students' class="table table-bordered table-hover" style='margin-top:10px;'>
                            <thead style='background-color: #343940;color: white;'>
                                <tr>
                                    <th>Number of Siblings (From Eldest to Youngest)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    for($a = 1; $a <= 10; $a++){
                                        $getSiblings = mysql_fetch_array(mysql_query("SELECT * FROM tbl_student_siblings WHERE `sequence` = '$a' AND student_id = '$st_id'"));
                                        $sibsVal = (!empty($getSiblings['student_id']))?$getSiblings['name'].":".$a:"q:$a";
                                        echo "<tr>";
                                            echo "<td> <textarea style='resize: none' id='sibs$a' onkeyup='getSiblingsValues(".$a.")' row='5' class='form-control'>".$getSiblings['name']."</textarea><input type='hidden' name='siblingsVal' id='siblingsVal$a' value='".$sibsVal."'> </td>";
                                        echo "</tr>";
                                    }
                                ?>
                            </tbody>
                        </table>          
                    </div>

                    <div class='col-md-12 breakAfter' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Parent's Marital Relationship(Please check the box that corresponds to your answer): </div>
                                <div class="input-group-addon"> <input type="checkbox" id="pmr_val" placeholder='' name="pmr_val" class="pmr_val" <?php echo ($stProfile['pmr'] == 'MLT')?"checked":""?> value='MLT'> Married and Living Together </div>
                                <div class="input-group-addon"> <input type="checkbox" id="pmr_val" placeholder='' name="pmr_val" class="pmr_val" <?php echo ($stProfile['pmr'] == 'SP')?"checked":""?> value='SP'> Single Parent </div>
                                <div class="input-group-addon"> <input type="checkbox" id="pmr_val" placeholder='' name="pmr_val" class="pmr_val" <?php echo ($stProfile['pmr'] == 'NMLT')?"checked":""?> value='NMLT'> Not Married but living together </div>
                                <div class="input-group-addon"> <input type="checkbox" id="pmr_val" placeholder='' name="pmr_val" class="pmr_val" <?php echo ($stProfile['pmr'] == 'MS')?"checked":""?> value='MS'> Married But Separated </div>
                                <div class="input-group-addon"> <input type="checkbox" id="pmr_val" placeholder='' name="pmr_val" class="pmr_val" <?php echo ($stProfile['pmr'] == 'others')?"checked":""?> value='others'> Others (Please Specify): <input type='text' class='' value='<?php echo $stProfile['pmr_specify']?>' id='otherSpec'> </div>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12 breakAfter' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon" >For deceased parent/s: </div>
                                <?php echo deceasedParents($st_id);?>
                                
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Ordinal Position(1st Child, 2nd Child, Etc.): </div>
                                    <input type='text' class='form-control' name="ordinal_pos" id='ordinal_pos' value="<?php echo $stProfile['ordinal_pos']?>">
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Number of Children in the family including yourself: </div>
                                    <input type='text' class='form-control' name="num_child" id='num_child' value="<?php echo $stProfile['num_child']?>">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-6'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Number of Brother/s: </div>
                                    <input type='text' class='form-control' name="num_brothers" id='num_brothers' value="<?php echo $stProfile['num_brothers']?>">
                            </div>
                        </div>
                    </div>
                    <div class='col-md-6'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Number of Sister/s: </div>
                                    <input type='text' class='form-control' name="num_sisters" id='num_sisters' value="<?php echo $stProfile['num_sisters']?>">
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Number of Brother/s and Sister/s gainfully employed: </div>
                                    <input type='text' class='form-control' name="num_bs_employed" id='num_bs_employed' value="<?php echo $stProfile['num_bs_employed']?>">
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> Number of Brother/s and Sister/s gainfully employed are providing financial support to your family: </div>
                                    <input type='text' class='form-control' name="num_bs_employed_support" id='num_bs_employed_support' value="<?php echo $stProfile['num_bs_employed_support']?>">
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Who finances your schooling?: </div>
                                <?php echo financeSchooling($st_id)?>
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon"> How much is your weekly allowance?: </div>
                                    <input type='text' class='form-control' name="weekly_allowance" id='weekly_allowance' value="<?php echo $stProfile['weekly_allowance']?>">
                            </div>
                        </div>
                    </div>

                    

                    <div class='col-md-12' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Household Monthly Income: </div>
                                <?php echo householdMonthlyIncomeList($st_id); ?>
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Nature of Residence while attending school: </div>
                                <div class="input-group-addon"> <input type="checkbox" id="resType" placeholder='' name="resType" class="resType" <?php echo ($stProfile['residence_school'] == 'FM')?"checked":""?> value='FM'> Family Home </div>
                                <div class="input-group-addon"> <input type="checkbox" id="resType" placeholder='' name="resType" class="resType" <?php echo ($stProfile['residence_school'] == 'BH')?"checked":""?> value='BH'> Boarding House </div>
                                <div class="input-group-addon"> <input type="checkbox" id="resType" placeholder='' name="resType" class="resType" <?php echo ($stProfile['residence_school'] == 'R')?"checked":""?> value='R'> Room </div>
                                <div class="input-group-addon"> <input type="checkbox" id="resType" placeholder='' name="resType" class="resType" <?php echo ($stProfile['residence_school'] == 'D')?"checked":""?> value='D'> Dorm (Including Board & Lodging) </div>
                                <div class="input-group-addon"> <input type="checkbox" id="resType" placeholder='' name="resType" class="resType" <?php echo ($stProfile['residence_school'] == 'RA')?"checked":""?> value='RA'> Rented Apartment: </div>
                                <div class="input-group-addon"> <input type="checkbox" id="resType" placeholder='' name="resType" class="resType" <?php echo ($stProfile['residence_school'] == 'BS')?"checked":""?> value='BS'> Bed Space: </div>
                                <div class="input-group-addon"> <input type="checkbox" id="resType" placeholder='' name="resType" class="resType" <?php echo ($stProfile['residence_school'] == 'HM')?"checked":""?> value='HM'> House of Married Brother/Sister </div>
                                <div class="input-group-addon"> <input type="checkbox" id="resType" placeholder='' name="resType" class="resType" <?php echo ($stProfile['residence_school'] == 'RH')?"checked":""?> value='RH'> Relative's House </div>
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Do you have a queit place to study?: </div>
                                <div class="input-group-addon"> <input type="checkbox" id="studyPlace" placeholder='' name="studyPlace" class="studyPlace" <?php echo ($stProfile['study_place'] == 'Yes')?"checked":""?> value='Yes'> Yes </div>
                                <div class="input-group-addon"> <input type="checkbox" id="studyPlace" placeholder='' name="studyPlace" class="studyPlace" <?php echo ($stProfile['study_place'] == 'No')?"checked":""?> value='No'> No </div>

                                <div class="input-group-addon"> Remarks <input type="text" id="remarks" placeholder='' name="remarks" class="remarks" value='<?php echo $stProfile['study_place_specify']?>'> </div>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Do you share your room with anyone?: </div>
                                <div class="input-group-addon"> <input type="checkbox" id="share_room" placeholder='' name="share_room" class="share_room" <?php echo ($stProfile['share_room'] == 'Yes')?"checked":""?> value='Yes'> Yes </div>
                                <div class="input-group-addon"> <input type="checkbox" id="share_room" placeholder='' name="share_room" class="share_room" <?php echo ($stProfile['share_room'] == 'No')?"checked":""?> value='No'> No </div>

                                <div class="input-group-addon"> if Yes, with whom? <input type="text" id="withwhom" placeholder='' name="withwhom" class="withwhom" value='<?php echo $stProfile['share_room_whom']?>'> </div>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12' id='divider' style='border: 1px solid #ced4da;'></div>                   
                    <div class='col-md-12'><label for="" style='font-weight: bolder;'> IV. Health: </label></div>

                    <div class='col-md-12' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Do you have health problems?: </div>
                                <?php echo healthList($st_id); ?>
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Did you have a recurring ailment?: </div>
                                <div class="input-group-addon"> <input type="checkbox" id="recurring_ailment" placeholder='' name="recurring_ailment" class="recurring_ailment" <?php echo ($stProfile['recurring_ailment'] == 'Yes')?"checked":""?> value='Yes'> Yes </div>
                                <div class="input-group-addon"> <input type="checkbox" id="recurring_ailment" placeholder='' name="recurring_ailment" class="recurring_ailment" <?php echo ($stProfile['recurring_ailment'] == 'No')?"checked":""?> value='No'> No </div>

                                <div class="input-group-addon"> if Yes, Specify? <input type="text" id="recurring_ailment_specify" placeholder='' name="recurring_ailment_specify" class="form-control recurring_ailment_specify" value='<?php echo $stProfile['recurring_ailment_specify']?>'> </div>
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Did you have any accident or illness which affected you?: </div>
                                <div class="input-group-addon"> <input type="checkbox" id="accident" placeholder='' name="accident" class="accident" <?php echo ($stProfile['accident'] == 'Yes')?"checked":""?> value='Yes'> Yes </div>
                                <div class="input-group-addon"> <input type="checkbox" id="accident" placeholder='' name="accident" class="accident" <?php echo ($stProfile['accident'] == 'No')?"checked":""?> value='No'> No </div>

                                <div class="input-group-addon"> if Yes, Specify? <input type="text" id="accident_specify" placeholder='' name="accident_specify" class="form-control accident_specify" value='<?php echo $stProfile['accident_specify']?>'> </div>
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12 breakAfter' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Have you consulted a psychiatrist or psychologist before?: </div>
                                <div class="input-group-addon"> <input type="checkbox" id="psych" placeholder='' name="psych" class="psych" <?php echo ($stProfile['psych'] == 'Yes')?"checked":""?> value='Yes'> Yes </div>
                                <div class="input-group-addon"> <input type="checkbox" id="psych" placeholder='' name="accident" class="psych" <?php echo ($stProfile['psych'] == 'No')?"checked":""?> value='No'> No </div>

                                <div class="input-group-addon"> if Yes, Specify date and nature of consultation/diagnosis? <input type="text" id="psych_specify" placeholder='' name="psych_specify" class="form-control psych_specify" value='<?php echo $stProfile['psych_specify']?>'> </div>
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12' id='divider' style='border: 1px solid #ced4da;'></div>                   
                    <div class='col-md-12'><label for="" style='font-weight: bolder;'> V. INTEREST/HOBBIES: </label></div>

                    <div class='col-md-6'>
                        <label for="">ACADEMIC</label>
                            <div class='form-group'>
                                <div class='input-group'>
                                    <div class='input-group-addon' style='width: 100%;'>1. At present, I am actively involved <br> in the following organizations or clubs <br> (state your position): </div>   
                                </div>
                                <input type='text' style='width: 100%;' value='<?php echo $getAoneData['ih_values']?>' class='form-control' name='positionOrg' id='positionOrg' data-role='tagsinput'>
                            </div>

                            <div class='form-group'>
                                <div class='input-group'>
                                    <div class='input-group-addon' style='width: 100%;'>2. My Favorite Subject/s: </div>   
                                </div>
                                <input type='text' style='width: 100%;' value='<?php echo $getAtwoData['ih_values']?>' class='form-control' name='favSubj' id='favSubj' data-role='tagsinput'>
                            </div>

                            <div class='form-group'>
                                <div class='input-group'>
                                    <div class='input-group-addon' style='width: 100%;'>3. Subject/s that i like the least: </div>   
                                </div>
                                <input type='text' style='width: 100%;' value='<?php echo $getAthreeData['ih_values']?>' class='form-control' name='leastSubj' id='leastSubj' data-role='tagsinput'>
                            </div>
                    </div>

                    <div class='col-md-6'>
                        <label for="">EXTRA CURRICULAR</label>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon" style='width: 100%;'>1. My Hobbies are </div>
                            </div>
                            <input type='text' style='width: 100%;' class='form-control' name="hobbies" id='hobbies' value="<?php echo $getE1Data['ih_values']?>" data-role="tagsinput">
                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon" style='width: 100%;'>If there's a chance, I am interested to join <br> in the following activities?: </div>
                               <?php echo activityList($st_id) ?>
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12' id='divider' style='border: 1px solid #ced4da;'></div>                   
                    <div class='col-md-12'><label for="" style='font-weight: bolder;'> VI. PSYCHOLOGICAL TEST RESULT: </label></div>
                    <div class='col-md-12'>
                        <table id='students' class="table table-bordered table-hover" style='margin-top:10px;'>
                            <thead style='background-color: #343940;color: white;'>
                                <tr>
                                    <th>DATE</th>
                                    <th>TEST</th>
                                    <th>RESULT</th>
                                    <th>INTERPRETATION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php tableContentPsych($st_id); ?>
                            </tbody>
                        </table>          
                    </div>
                    <div class='col-md-12' id='divider' style='border: 1px solid #ced4da;margin-top: 10px;'></div>                   
                    <div class='col-md-12'><label for="" style='font-weight: bolder;'> VII. CONCERNS: </label></div>

                    <div class='col-md-12' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Do you have any problem/concern which bother you?: </div>
                                <div class="input-group-addon"> <input type="checkbox" id="problem" placeholder='' name="problem" class="problem" <?php echo ($stProfile['problem'] == 'Yes')?"checked":""?> value='Yes'> Yes </div>
                                <div class="input-group-addon"> <input type="checkbox" id="problem" placeholder='' name="problem" class="problem" <?php echo ($stProfile['problem'] == 'No')?"checked":""?> value='No'> No </div>

                                <div class="input-group-addon"> if Yes, Specify <input type="text" id="problem_specify" placeholder='' name="problem_specify" class="form-control problem_specify" value='<?php echo $stProfile['problem_specify']?>'> </div>
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Do you like to discuss this with your counselor?: </div>
                                <div class="input-group-addon"> <input type="checkbox" id="discuss_problem" placeholder='' name="discuss_problem" class="discuss_problem" <?php echo ($stProfile['discuss_problem'] == 'Yes')?"checked":""?> value='Yes'> Yes </div>
                                <div class="input-group-addon"> <input type="checkbox" id="discuss_problem" placeholder='' name="discuss_problem" class="discuss_problem" <?php echo ($stProfile['discuss_problem'] == 'No')?"checked":""?> value='No'> No </div>
                            </div>
                        </div>
                    </div>

                    <div class='col-md-12' id='divider' style='border: 1px solid #ced4da;'></div>                   
                    <div class='col-md-12'><label for="" style='font-weight: bolder;'> VIII. REMARKS (FOR GUIDANCE COUNSELOR ONLY): </label></div>
                    <div class='col-md-12' style='margin-top: 10px'>
                        <div class="form-group">
                            <div class="input-group">
                                <textarea style='resize: none' placeholder='write something... ' id='student_remarks' row='7' class='form-control'><?php echo $stProfile['student_remarks']?></textarea>
                            </div>
                        </div>
                    </div>   

                    <div class='col-md-12' style='margin-top: 10px'>
                        <center><label for="" style='text-align: center'><input type="checkbox" <?php echo ($stProfile['check_hereby'] == 1) ? "checked" : ""; ?> name="herebyTrue" id="herebyTrue"><i> I Hereby certify that the above information is true and correct</i></label><center>
                    </div>             

                    <div class='col-md-12'>
                        <div class="form-group">
                            <button class='btn btn-sm btn-primary pull-right' id='btn_profile' onclick='updateProfile()'><span class='fa fa-check-circle'></span> Save Changes</button>
                        </div>
                    </div>
                   </div>
                </div>
            </div>
        </div>   
    </div>
</div>
<script>
$(document).ready( function(){
   
});
function getTestResult(num){

    var dateTest = $("#dateTest"+num).val();
    var test = $("#test"+num).val();
    var result = $("#result"+num).val();
    var interp = $("#interp"+num).val();

    $("#Testresult"+num).val(dateTest+" : "+test+" : "+result+" : "+interp+" : "+num);

}
function getHealthVal(num){
    if($("#healthVal"+num).prop("checked")){
     var checkval = $("#healthVal"+num).val();
    }else{
    var checkval = "a";
    }

    var getHealthVal = $("#getHealthVal"+num).val();
    if(getHealthVal != ''){
        var getHealthVal2 = $("#getHealthVal"+num).val();
    }else{
        var getHealthVal2 = "a";
    }
    

    $("#savedResult"+num).val(checkval+':'+getHealthVal2);
}

function getactList(num){
    if($("#actList"+num).prop("checked")){
     var checkval = $("#actList"+num).val();
    }else{
    var checkval = "a";
    }

    var actSpec = $("#actSpec"+num).val();
    if(actSpec != ''){
        var actSpec2 = $("#actSpec"+num).val();
    }else{
        var actSpec2 = "a";
    }
    

    $("#savedResultAct"+num).val(checkval+':'+actSpec2);
}

function getactListVal(num){
    if($("#actList"+num).prop("checked")){
     var checkval = $("#actList"+num).val();
    }else{
    var checkval = "a";
    }

    var actSpec = $("#actSpec"+num).val();
    if(actSpec != ''){
        var actSpec2 = $("#actSpec"+num).val();
    }else{
        var actSpec2 = "a";
    }

    $("#savedResultAct"+num).val(checkval+':'+actSpec2);
}
function getHealthValue(num){
    if($("#healthVal"+num).prop("checked")){
     var checkval = $("#healthVal"+num).val();
    }else{
    var checkval = "a";
    }

    var getHealthVal = $("#getHealthVal"+num).val();
    if(getHealthVal != ''){
        var getHealthVal2 = $("#getHealthVal"+num).val();
    }else{
        var getHealthVal2 = "a";
    }
    

    $("#savedResult"+num).val(checkval+':'+getHealthVal2);
}
function getFinance(num){
    
    if($("#financer"+num).prop("checked")){
        var checkval = $("#financer"+num).val();
    }else{
        var checkval = "";
    }

    if(num == 5 || num == 6){
        var financeSpec = $("#financeSpec"+num).val();
    }else{
        var financeSpec = "";
    }
    

    $("#saveResultFS"+num).val(checkval+':'+financeSpec);
}

function getSpecFinance(num){
    if($("#financer"+num).prop("checked")){
        var checkval = $("#financer"+num).val();
    }else{
        var checkval = "";
    }

    if(num == 5 || num == 6){
        var financeSpec = $("#financeSpec"+num).val();
    }else{
        var financeSpec = "a";
    }
    

    $("#saveResultFS"+num).val(checkval+':'+financeSpec);
}
function checkifYes(num){
    if($("#deceasedCheck"+num).prop("checked")){
        var checkval = 1;
        var checkval2 = num;
    }else{
        var checkval = 'a';
        var checkval2 = 'a';
    }
    var dateDec = $("#dateDec"+num).val();
    if(dateDec != ''){
        var dateVal = $("#dateDec"+num).val();
    }else{
        var dateVal = 'a';
    }
    var CauseDec = $("#CauseDec"+num).val();
    if(CauseDec != ''){
        var causeVal = $("#CauseDec"+num).val();
    }else{
        var causeVal = 'a';
    }

    $("#saveResult"+num).val(checkval+':'+dateVal+':'+causeVal+':'+checkval2);
}

function causeDeceased(num){
    if($("#deceasedCheck"+num).prop("checked")){
        var checkval = 1;
        var checkval2 = num;
    }else{
        var checkval = 'a';
        var checkval2 = 'a';
    }
    var dateDec = $("#dateDec"+num).val();
    if(dateDec != ''){
        var dateVal = $("#dateDec"+num).val();
    }else{
        var dateVal = 'a';
    }
    var CauseDec = $("#CauseDec"+num).val();
    if(CauseDec != ''){
        var causeVal = $("#CauseDec"+num).val();
    }else{
        var causeVal = 'a';
    }

    $("#saveResult"+num).val(checkval+':'+dateVal+':'+causeVal+':'+checkval2);
}
function getDateTest(num){
    var dateTest = $("#dateTest"+num).val();
    var test = $("#test"+num).val();
    var result = $("#result"+num).val();
    var interp = $("#interp"+num).val();

    $("#Testresult"+num).val(dateTest+" : "+test+" : "+result+" : "+interp+" : "+num);
}
// function getValfamily(num){
//     var famBg = $("#pp"+num).val();
//     $("#homeFamBg").val(famBg);
// }
function getSiblingsValues(num){
    var sibs = $("#sibs"+num).val();
    if(sibs == ''){
        $("#siblingsVal"+num).val('q:'+num);
    }else{
        $("#siblingsVal"+num).val(sibs+':'+num);
    }
}
function getVal(valRet){
    var sch = $("#sch"+valRet).val();
    var yc = $("#yc"+valRet).val();
    var pp = $("#pp"+valRet).val();
    var hr = $("#hr"+valRet).val();

    $("#val"+valRet).val(sch+" : "+yc+" : "+pp+" : "+hr+" : "+valRet);
}
function getPPVal(valRet){
    var sch = $("#sch"+valRet).val();
    var yc = $("#yc"+valRet).val();
    var pp = $("#pp"+valRet).val();
    var hr = $("#hr"+valRet).val();

    $("#val"+valRet).val(sch+" : "+yc+" : "+pp+" : "+hr+" : "+valRet);
}
function inputIncomeOthers(){
    
    if($("#hIncome").is(":checked")){
        $("#incomeOthers").attr("readonly", false);
    }else{
        $("#incomeOthers").attr("readonly", true);
        $("#incomeOthers").val("");
    }
}
function updateProfile(){
    var fname = $("#fname").val();
    var mname = $("#mname").val();
    var lname = $("#lname").val();
    var program = $("#program").val();
    var year_level = $("#year_level").val();
    var section = $("#section").val();
    var herebyTrue = ($("input[name='herebyTrue']").is(":checked")) ? 1 : 0;
    
    if($("input[name='stSex']").is(":checked")){
        var stSex = $("input[name='stSex']:checked").val();
    }else{
        var stSex = "";
    }
    var stAge = $("#stAge").val();
    var stcontact = $("#stcontact").val();
    var civil_stat = $("#civil_stat").val();
    
    if($("input[name='soloP']").is(":checked")){
        var soloP = $("input[name='soloP']:checked").val();
    }else{
        var soloP = "";
    }
    if($("input[name='sexOrient']").is(":checked")){
        var sexOrient = $("input[name='sexOrient']:checked").val();
    }else{
        var sexOrient = "";
    }

    if($("input[name='pmr_val']").is(":checked")){
        var pmr_res = $("input[name='pmr_val']:checked").val();
    }else{
        var pmr_res = "";
    }

    var otherSpec = $("#otherSpec").val();

    if($("input[name='studyPlace']").is(":checked")){
        var studyPlace = $("input[name='studyPlace']:checked").val();
    }else{
        var studyPlace = "";
    }

    var remarks = $("#remarks").val();

    if($("input[name='share_room']").is(":checked")){
        var share_room = $("input[name='share_room']:checked").val();
    }else{
        var share_room = "";
    }

    var withwhom = $("#withwhom").val();

    if($("input[name='recurring_ailment']").is(":checked")){
        var recurring_ailment = $("input[name='recurring_ailment']:checked").val();
    }else{
        var recurring_ailment = "";
    }

    var recurring_ailment_specify = $("#recurring_ailment_specify").val();

    if($("input[name='accident']").is(":checked")){
        var accident = $("input[name='accident']:checked").val();
    }else{
        var accident = "";
    }

    var accident_specify = $("#accident_specify").val();

    if($("input[name='psych']").is(":checked")){
        var psych = $("input[name='psych']:checked").val();
    }else{
        var psych = "";
    }

    var psych_specify = $("#psych_specify").val();

    if($("input[name='problem']").is(":checked")){
        var problem = $("input[name='problem']:checked").val();
    }else{
        var problem = "";
    }

    var problem_specify = $("#problem_specify").val();

    if($("input[name='discuss_problem']").is(":checked")){
        var discuss_problem = $("input[name='discuss_problem']:checked").val();
    }else{
        var discuss_problem = "";
    }

    var student_remarks = $("#student_remarks").val();

    if($("input[name='resType']").is(":checked")){
        var resType = $("input[name='resType']:checked").val();
    }else{
        var resType = "";
    }

    if($("input[name='hIncome']").is(":checked")){
        var hIncome = $("input[name='hIncome']:checked").val();
    }else{
        var hIncome = "";
    }
    
    var incomeOthers = $("#incomeOthers").val();
    var height = $("#height").val();
    var weight = $("#weight").val();
    var bday = $("#bday").val();
    var bplace = $("#bplace").val();
    var nationlity = $("#nationlity").val();
    var homeAdd = $("#homeAdd").val();
    var emailAdd = $("#emailAdd").val();
    var cityAdd = $("#cityAdd").val();
    var genAdd = $("#genAdd").val();
    var stTS = $("#stTS").val();
    var rel = $("#rel").val();
    var lsa = $("#lsa").val();
    var ioe_person = $("#ioe_person").val();
    var ioe_address = $("#ioe_address").val();
    var ioe_contact = $("#ioe_contact").val();
    var ioe_relation = $("#ioe_relation").val();

    var stid = $("#stid").val();

    var edBack = $('input[name="edBack"]').map(function() {
									return this.value;
							 	}).get();
    array_ed = [];

    var siblings = $('input[name="siblingsVal"]').map(function() {
									return this.value;
							 	}).get();
    array_sibs = [];

    var Testresult = $('input[name="Testresult"]').map(function() {
									return this.value;
							 	}).get();
    array_test = [];
    var deceasedMember = $('input[name="saveResult"]').map(function() {
									return this.value;
							 	}).get();
    array_deceased = [];

    var health = $('input[name="savedResultHealth"]').map(function() {
									return this.value;
							 	}).get();
    array_health = [];

    var finance = $('input[name="saveResultFS"]').map(function() {
									return this.value;
							 	}).get();
    array_finance = [];

    var savedResultAct = $('input[name="savedResultAct"]').map(function() {
									return this.value;
							 	}).get();
    array_act = [];
    
    var ordinal_pos = $("#ordinal_pos").val();
    var num_child = $("#num_child").val();
    var num_brothers = $("#num_brothers").val();
    var num_sisters = $("#num_sisters").val();
    var num_bs_employed = $("#num_bs_employed").val();
    var num_bs_employed_support = $("#num_bs_employed_support").val();
    var weekly_allowance = $("#weekly_allowance").val();

    // MOTHER
    var Mname = $("#Mname").val();
    var Mcontact = $("#Mcontact").val();
    var Mbplace = $("#Mbplace").val();
    var Mrel = $("#Mrel").val();
    var Mnatl = $("#Mnatl").val();
    var Mlord = $("#Mlord").val();
    var Meduc = $("#Meduc").val();
    var Moccu = $("#Moccu").val();
    var Mne = $("#Mne").val();
    var MaddE = $("#MaddE").val();

    // FATHER
    var Fname = $("#Fname").val();
    var Fcontact = $("#Fcontact").val();
    var Fbplace = $("#Fbplace").val();
    var Frel = $("#Frel").val();
    var Fnatl = $("#Fnatl").val();
    var Flord = $("#Flord").val();
    var Feduc = $("#Feduc").val();
    var Foccu = $("#Foccu").val();
    var Fne = $("#Fne").val();
    var FaddE = $("#FaddE").val();

    var positionOrg = $("#positionOrg").val();
    var favSubj = $("#favSubj").val();
    var leastSubj = $("#leastSubj").val();
    var hobbies = $("#hobbies").val();

    $("#btn_profile").prop("disabled", true);
    $("#btn_profile").html("<span class='fa fa-spin fa-spinner'></span> Updating... ");
    $.post("ajax/update_student_profile.php",{
        stid: stid,
        fname: fname,
        mname: mname,
        lname: lname,
        program: program,
        year_level: year_level,
        section: section,
        stSex: stSex,
        stAge: stAge,
        stcontact: stcontact,
        civil_stat: civil_stat,
        soloP: soloP,
        sexOrient: sexOrient,
        height: height,
        weight: weight,
        bday: bday,
        bplace: bplace,
        nationlity: nationlity,
        homeAdd: homeAdd,
        emailAdd: emailAdd,
        cityAdd: cityAdd,
        genAdd: genAdd,
        stTS: stTS,
        rel: rel,
        lsa: lsa,
        ioe_person: ioe_person,
        ioe_address: ioe_address,
        ioe_contact: ioe_contact,
        ioe_relation: ioe_relation,
        array_ed: edBack,
        array_sibs: siblings,
        pmr_res: pmr_res,
        otherSpec: otherSpec,
        ordinal_pos: ordinal_pos,
        num_child: num_child,
        num_brothers: num_brothers,
        num_sisters: num_sisters,
        num_bs_employed: num_bs_employed,
        num_bs_employed_support: num_bs_employed_support,
        weekly_allowance: weekly_allowance,
        hIncome: hIncome,
        incomeOthers: incomeOthers,
        resType: resType,
        studyPlace: studyPlace,
        remarks: remarks,
        share_room: share_room,
        withwhom: withwhom,
        recurring_ailment: recurring_ailment,
        recurring_ailment_specify: recurring_ailment_specify,
        accident: accident,
        accident_specify: accident_specify,
        psych: psych,
        psych_specify: psych_specify,
        problem: problem,
        problem_specify: problem_specify,
        discuss_problem: discuss_problem,
        student_remarks: student_remarks,
        array_test: Testresult,
        array_deceased: deceasedMember,
        array_health: health,
        array_finance: finance,
        Mname: Mname,
        Mcontact: Mcontact,
        Mbplace: Mbplace,
        Mrel: Mrel,
        Mnatl: Mnatl,
        Mlord: Mlord,
        Meduc: Meduc,
        Moccu: Moccu,
        Mne: Mne,
        MaddE: MaddE,
        Fname: Fname,
        Fcontact: Fcontact,
        Fbplace: Fbplace,
        Frel: Frel,
        Fnatl: Fnatl,
        Flord: Flord,
        Feduc: Feduc,
        Foccu: Foccu,
        Fne: Fne,
        FaddE: FaddE,
        positionOrg: positionOrg,
        favSubj: favSubj,
        leastSubj: leastSubj,
        hobbies: hobbies,
        array_act: savedResultAct,
        herebyTrue: herebyTrue
    }, function(data){
        if(data > 0){
            alert("Successfully Updated");
            window.location.reload();
        }else{
            alert("ERROR");
        }
        $("#btn_profile").prop("disabled", false);
        $("#btn_profile").html("<span class='fa fa-check-circle'></span> Save Changes ");
    });

}
</script>