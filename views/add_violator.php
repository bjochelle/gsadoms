<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Add Violator</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Student: </div>
                                    <select id='studentID' class='form-control' onchange="getPrevViolation()">
                                        <?php echo getActiveStudents(); ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class='col-md-12'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Incident date: </div>
                                    <input type="date" class='form-control' id='incedentDate' name="">
                                    <div class="input-group-addon"> Time: </div>
                                    <input type="time" class='form-control' id='incedentTime' name="">
                                </div>
                            </div>
                        </div>
                        
                        <div class='col-md-12'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Violation: </div>
                                    <select id='violationID' class='form-control'  onchange="getPrevViolation()">
                                        <?php echo getViolations(); ?>
                                    </select>
                                    <div class="input-group-addon"><input type="checkbox" name="checkOthers" id='checkOthers' onchange='other_violation()'> Others </div>
                                </div>
                            </div>
                        </div>

                        <div class='col-md-12' id='violation_others_div' style="display: none;">
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Violation: </div>
                                    <input type="text" class='form-control' id='violation_others' name="">
                                </div>
                            </div>
                        </div>

                        <div class='col-md-12'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Reported By: </div>
                                    <input type="text" class='form-control' id='reportedBy' name="">
                                </div>
                            </div>
                        </div>

                         <div class='col-md-12'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Reported date: </div>
                                    <input type="date" class='form-control' id='reportedDate' name="">
                                    <div class="input-group-addon"> Time: </div>
                                    <input type="time" class='form-control' id='reportedTime' name="">
                                </div>
                            </div>
                        </div>
                        <div class='col-md-12' style='border: 1px solid #ced4da;'></div>
                        
                           
                             <div class='col-md-12' id='prev_violations' style='display: none;margin-top: 10px'>
                                <table id='prevviolations' class="table table-bordered table-hover" style='margin-top:10px;width: 100%;'>
                                    <thead style='background-color: #343940;color: white;'>
                                        <tr>
                                            <th>#</th>
                                            <th>VIOLATION</th>
                                            <th>LEVEL</th>
                                            <th>PENALTY</th>
                                            <th>DATE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <div class='col-md-12' id='for_penalty' style="margin-top: 10px">
                                
                            </div>
                       
                            <div class='col-md-12' id="for_existing">
                                <div class="form-group" style='margin-top: 10px;'>
                                   <button class='btn btn-sm btn-primary pull-right' id='saveV' onclick='saveViolator()'> <span class='fa fa-check-circle'></span> Save Changes</button>
                                </div>
                            </div>

                            <div class="col-md-12" id="non_exist" style="display: none;margin-top: 10px;">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">Violation: </div>
                                        <input type="text" class='form-control' id='nonexistViolation' name="">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12" id="non_exist_penalty" style="display: none;margin-top: 10px;">
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">Penalty: </div>
                                        <select id='penaltyID_nonExist' class='form-control' style="width: 35%;">
                                            <?php 
                                                echo "<option value=''> &mdash; Please Choose &mdash; </option>";
                                                $query = mysql_query("SELECT * FROM tbl_penalties");
                                                while($row = mysql_fetch_array($query)){
                                                    $selected = ($row['level'] == $level_v2)?"selected":"";
                                                    echo "<option ".$selected." value='".$row['penalty_id']."'>".$row['penalty_name']."</option>";
                                                }
                                            ?>
                                        </select>
                                        <div class="input-group-addon"> 1st Offense - Level:  </div>
                                        <input type="number" class="form-control" id="levelNonExist" name="">
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12" id="non_exist_btn" style="display: none;margin-top: 10px;">
                                <div class="form-group" style='margin-top: 10px;'>
                                   <button class='btn btn-sm btn-primary pull-right' id='saveNonExist' onclick='saveNonExistViolator()'> <span class='fa fa-check-circle'></span> Save Changes</button>
                                </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>

<script>

$(document).ready( function(){
    
});
function getOffenseLevel(){
    var penaltyID_others = $("#penaltyID_others").val();
    $.post("ajax/getOffenseLevel_others.php", {
        penaltyID_others: penaltyID_others
    }, function(data){
        $("#for_offense_level").html(data);
    });
}
function other_violation(){
   if($("input[name='checkOthers']").is(":checked")){
        $("#violationID").prop("disabled", true);
        $("#violationID").val("");
        $("#prev_violations").css("display","none");
        $("#for_penalty").css("display","none");
        $("#for_existing").css("display","none");
        $("#non_exist").css("display","block");
        $("#non_exist_btn").css("display","block");
        $("#non_exist_penalty").css("display","block");
   }else{
        $("#violationID").prop("disabled", false);
        $("#violation_others").val("");
        $("#prev_violations").css("display","block");
        $("#for_penalty").css("display","block");
        $("#for_existing").css("display","block");
        $("#non_exist").css("display","none");
        $("#non_exist_btn").css("display","none");
        $("#non_exist_penalty").css("display","none");
   }
}
function getPrevViolation(){
    var studentID = $("#studentID").val();
    var violationID = $("#violationID").val();
    $.post("ajax/check_exist_violation.php", {
        studentID: studentID
    }, function(data){ 
            $("#prev_violations").css("display","block");
            prevviolationsList(studentID, violationID);
            getPenaltyLevel(violationID, studentID);
        
    })
}
function getPenaltyLevel(violationID, studentID){
    $.post("ajax/getPenaltyforViolator.php", {
        violationID: violationID,
        studentID: studentID
    }, function(data){
        $("#for_penalty").html(data);
    });
}
function saveNonExistViolator(){
    var studentID = $("#studentID").val();
    var penaltyID_nonExist = $("#penaltyID_nonExist").val();
    var incedentDate = $("#incedentDate").val();
    var incedentTime = $("#incedentTime").val();
    var reportedBy = $("#reportedBy").val();
    var reportedDate = $("#reportedDate").val();
    var reportedTime = $("#reportedTime").val();
    var levelNonExist = $("#levelNonExist").val();
    var nonexistViolation = $("#nonexistViolation").val();
    $("#saveNonExist").prop("disabled", true);
    $("#saveNonExist").html("<span class='fas fa-spin fa-spinner'></span> Loading");
    $.post("ajax/add_violator_nonExist.php", {
        studentID: studentID,
        penaltyID_nonExist: penaltyID_nonExist,
        incedentDate: incedentDate,
        incedentTime: incedentTime,
        reportedBy: reportedBy,
        reportedDate: reportedDate,
        reportedTime: reportedTime,
        levelNonExist: levelNonExist,
        nonexistViolation: nonexistViolation
    }, function(data){
        if(data == 1){
            alert("Successfully Added");
        }else{
            alert("Error");
        }
        window.location = 'index.php?page=violator-lists';
    });
}
function saveViolator(){
    var studentID = $("#studentID").val();
    var violationID = $("#violationID").val();
    var penaltyID = $("#penaltyID").val();
    var incedentDate = $("#incedentDate").val();
    var incedentTime = $("#incedentTime").val();
    var reportedBy = $("#reportedBy").val();
    var reportedDate = $("#reportedDate").val();
    var reportedTime = $("#reportedTime").val();
    $("#saveV").prop("disabled", true);
    $("#saveV").html("<span class='fas fa-spin fa-spinner'></span> Loading");
    $.post("ajax/add_violator.php", {
        studentID: studentID,
        violationID: violationID,
        penaltyID: penaltyID,
        incedentDate: incedentDate,
        incedentTime: incedentTime,
        reportedBy: reportedBy,
        reportedDate: reportedDate,
        reportedTime: reportedTime
    }, function(data){
        if(data == 1){
            alert("Successfully Added");
        }else{
            alert("Error");
        }
        window.location = 'index.php?page=violator-lists';
    });
}
function viewRecord(vltnID,vltnName){
    $("#vltnModalU").modal('show');
    $("#vltnName").val(vltnName);
    $("#vltnID").val(vltnID);
}
function updateVltn(){
    var vltnName = $("#vltnName").val();
    var vltnID = $("#vltnID").val();
    var action = 'update';
    $("#btn_update_vltn").prop("disabled", true);
    $("#btn_update_vltn").html("<span class='fas fa-spin fa-spinner'></span> Loading");
    $.post("ajax/violation_functions.php", {
        vltnName: vltnName,
        vltnID: vltnID,
        action: action
    }, function(data){
        $("#vltnModalU").modal('hide');
        if(data == 1){
            alert("Successfully Added");
        }else if(data == 2){
            alert("Already Existed");
        }else{
            alert("Error");
        }
        //violationLists();
        $("#btn_update_vltn").prop("disabled", false);
        $("#btn_update_vltn").html("<span class='fas fa-check-circle'></span> Continue");
    });
}
function deleteVltn(){
    var count_checked = $('input[name="checkbox_vltn"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        var action = 'delete';
        $("#deleteVltn").prop("disabled", true);
        $("#deleteVltn").html("<span class='fas fa-spin fa-spinner'></span> Loading");
        $.post("ajax/violation_functions.php", {
            action: action,
            checked_array: count_checked
        }, function(data){
            if(data > 0){
                alert("Successfully Deleted");
            }else{
                alert("Error");
            }
          //  violationLists();
            $("#deleteVltn").prop("disabled", false);
            $("#deleteVltn").html("<span class='fas fa-trash'></span> Delete");
        });
}
function prevviolationsList(studentID, violationID){
    $("#prevviolations").DataTable().destroy();
    $('#prevviolations').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/prev_violations.php",
        "dataSrc":"data",
        "data":{
          studentID: studentID,
          violationID: violationID
        },
        "type": "POST"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"violation"
        },
        {
            "data":"level"
        },
        {
            "data":"penalty"
        },
        {
            "data":"date"
        }
        
    ]   
    });
}
</script>