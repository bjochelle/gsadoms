<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">User Classification</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                   <div class='row'>
                        <div class='col-md-12'>
                            <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target="#mediumModal"><span class='fa fa-plus-circle'></span> Add</button>
                            <button class='btn btn-sm btn-danger pull-right' onclick='deleteClass()' id='deleteClass'><span class='fa fa-trash'></span> Delete</button>
                        </div>
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='classification' class="table table-bordered table-hover" style='margin-top:10px;'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th></th>
                                        <th>#</th>
                                        <th></th>
                                        <th>CLASSIFICATION NAME</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                   </div>
                </div>
            </div>
        </div>   
    </div>
</div>

<script>
$(document).ready( function(){
    classificationLists();
});
function addNewClass(){
    var className = $("#className").val();
    var action = 'add';
    $("#btn_add_class").prop("disabled", true);
    $("#btn_add_class").html("<span class='fas fa-spin fa-spinner'></span> Loading");
    $.post("ajax/classification_functions.php", {
        className: className,
        action: action
    }, function(data){
        $("#mediumModal").modal('hide');
        if(data == 1){
            alert("Successfully Added");
        }else if(data == 2){
            alert("Existing Data");
        }else{
            alert("Error");
        }
        classificationLists();
        $("#className").val("");
        $("#btn_add_class").prop("disabled", false);
        $("#btn_add_class").html("<span class='fas fa-check-circle'></span> Continue");
    });
}
function viewRecord(classID, classNAME){
    $("#updateClass").modal();
    $("#classificationName").val(classNAME);
    $("#classificationID").val(classID);
}
function UpdateClass(){
    var cName = $("#classificationName").val();
    var cId = $("#classificationID").val();
    var action = 'update';
    $("#btn_update_class").prop("disabled", true);
    $("#btn_update_class").html("<span class='fas fa-spin fa-spinner'></span> Loading");
    $.post("ajax/classification_functions.php", {
        cName: cName,
        cId: cId,
        action: action
    }, function(data){
        $("#updateClass").modal('hide');
        if(data == 1){
            alert("Successfully Changed");
        }else{
            alert("Error");
        }
        classificationLists();
        $("#btn_update_class").prop("disabled", false);
        $("#btn_update_class").html("<span class='fas fa-check-circle'></span> Continue");
    });
}
function deleteClass(){
    var count_checked = $('input[name="checkbox_class"]:checked').map(function() {
									return this.value;
							 	}).get();
    checked_array = [];
    var action = 'delete';
    $("#deleteClass").prop("disabled", true);
    $("#deleteClass").html("<span class='fas fa-spin fa-spinner'></span> Loading");
    $.post("ajax/classification_functions.php", {
        action: action,
        checked_array: count_checked
    }, function(data){
        if(data > 0){
            alert("Successfully Deleted");
        }else{
            alert("Error");
        }
        classificationLists();
        $("#deleteClass").prop("disabled", false);
        $("#deleteClass").html("<span class='fas fa-trash'></span> Delete");
    });
}
function classificationLists(){
    $("#classification").DataTable().destroy();
    $('#classification').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/classifications.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "mRender": function(data,type,row){
                return "<input type='checkbox' name='checkbox_class' value='"+row.classID+"'>";		
            }
        },
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"classification"
        }
        
    ]   
    });
}
</script>