<style type="text/css">

</style>
<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Counseling</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                   <div class='row'>
                        <div class='col-md-12'>
                            <div class="form-group">
                               <button class='btn btn-primary btn-sm pull-right' data-toggle='modal' data-target='#addDirectCounseling'><span class='fa fa-plus-circle'></span> Add New</button>
                              
                            </div>
                        </div> 
                        <div class='col-md-12' style='margin-top:10px;overflow: auto;'>
                            <table id='councelingList' class="table table-bordered table-hover" style='margin-top:10px;width: 100%;'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th>#</th>
                                        <th></th>
                                        <th>STUDENT NAME</th>
                                        <th>CONCERN</th>
                                        <th>ADDED BY</th>
                                        <th>SCHEDULE</th>
                                        <th>ACTION TAKEN</th>
                                        <th>REMARKS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                   </div>
                </div>
            </div>
        </div>   
    </div>
</div>


<script>
    $(document).ready( function(){
        councelingList();
    });
    function setReferalSchedDirect(){
        var stID = $("#studentID").val();
        var date = $("#dcDate").val();
        var time = $("#dcTime").val();
        var concern = $("#dcViolation").val();
        if(stID == '' || date == '' || time == '' || concern == ''){
            alert("Please Fill All Fields");
        }else{
            $.post("ajax/addDirectReferral.php", {
                stID: stID,
                date: date,
                time: time,
                concern: concern
            }, function(data){
                if(data > 0){
                    alert("Successfully Added");
                }else{
                    alert("Failed");
                }

                $("#addDirectCounseling").modal('hide');
                councelingList();
            });
        }
    }
    function saveCnclngFeedback(){
        var actionTaken = $("#actionTaken").val();
        var remarks = $("#remarks").val();
        var counsellingID = $("#counsellingID").val();
        var studentid = $("#CstID").val();
        var dateSched = $("#dateSched").val();
        $.post("ajax/saveFeedback.php", {
            actionTaken: actionTaken,
            remarks: remarks,
            counsellingID: counsellingID,
            studentid: studentid
        }, function(data){
            $("#feedbackModal").modal('hide');
            if(data > 0){
                alert("Successfully Added");
            }else{
                alert("Failed");
            }
            councelingList();

        });
    }
    function feedback(id,student){
        $.post("ajax/feedbackChecker.php", {
            id: id,
            student: student
        }, function(data){
            if(data == 1){
                alert("Already Have Feedback");
            }else{
                $("#counsellingID").val(id);
                $("#CstID").val(student);
                $("#feedbackModal").modal();
            }
        });
    }
    function setCounselingSched(sc_id , st_id){
        $("#counselingSched").modal();
        $("#scID").val(sc_id);
        $("#studID").val(st_id);
    }
    function setReferalSched(){
        var date = $("#counselingDate").val();
        var time = $("#counselingTime").val();
        var sc_id = $("#scID").val();
        var st_id = $("#studID").val();
        if(date == '' && time == ''){
            alert("Please select date and time");
        }else{
            $.post("ajax/setReferelSchedule.php", {
                date: date,
                time: time,
                sc_id: sc_id,
                st_id: st_id
            }, function(data){

                if(data > 0){
                    alert("Successfully Added");
                }else{
                    alert("Error");
                }
                $("#counselingSched").modal('hide');
                councelingList();
            });
        }
    }
    function councelingList(){
        $("#councelingList").DataTable().destroy();
        $('#councelingList').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/counceling_list.php",
            "dataSrc":"data"
            },
        "columns":[
            {
                "data":"count"
            },
            {
                "data":"action"
            },
            {
                "data":"name"
            },
            {
                "data":"concern"
            },
            {
                "data":"addedBy"
            },
            {
                "data":"sched"
            },
            {
                "data":"actionTaken"
            },
            {
                "data":"remarks"
            }
            
        ]   
        });
    }
</script>