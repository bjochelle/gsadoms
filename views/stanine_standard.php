<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Standard Stanine</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <div class='row'>
                        <div class='col-md-12'>
                            <button class='btn btn-sm btn-primary pull-right' onclick='setStanine()' id='stanineStandard'><span class='fa fa-check-circle'></span> Save Changes </button>
                        </div>
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='coursePrograms' class="table table-bordered table-hover" style='margin-top:10px;'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th>#</th>
                                        <th>DEPARTMENT</th>
                                        <th>PASSING STANINE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>
<script>
$(document).ready( function(){
    programLists();
});
function statnineValue(prgmID){
    var stnn = $("#stnn"+prgmID).val();
    $("#stnnToSave"+prgmID).val(stnn+" - "+prgmID);
}
function setStanine(){
    var checkedData = $('input[name="stnnToSave"]').map(function() {
                            return this.value;
                        }).get();
    checkedData_arr = [];
    $("#stanineStandard").prop("disabled", true);
    $("#stanineStandard").html("<span class='fas fa-spin fa-spinner'></span> Loading");
    $.post("ajax/stanine_functions.php", {
        checkedData_arr: checkedData
    }, function(data){
        if(data > 0){
            alert("Successfully Updated");
        }else{
            alert("Error");
        }
        programLists();
        $("#stanineStandard").prop("disabled", false);
        $("#stanineStandard").html("<span class='fas fa-check-circle'></span> Save Changes");
    });                 

}
function programLists(){
    $("#coursePrograms").DataTable().destroy();
    $('#coursePrograms').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/programs_stanine.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"prgm"
        },
        {
            "data":"stanine"
        }
        
    ]   
    });
}
</script>