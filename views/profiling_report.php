<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Profiling Report</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="form-group">
                                <div class="input-group" >
                                    <div class="input-group-addon">Graph Type: </div>
                                       <select id='graphCategory' class='form-control select2' onchange="graphCategory()">
                                          <option value=""> &mdash; Please Choose &mdash;</option>
                                          <option value='SO'> Sexual Orientation </option>
                                          <option value='CS'> Civil Status </option>
                                          <option value='R'> Religion </option>
                                          <option value='A'> Address </option>
                                          <option value='ST'> School Type </option>
                                          <option value='PMR'> Parent Marital Relationship </option>
                                          <option value='DP'> Deceased Parents </option>
                                          <option value='AGE'> Age </option>
                                          <option value='NCF'> Number of children in family </option>
                                          <option value='NGE'> Brother/s and Sister/s gainfully employed </option>
                                          <option value='GEFS'> Brother/s and Sister/s gainfully employed that provides financial support to the family </option>
                                          <option value='WA'> Weekly Allowance </option>
                                          <option value='HMI'> Household Monthly Income </option>
                                          <option value='NR'> Nature of Residence </option>
                                          <option value='FWS'> Students Financial Support </option>
                                         
                                       </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-12'>
                            <div id='chart' style='width:100% !important; margin-top: 20px;'>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>

<script src="assets/js/highcharts1.js"></script>
<script src="assets/js/highcharts-more.js"></script>
<script src="assets/js/exporting.js"></script>

<script type="text/javascript">
function graphCategory(){
    var graphCategory = $("#graphCategory").val();
    bot_chart(graphCategory);
}

  $(document).ready(function(){
    
  });
  
  function bot_chart(graphCategory){
    var title = (graphCategory == 'SO') ? "Sexual Orientation" : ((graphCategory == "CS")? "Civil Status" : ((graphCategory == "R")?"Religion":((graphCategory == 'A')?"Adress":((graphCategory == 'PMR')?"Parents Marital Relationship":((graphCategory == "AGE")?"Age":((graphCategory == 'NCF')?"Number of children in family":((graphCategory == "WA")?"Weekly Allowance":((graphCategory == 'HMI')?"Household Monthly Income":((graphCategory == 'NR')?"Nature of Residence":((graphCategory == 'NGE')?"Brother/s and Sister/s gainfully employed":((graphCategory == 'GEFS')?"Brother/s and Sister/s gainfully employed that provides financial support to the family":((graphCategory == 'FWS')?"Students Financial Support":((graphCategory == 'DP')?"Deceased Parents":((graphCategory == 'ST')?"School Type":""))))))))))))));
  $("#chart").html("<h4><center><span class='fa fa-spinner fa-spin'></span> Loading graph ...</center></h4>");

  $.getJSON('ajax/chart.php?graphCat='+graphCategory+'', function(data){
    Highcharts.chart("chart", {
      chart: {
        type: 'column'
      },
      title: {
        useHTML: true,
        text: "<div class='col-md-12' style='text-align:center;margin-bottom:20px'><img src='assets/images/chmsc.png' alt='CHMSC-Alijis Office of the Guidance Services'><br><h5>"+title+"</h5></div>"
      },
      xAxis: {
        type: 'category',
        title: {
          text: title
        }
      },
      yAxis: {
        title: {
          text: 'Number of Students'
        }
      },
      plotOptions: {
          column: {
            dataLabels: {
              enabled: true
            },
            enableMouseTracking: true
          },
          series: {
            connectNulls: true
          }
      },

      tooltip: {
          headerFormat: '',
          pointFormat: '<span style="color:{point.color}">{point.title} </span> <span style="font-size:11px">{series.title}</span>: <b>{point.y:.0f}</b><br/>'
      },

     series: data['series']
    });

  });
}
</script>
