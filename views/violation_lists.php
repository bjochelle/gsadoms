<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Violation Lists</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <div class='row'>
                        <div class='col-md-12'>
                            <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target="#vltnModal"><span class='fa fa-plus-circle'></span> Add</button>
                            <button class='btn btn-sm btn-danger pull-right' onclick='deleteVltn()' id='deleteVltn'><span class='fa fa-trash'></span> Delete</button>
                        </div>
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='violations' class="table table-bordered table-hover" style='margin-top:10px;'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th></th>
                                        <th>#</th>
                                        <th></th>
                                        <th>VIOLATION</th>
                                        <th>LEVEL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>
<script>
$(document).ready( function(){
    violationLists();
});
function addNewvltn(){
    var violationName = $("#violationName").val();
    var level = $("#levelID").val();
    var action = 'add';
    $("#btn_add_vltn").prop("disabled", true);
    $("#btn_add_vltn").html("<span class='fas fa-spin fa-spinner'></span> Loading");
    $.post("ajax/violation_functions.php", {
        violationName: violationName,
        level: level,
        action: action
    }, function(data){
        $("#vltnModal").modal('hide');
        if(data == 1){
            alert("Successfully Added");
        }else if(data == 2){
            alert("Already Existed");
        }else{
            alert("Error");
        }
        violationLists();
        $("#violationName").val("");
        $("#btn_add_vltn").prop("disabled", false);
        $("#btn_add_vltn").html("<span class='fas fa-check-circle'></span> Continue");
    });
}
function viewRecord(vltnID,vltnName,vltnLevel){
    $("#vltnModalU").modal('show');
    $("#vltnName").val(vltnName);
    $("#vltnID").val(vltnID);
}
function updateVltn(){
    var vltnName = $("#vltnName").val();
    var vltnID = $("#vltnID").val();
    var action = 'update';
    $("#btn_update_vltn").prop("disabled", true);
    $("#btn_update_vltn").html("<span class='fas fa-spin fa-spinner'></span> Loading");
    $.post("ajax/violation_functions.php", {
        vltnName: vltnName,
        vltnID: vltnID,
        action: action
    }, function(data){
        $("#vltnModalU").modal('hide');
        if(data == 1){
            alert("Successfully Added");
        }else if(data == 2){
            alert("Already Existed");
        }else{
            alert("Error");
        }
        violationLists();
        $("#btn_update_vltn").prop("disabled", false);
        $("#btn_update_vltn").html("<span class='fas fa-check-circle'></span> Continue");
    });
}
function deleteVltn(){
    var count_checked = $('input[name="checkbox_vltn"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        var action = 'delete';
        $("#deleteVltn").prop("disabled", true);
        $("#deleteVltn").html("<span class='fas fa-spin fa-spinner'></span> Loading");
        $.post("ajax/violation_functions.php", {
            action: action,
            checked_array: count_checked
        }, function(data){
            if(data > 0){
                alert("Successfully Deleted");
            }else{
                alert("Error");
            }
            violationLists();
            $("#deleteVltn").prop("disabled", false);
            $("#deleteVltn").html("<span class='fas fa-trash'></span> Delete");
        });
}
function violationLists(){
    $("#violations").DataTable().destroy();
    $('#violations').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/violations.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "mRender": function(data,type,row){
                return "<input type='checkbox' name='checkbox_vltn' value='"+row.violationID+"'>";		
            }
        },
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"violation"
        },
        {
            "data":"level"
        }
        
    ]   
    });
}
</script>