<div class="modal fade" id="moveUP" tabindex="-1" role="dialog" aria-labelledby="usersModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-arrow-up'></span> Move up</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Course: </div>
                            <select id='programMoveUp' class='form-control'>
                                <?php echo GETALLPROGRAMS();?>
                            </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Year: </div>
                            <select id='year_levelMoveUp' class='form-control'>
                                <option value=""> &mdash; Please Choose &mdash;</option>
                                <option value='1'> First Year </option>
                                <option value='2'> Second Year</option>
                                <option value='3'> Third Year</option>
                                <option value='4'> Fourth Year</option>
                                <option value='5'> Fifth Year</option>
                            </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Section: </div>
                        <select id='sectionMoveUp' class='form-control'>
                            <option value=""> &mdash; Please Choose &mdash;</option>
                            <option value='A'> A </option>
                            <option value='B'> B</option>
                            <option value='C'> C</option>
                            <option value='D'> D</option>
                            <option value='E'> E </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_move" type="button" onclick='MoveingUp()'><span class="fas fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>