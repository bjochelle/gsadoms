<div class="modal fade" id="uploadList" tabindex="-1" role="dialog" aria-labelledby="uploadListLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-upload'></span> Upload Student List</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            	<form method='post' action='' enctype='multipart/form-data' id="form_upload_file">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> File: </div>
                        <input type="file" id="entry_file" name="entry_file" class="form-control">
                    </div>
                </div>
<!-- 
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> Level: </div>
                        <select name='studentLevel' class='form-control'>
                        	<option value=""> &mdash; Please Choose &mdash; </option>
                        	<option value="1"> 1st Year </option>
                        	<option value="2"> 2nd Year </option>
                        	<option value="3"> 3rd Year </option>
                        	<option value="4"> 4th Year </option>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> Course: </div>
                        <select name='studentCourse' class='form-control'>
                        	<?php echo GETALLPROGRAMS() ?>
                        </select>
                    </div>
                </div> -->
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_update_vltn" type="submit"><span class="fas fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
				</form>
            </div>
        </div>
    </div>
</div>