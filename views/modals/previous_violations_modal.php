<div class="modal fade" id="prevModal" tabindex="-1" role="dialog" aria-labelledby="prevModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-plus-circle'></span> Previous Violations </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
              <div class='col-md-12' style='margin-top:10px;'>
                <table id='prevviolations' class="table table-bordered table-hover" style='margin-top:10px;width: 100%;'>
                    <thead style='background-color: #343940;color: white;'>
                        <tr>
                            <th>#</th>
                            <th>VIOLATION</th>
                            <th>PENALTY</th>
                            <th>DATE</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
             </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>