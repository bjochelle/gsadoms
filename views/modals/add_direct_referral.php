<div class="modal fade" id="addDirectCounseling" tabindex="-1" role="dialog" aria-labelledby="addDirectCounselingLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-plus-circle'></span> Set Counseling Schedule</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="input-group" >
                        <div class="input-group-addon">Student: </div>
                           <select id='studentID' style='width:81%' class='form-control select2'>
                              <?php echo student_lists() ?>

                           </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Date: </div>
                        <input type="date" id="dcDate" name="dcDate" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Time: </div>
                        <input type="time" id="dcTime" name="dcTime" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> Concern: </div>
                        <select id='dcViolation' class='form-control'>
                            <?php echo getViolations(); ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_class" type="button" onclick='setReferalSchedDirect()'><span class="fas fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
            </div>
        </div>
    </div>
</div>