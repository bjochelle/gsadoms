<div class="modal fade" id="viewStudentProfile" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-eye'></span> View/Update Profile</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="modal_body">
                
            </div>
            <div class="modal-footer">
                <span class="btn-group">
                    <button class="btn btn-default btn-sm" onclick='printDiv()'><span class="fa fa-print"></span> Print</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>