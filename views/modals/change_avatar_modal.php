<form action="" method='POST' id='avatarUpload'>
<div class="modal fade" id="changeAvatar" tabindex="-1" role="dialog" aria-labelledby="changeCredLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-edit'></span> Change Avatar </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <center>
            <div class="col-md-12" style="">
                <img id="img_wrap" class="previewImage01 image-wrap" src="assets/images/avatar.png" width="150" height="150" style="object-fit: cover;border-radius: 50%;border:3px solid #b7b1b1;height: 200px;width: 200px;">
                <div class="image-upload" style="margin-top: 5px;margin-left: 30px;">
                <input type="file" style='visibility:hidden' name="avatar" id="files" class="btn-inputfile share" />
                    <label for="files" class="btn default" style="font-size: 16px;margin-right: 16px;margin-top: -40px;"><i class="fa fa-file-image-o"></i> Change</label>
                </div>
            </div>
            </center>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_avatar" type="submit"><span class="fas fa-check-circle"></span> Save Changes</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
            </div>
        </div>
    </div>
</div>
</form>