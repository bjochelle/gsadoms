<div class="modal fade" id="vltnModal" tabindex="-1" role="dialog" aria-labelledby="vltnModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-plus-circle'></span> Add Violation</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> Violation: </div>
                        <input type="text" id="violationName" name="violationName" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> Level of Violation: </div>
                        <select class='form-control' id='levelID'>
                        <?=getViolationLevels()?>

                        </select>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_vltn" type="button" onclick='addNewvltn()'><span class="fas fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
            </div>
        </div>
    </div>
</div>