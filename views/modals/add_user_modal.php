<div class="modal fade" id="usersModal" tabindex="-1" role="dialog" aria-labelledby="usersModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-plus-circle'></span> Add User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                 <label style='color: red'>Note: Program is only for chairperson leave blank if not.</label>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Name: </div>
                        <input type="text" id="username" name="username" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Email: </div>
                        <input type="email" id="emailAddress" name="emailAddress" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Category: </div>
                        <select class='form-control' id='categ' onchange="categoryChecker()">
                            <?php echo GETCATEGORIES()?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Program (<span style='color: red'>Optional</span>): </div>
                        <select class='form-control' id='programs' disabled>
                            <?php echo GETALLPROGRAMS()?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_type" type="button" onclick='addNewUser()'><span class="fas fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>