<div class="modal fade" id="updateClass" tabindex="-1" role="dialog" aria-labelledby="updateClassLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-edit'></span> Update Classification</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Classification</div>
                        <input type="text" id="classificationName" name="classificationName" class="form-control">
                        <input type="hidden" id="classificationID" name="classificationID" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_update_class" type="button" onclick='UpdateClass()'><span class="fas fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
            </div>
        </div>
    </div>
</div>