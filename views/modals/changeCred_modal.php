<div class="modal fade" id="changeCred" tabindex="-1" role="dialog" aria-labelledby="changeCredLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-edit'></span> Update Credentials </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> New Username: </div>
                        <input type="text" id="newuser" name="newuser" class="form-control"">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> Old Password: </div>
                        <input type="password" id="oldPass" name="oldPass" class="form-control" onkeyup="checkOldPass()">
                        <div class="input-group-addon"> <span id='iconResult'></span> </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> New Password: </div>
                        <input type="password" id="newPass" name="newPass" class="form-control" readonly>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> Confirm Password: </div>
                        <input type="password" id="confPass" name="confPass" onkeyup="checkconfPass()" class="form-control" readonly>
                        <div class="input-group-addon"> <span id='iconResultc'></span> </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" disabled id="btn_cred" type="button" onclick='updateCred()'><span class="fas fa-check-circle"></span> Save Changes</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
            </div>
        </div>
    </div>
</div>