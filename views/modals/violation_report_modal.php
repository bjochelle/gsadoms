<div id="violation_report" class="modal fade" role="dialog" data-backdrop='static'>
	<div class="modal-dialog modal-lg" style="width: 1000px">

		<div class="modal-content">
			<div class="modal-header">
			</div>
			
			<div class="modal-body">
				<div id='loader1' style='text-align: center;font-size: 2.5rem;'><span class='fa fa-spin fa-spinner'></span> Loading Data, Please Wait </div>
				<iframe id="JOframe1" name="JOframe1" width="100%" height="500px" style="margin-bottom:-10px;" frameborder="0">
				</iframe>
				<div class="modal-footer input-group-btn">
					<span class="btn-group" role="group">
						 <button type="button" class="btn btn-primary" data-dismiss="modal" onclick="printIframe('JOframe1');">Print</button>
	                     <button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
					</span>
				</div>
			</div>

		
	</div>
</div>
</div>
