<div class="modal fade" id="schedModal" tabindex="-1" role="dialog" aria-labelledby="schedModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-plus-circle'></span> Schedule for Counseling</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <!-- <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> Date: </div>
                        <input type="date" id="cnclngDate" name="cnclngDate" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> Time: </div>
                        <input type="time" id="cnclngTime" name="cnclngTime" class="form-control">
                    </div>
                </div> -->
                <input type="hidden" id="studentID" name="studentID" class="form-control">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> Concern: </div>
                        <select id='violationID' class='form-control'>
                            <?php echo getViolations(); ?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_ssc" type="button" onclick='saveSchedCnclng()'><span class="fas fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>