<div class="modal fade" id="selModal" tabindex="-1" role="dialog" aria-labelledby="selModalULabel" data-backdrop='static'>
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-plus-circle'></span> Scheduled Exams</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class='row'>
                    <div class='col-md-12'>
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">Date:</div>
                                <input type="date" id="examDate" class="form-control" style='max-width: 35%' value='<?php echo date("Y-m-d", strtotime(getCurrentDate()))?>' onchange='examScheduleDate()'>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-12' style='margin-top:10px;'>
                        <table id='students_stnn' class="table table-bordered table-hover" style='margin-top:10px;width:100%'>
                            <thead style='background-color: #343940;color: white;'>
                                <tr>
                                    <th>#</th>
                                    <th style='width: 180px;'>APPLICANT NAME</th>
                                    <th>SCORE</th>
                                    <th>STANINE</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_save_exam" type="button" onclick='saveExamScore()'><span class="fas fa-check-circle"></span> Save</button>
				</span>
            </div>
        </div>
    </div>
</div>