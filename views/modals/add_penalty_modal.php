<div class="modal fade" id="penaltyModal" tabindex="-1" role="dialog" aria-labelledby="penaltyModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-plus-circle'></span> Add Penalty</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Level: </div>
                        <select class='form-control' id="levelOfOffense">
                            <option value=""> &mdash; Please Choose &mdash; </option>
                            <option value="1"> 1st Offense </option>
                            <option value="2"> 2nd Offense </option>
                            <option value="3"> 3rd Offense </option>
                            <option value="4"> 4th Offense </option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Name: </div>
                        <input type="text" id="penName" name="penName" class="form-control">
                    </div>
                </div>


            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_pen" type="button" onclick='addNewPen()'><span class="fas fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>