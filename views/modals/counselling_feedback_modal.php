<div class="modal fade" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="feedbackLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-pencil'></span> Counseling Feedback</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> Action Taken: </div>
                        <textarea name="actionTaken" id="actionTaken" rows="4" class='form-control' placeholder="Action/s Taken"></textarea>
                        <input type="hidden" id='counsellingID'>
                        <input type="hidden" id='CstID'>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon"> Remarks: </div>
                        <textarea name="remarks" id="remarks" rows="4" class='form-control' placeholder="Remarks"></textarea>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_ssc" type="button" onclick='saveCnclngFeedback()'><span class="fas fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>