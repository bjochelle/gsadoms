<div class="modal fade" id="addEventModal" tabindex="-1" role="dialog" aria-labelledby="addEventModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-plus-circle'></span> Add Event / Reminders</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Title: </div>
                        <input type="text" class="form-control" autocomplete='off' id="eventName" name="eventName" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Date: </div>
                        <input type="text" class="form-control" autocomplete='off' id="daterange" name="addEvent_date" required>
                    </div>
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                    <span class="input-group-text"><strong> Time:</strong></span>
                    </div>
                    <input type="time" class="form-control" id="addEvent_time" name="addEvent_time" required>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_type" type="button" onclick='addEvent()'><span class="fas fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>