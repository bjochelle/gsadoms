<div class="modal fade" id="counselingSched" tabindex="-1" role="dialog" aria-labelledby="counselingSchedLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-plus-circle'></span> Set Counseling Schedule</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Date: </div>
                        <input type="date" id="counselingDate" name="counselingDate" class="form-control">
                        <input type="hidden" id="scID" name="scID" class="form-control">
                        <input type="hidden" id="studID" name="studID" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Time: </div>
                        <input type="time" id="counselingTime" name="counselingTime" class="form-control">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_class" type="button" onclick='setReferalSched()'><span class="fas fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Cancel</button>
				</span>
            </div>
        </div>
    </div>
</div>