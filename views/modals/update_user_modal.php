<div class="modal fade" id="updateUserModal" tabindex="-1" role="dialog" aria-labelledby="usersModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-edit'></span> Update User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                 <label style='color: red'>Note: Program is only for chairperson leave blank if not.</label>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Name: </div>
                        <input type="text" id="updt_username" name="updt_username" class="form-control">
                        <input type="hidden" id="updt_userID" name="updt_userID" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Email: </div>
                        <input type="email" id="updt_emailAddress" name="updt_emailAddress" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Category: </div>
                        <select class='form-control' id='updt_categ' onchange="categoryChecker()">
                            <?php echo GETCATEGORIES()?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Program (<span style='color: red'>Optional</span>): </div>
                        <select class='form-control' id='updt_programs'>
                            <?php echo GETALLPROGRAMS()?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_update_users" type="button" onclick='updateUser()'><span class="fas fa-check-circle"></span> Continue</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>