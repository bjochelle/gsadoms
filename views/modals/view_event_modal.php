<div class="modal fade" id="viewEvent" tabindex="-1" role="dialog" aria-labelledby="viewEventLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-eye'></span> View Event / Reminders</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Title: </div>
                        <input type="text" class="form-control" autocomplete='off' id="eventNameV" name="eventNameV" readonly>
                    </div>
                </div>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Date: </div>
                        <input type="text" class="form-control" autocomplete='off' id="eventDateV" name="eventDateV" readonly>
                    </div>
                </div>
                <div class="input-group">
                    <div class="input-group-prepend">
                    <span class="input-group-text"><strong> Time:</strong></span>
                    </div>
                    <input type="time" class="form-control" id="event_timeV" name="event_timeV" readonly>
                </div>
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>