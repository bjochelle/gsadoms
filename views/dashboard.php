<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Dashboard</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <div class='row'>
                        <?php if($category_id == 0){ ?>
                          <div class='col-md-6'>
                            <div class="au-card m-b-30" style='box-shadow: 3px 3px 7px 1px;'>
                                <div class="au-card-inner">
                                <label for="" style='font-weight: bolder;'> Scheduled Counseling for today: </label>
                                <ol>
                                   <?php echo getTodayCounselling() ?>
                                </ol>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="au-card m-b-30" style='box-shadow: 3px 3px 7px 1px;'>
                                <div class="au-card-inner">
                                <label for="" style='font-weight: bolder;'> Scheduled Exam For Today: </label>
                                <ol>
                                    <?php echo getTodayExam() ?>
                                </ol>
                                </div>
                            </div>
                        </div>
                        <?php }else {  ?>
                            <div class='col-md-3'>
                            
                                    <div class="btn-group col-md-12">
                                    <button class='btn btn-success btn-sm pull-right btn-block' onclick='addEventModal()'><span class='fa fa-calendar'></span> Add Event </button>
                                    </div>

                                    <div class="card" style='margin-top: 10px;box-shadow: 1px 2px 3px #ababab;'>
                                    <div class="card-body" id="calendarTrash">
                                        <div>
                                        <center><span class="fas fa-trash" style="font-size: 2em;color: red;"></span></center>
                                        </div>
                                    </div>
                                    </div>
                              
                            </div>
                            
                          <div class='col-md-9' id='calendarDiv'>
                          
                            <div id="calendar"></div>
                          </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>

<?php 
$events = mysql_query("SELECT * FROM tbl_events WHERE event_added_by = '$user_id'");
$count_events = mysql_num_rows($events);
?>
<script>
function addEventModal(){
    $("#addEventModal").modal();
}
function addEvent(){
    var eventName = $("#eventName").val();
    var daterange = $("#daterange").val();
    var addEvent_time = $("#addEvent_time").val();
    if(eventName == '' || daterange == '' || addEvent_time == ''){
        alert("Please Fill all fields.");

    }else{
        $.post("ajax/addEvent.php", {
            eventName: eventName,
            daterange: daterange,
            addEvent_time: addEvent_time
        }, function(data){
            if(data > 0){
                alert("Successfully Added");
                window.location.reload();
            }else{
                alert("Error");
            }
        });
    }
}
function updateDroppedEvent(change_date,end_change_date,id){
    $.post("ajax/update_drop_event.php", {
        change_date: change_date,
        end_change_date: end_change_date,
        id: id
    }, function(data){
        if(data > 0){
            alert("Successfully Moved");
        }else{
            alert("Error");
        }
        calendarEvents();
    });
}
function deleteEvent(event_id){
    $.post("ajax/delete_dropped_event.php", {
        event_id: event_id
    }, function(data){
        if(data > 0){
            alert("Successfully Deleted");
            window.location.reload();
        }else{
            alert("Error");
        }
    })
}
function updateResizedEvent(resized_date,event_id){
    $.post("ajax/update_resized_event.php", {
        resized_date: resized_date,
        event_id: event_id
    }, function(data){
        if(data > 0){
            alert("Successfully Updated");
        }else{
            alert("Error");
        }
    });
}
 $(document).ready( function(){
    calendarEvents();
  });
  function calendarEvents(){
    $('#calendar').fullCalendar({
      
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'month,basicWeek,basicDay'
      },
      
      defaultDate: '<?php echo date('Y-m-d'); ?>',
      editable: true,
      eventLimit: true,
      droppable: true,
      events: [
        <?php 
          
        $ctrEvent=1;
        while($fetch_events = mysql_fetch_array($events)){
          $event_title = $fetch_events['event_name'];
          $start_date = $fetch_events['event_start_date'];
          $end_date = date("Y-m-d", strtotime('+1 day', strtotime($fetch_events['event_end_date'])));
          $event_time = date("g:ia", strtotime($fetch_events['event_time']));
        ?>
        {
          title: '<?php echo $event_title." "."-"." ".$event_time; ?>',
          start: '<?php echo $start_date; ?>',
          end: '<?php echo $end_date?>',
          color: '#3498db',
          id: '<?php echo $fetch_events['event_id']?>',
          event_date: '<?php echo date("F d, Y", strtotime($fetch_events['event_start_date']))." - ".date("F d,Y", strtotime($fetch_events['event_end_date'])); ?>',
          event_time: '<?php echo $fetch_events['event_time']; ?>',
          event_title: '<?php echo $fetch_events['event_name']; ?>'
        }<?php 
          if($ctrEvent < $count_events){ 
            echo ",";
          }
          $ctrEvent++;
        }
        ?>
        
      ],
      eventDragStop: function(event,jsEvent) {
        var event_id = event.id;

          var trashEl = jQuery('#calendarTrash');
          var ofs = trashEl.offset();

          var x1 = ofs.left;
          var x2 = ofs.left + trashEl.outerWidth(true);
          var y1 = ofs.top;
          var y2 = ofs.top + trashEl.outerHeight(true);

          if (jsEvent.pageX >= x1 && jsEvent.pageX <= x2 &&
              jsEvent.pageY >= y1 && jsEvent.pageY <= y2) {
                var conf = confirm("Proceed to delete this event ?");
                if(conf == true){
                    deleteEvent(event_id);
                }
          }
        
      },
      eventDrop: function(event , delta , revertfunc){
        var title = event.title;
        var id = event.id;
        var change_date = event.start.format();
        var end_change_date = event.end.format();
        var conf = confirm("Continue to update this event / reminder ?");
        if(conf == true){
            
            updateDroppedEvent(change_date,end_change_date,id);
        }
      },
      eventClick: function(callEvent , jsEvent, view){
        $("#viewEvent").modal();

        $("#eventNameV").val(callEvent.event_title);
        $("#eventDateV").val(callEvent.event_date);
        $("#event_timeV").val(callEvent.event_time);

      },
      drop: function(){
        if ($('#drop-remove').is(':checked')) {
          $(this).remove();
        }
      },
      eventReceive: function(event){
       var val = $("#de").val();
       var eventval = $('#external-events .fc-event').each(function() {
                 $(this).data('event', {
		            title: $.trim($(this).text()), // use the element's text as the event title
		            stick: true, // maintain when user navigates (see docs on the renderEvent method)
		            id: $(this).attr('id')
		        });
              });
        var date_dropped = event.start.format();
        
        alert(date_dropped + " - " + id);
      },
      eventResize: function(event , delta , revertfunc){
        var resized_date = event.end.format();
        var event_t = event.title;
        var event_id = event.id;
        var conf = confirm("Continue to update this event / reminder ?");
        if(conf == true){
            updateResizedEvent(resized_date,event_id);
        }
        
      },
            eventConstraint: {
                start: moment().format('YYYY-MM-DD'),
                end: '2100-01-01'
            }
    });
  }
</script>