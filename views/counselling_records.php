<?php 
if($_GET['stID'] != ""){
    $st_id = $_GET['stID'];
}else{
    $st_id = 0;
}

?>
<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1"> Counseling Records</h2>
        </div>
        <hr>
        <input type="hidden" id='stID' value="<?php echo $st_id ?>">
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                 <div class='row'>
                    <div class='col-md-12' style='margin-top:10px;'>
                        <table id='stCounselling' class="table table-bordered table-hover" style='margin-top:10px;'>
                            <thead style='background-color: #343940;color: white;'>
                                <tr>
                                    <th>#</th>
                                    <th>CONCERN</th>
                                    <th>ACTION TAKEN</th>
                                    <th>REMARKS</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                 </div>
                </div>
            </div>
        </div>   
    </div>
</div>
<script>
$(document).ready( function(data){
    var stID = $("#stID").val();
    student_couselling_records(stID);
});
function student_couselling_records(stID){
    $("#stCounselling").DataTable().destroy();
        $('#stCounselling').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/counselling_records.php",
            "dataSrc":"data",
            "data":{
                stID: stID
            },
            "type":"POST"
        },
        "columns":[
            {
                "data":"count"
            },
            {
                "data":"concern"
            },
            {
                "data":"actionTaken"
            }
            ,
            {
                "data":"remarks"
            }
            
        ]   
        });
}
</script>