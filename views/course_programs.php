<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Programs</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                   <div class='row'>
                        <div class='col-md-12'>
                            <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target="#prgmModal"><span class='fa fa-plus-circle'></span> Add</button>
                            <button class='btn btn-sm btn-danger pull-right' onclick='deletePrgm()' id='deletePrgm'><span class='fa fa-trash'></span> Delete</button>
                        </div>
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='coursePrograms' class="table table-bordered table-hover" style='margin-top:10px;'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th></th>
                                        <th>#</th>
                                        <th></th>
                                        <th>DEPARTMENT</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                   </div>
                </div>
            </div>
        </div>   
    </div>
</div>


<script>
    $(document).ready( function(){
        programLists();
    });
    function addNewPrgm(){
        var ProgramName = $("#ProgramName").val();
        var ProgramAlias = $("#ProgramAlias").val();
        var action = 'add';
        $("#btn_add_prgm").prop("disabled", true);
        $("#btn_add_prgm").html("<span class='fas fa-spin fa-spinner'></span> Loading");
        $.post("ajax/program_functions.php", {
            ProgramName: ProgramName,
            ProgramAlias: ProgramAlias,
            action: action
        }, function(data){
            $("#prgmModal").modal('hide');
            if(data == 1){
                alert("Successfully Added");
            }else if(data == 2){
                alert("Already Existed");
            }else{
                alert("Error");
            }
            programLists();
            $("#ProgramName").val("");
            $("#btn_add_prgm").prop("disabled", false);
            $("#btn_add_prgm").html("<span class='fas fa-check-circle'></span> Continue");
        });
    }
    function viewRecord(prgmID, prgmName, prgmAlias){
        $("#updatePrgm").modal('show');
        $("#programName").val(prgmName);
        $("#programAlias").val(prgmAlias);
        $("#programID").val(prgmID);
    }
    function UpdatePrgm(){
        var programName = $("#programName").val();
        var programAlias = $("#programAlias").val();
        var programID = $("#programID").val();
        var action = 'update';
        $("#btn_update_prgm").prop("disabled", true);
        $("#btn_update_prgm").html("<span class='fas fa-spin fa-spinner'></span> Loading");
        $.post("ajax/program_functions.php", {
            programName: programName,
            programAlias: programAlias,
            programID: programID,
            action: action
        }, function(data){
            $("#updatePrgm").modal('hide');
            if(data == 1){
                alert("Successfully Updated");
            }else{
                alert("Error");
            }
            programLists();
            $("#btn_update_prgm").prop("disabled", false);
            $("#btn_update_prgm").html("<span class='fas fa-check-circle'></span> Continue");
        });
    }
    function deletePrgm(){
        var count_checked = $('input[name="checkbox_prgm"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        var action = 'delete';
        var conf = confirm("Are you sure you want to delete this?");
        if(conf == true){
            $("#deletePrgm").prop("disabled", true);
            $("#deletePrgm").html("<span class='fas fa-spin fa-spinner'></span> Loading");
            $.post("ajax/program_functions.php", {
                action: action,
                checked_array: count_checked
            }, function(data){
                if(data > 0){
                    alert("Successfully Deleted");
                }else{
                    alert("Error");
                }
                programLists();
                $("#deletePrgm").prop("disabled", false);
                $("#deletePrgm").html("<span class='fas fa-trash'></span> Delete");
            });
        }
       
    }
    function programLists(){
        $("#coursePrograms").DataTable().destroy();
        $('#coursePrograms').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/course_programs.php",
            "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_prgm' value='"+row.prgrmID+"'>";		
                }
            },
            {
                "data":"count"
            },
            {
                "data":"action"
            },
            {
                "data":"prgm"
            }
            
        ]   
        });
    }
</script>