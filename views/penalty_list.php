<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Penalties</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                   <div class='row'>
                        <div class='col-md-12'>
                            <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target="#penaltyModal"><span class='fa fa-plus-circle'></span> Add</button>
                            <button class='btn btn-sm btn-danger pull-right' onclick='deletePrgm()' id='deletePrgm'><span class='fa fa-trash'></span> Delete</button>
                        </div>
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='penalties' class="table table-bordered table-hover" style='margin-top:10px;width: 100%'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th></th>
                                        <th>#</th>
                                        <th></th>
                                        <th>LEVEL</th>
                                        <th>NAME</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                   </div>
                </div>
            </div>
        </div>   
    </div>
</div>


<script>
    $(document).ready( function(){
        penaltyLists();
    });
    function deletePrgm(){
        var count_checked = $('input[name="checkbox_appType"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        var action = 'delete';
        var conf = confirm("Are you sure you want to delete this?");
        if(conf == true){
            $("#deletePrgm").prop("disabled", true);
            $("#deletePrgm").html("<span class='fas fa-spin fa-spinner'></span> Loading");
            $.post("ajax/penalty_function.php", {
                action: action,
                checked_array: count_checked
            }, function(data){
                if(data > 0){
                    alert("Successfully Deleted");
                }else{
                    alert("Error");
                }
                penaltyLists();
                $("#deletePrgm").prop("disabled", false);
                $("#deletePrgm").html("<span class='fas fa-trash'></span> Delete");
            });
        }
        
    }
    function viewRecord(TSid, TSName){
        $("#updatePenModal").modal('show');
        $("#penName_u").val(TSName);
        $("#penid_u").val(TSid);
    }
    function updatePen(){
        var TSName = $("#penName_u").val();
        var TSId = $("#penid_u").val();
        var action = 'update';
        $("#btn_update_pen").prop("disabled", true);
        $("#btn_update_pen").html("<span class='fas fa-spin fa-spinner'></span> Loading");
        $.post("ajax/penalty_function.php", {
            TSName: TSName,
            TSId: TSId,
            action: action
        }, function(data){
            $("#updatePenModal").modal('hide');
            if(data == 1){
                alert("Successfully Updated");
            }else{
                alert("Error");
            }
            penaltyLists();
            $("#btn_update_pen").prop("disabled", false);
            $("#btn_update_pen").html("<span class='fas fa-check-circle'></span> Continue");
        });
    }
    function addNewPen(){
        var tsName = $("#penName").val();
        var levelOfOffense = $("#levelOfOffense").val();
        var action = 'add';
        $("#btn_add_pen").prop('disabled', true);
        $("#btn_add_pen").html("<span class='fas fa-spin fa-spinner'></span> Loading ");
        $.post("ajax/penalty_function.php", {
            tsName: tsName,
            action: action,
            levelOfOffense: levelOfOffense
        }, function(data){
            $("#penaltyModal ").modal('hide');
            if(data == 1){
                alert("Success");
            }else{
                alert("Failed");
            }
            $("#btn_add_pen").prop('disabled', false);
            $("#btn_add_pen").html("<span class='fas fa-check-circle'></span> Continue ");
            $("#penName").val("");
            penaltyLists();
        }); 
    }
    function penaltyLists(){
        $("#penalties").DataTable().destroy();
        $('#penalties').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/penalty_list.php",
            "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_appType' value='"+row.ts_id+"'>";     
                }
            },
            {
                "data":"count"
            },
            {
                "data":"action"
            },
            {
                "data":"level"
            },
            {
                "data":"tsName"
            }
            
        ]   
        });
    }
</script>