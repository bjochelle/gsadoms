<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Summary of Applicants</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <div class='row'>
                        <div class='col-md-12'>
                            <div id='chart' style='width:100% !important; margin-top: 20px;'>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>

<script src="assets/js/highcharts1.js"></script>
<script src="assets/js/highcharts-more.js"></script>
<script src="assets/js/exporting.js"></script>

<script type="text/javascript">

  $(document).ready(function(){
    bot_chart();
  });
  
  function bot_chart(){

  $.getJSON('ajax/chart2.php', function(data){
    Highcharts.chart("chart", {
      chart: {
        type: 'column'
      },
      title: {
        useHTML: true,
        text: "<div class='col-md-12' style='text-align:center;margin-bottom:20px'><img src='assets/images/chmsc.png' alt='CHMSC-Alijis Office of the Guidance Services'><br><h5>Summary of Applicants</h5></div>"
      },
      xAxis: {
        type: 'category',
        title: {
          text: "Summary of Applicants"
        }
      },
      yAxis: {
        title: {
          text: 'Number of Students'
        }
      },
      plotOptions: {
          column: {
            dataLabels: {
              enabled: true
            },
            enableMouseTracking: true
          },
          series: {
            connectNulls: true
          }
      },

      tooltip: {
          headerFormat: '',
          pointFormat: '<span style="color:{point.color}">{point.title} </span> <span style="font-size:11px">{series.title}</span>: <b>{point.y:.0f}</b><br/>'
      },

     series: data['series']
    });

  });
}
</script>
