<style type="text/css">

    .modal-lg{
        max-width: 100%;
    }
    @media print{
  .breakAfter{
    page-break-before: always;
  }
}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Students List</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <div class='row'>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group" >
                                    <div class="input-group-addon">Type: </div>
                                       <select id='sortingType' class='form-control' onchange="sortingType()">
                                          <option value=""> &mdash; Please Choose &mdash;</option>
                                          <option value='A'> All </option>
                                          <option value='S'> Sort </option>
                                          <option value='AC'> Archived </option>
                                       </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Course: </div>
                                       <select id='program' class='form-control' onchange="getSections()">
                                           <?php echo GETALLPROGRAMS();?>
                                       </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Year: </div>
                                       <select id='year_level' class='form-control' onchange="getSections()">
                                          <option value=""> &mdash; Please Choose &mdash;</option>
                                          <option value='1'> First Year </option>
                                          <option value='2'> Second Year</option>
                                          <option value='3'> Third Year</option>
                                          <option value='4'> Fourth Year</option>
                                          <option value='5'> Fifth Year</option>
                                       </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Section: </div>
                                       <select id='section' class='form-control'>
                                         
                                       </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-12'>
                            <div class="form-group">
                                <button class='btn btn-sm btn-danger pull-right' onclick="archiveStudent()"><span class='fa fa-archive'></span> Archive </button>
                                <!-- <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target="#moveUP"><span class='fa fa-arrow-up'></span> Move up</button> -->
                                <button class='btn btn-sm btn-success pull-right' data-toggle='modal' data-target="#uploadList"><span class='fa fa-plus-circle'></span> Upload Student List</button>
                               <button class='btn btn-primary btn-sm pull-right' onclick='genList()'><span class='fa fa-gear'></span> Generate</button>
                              
                            </div>
                        </div>
                        <div class='col-md-12' style='margin-top:10px;'>

                            <table id='students' class="table table-bordered table-hover" style='margin-top:10px;'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th></th>
                                        <th>#</th>
                                        <th></th>
                                        <th>NAME</th>
                                        <th>STATUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>
<script>
$(document).ready( function(){
    //genList();

    $("#form_upload_file").submit(function(e){
    e.preventDefault();

    // $("#btn_upload_file").prop('disabled', true);
    // $("#btn_upload_file").html("<span class='fa fa-spin fa-spinner'></span> Loading ...");
    
    $.ajax({
        url:"ajax/upload_student_list.php",
        method:"POST",
        data:new FormData(this),
        contentType:false,          // The content type used when sending data to the server.
        cache:false,                // To unable request pages to be cached
        processData:false,          // To send DOMDocument or non processed data file it is set to false
        success: function(data){
            $("#entry_file").val("");
            $("#uploadList").modal('hide');
            $("#responseModal").modal('show');
            $("#response_cont").html(data);
            studentList();
        }
    });
    });
});
function archiveStudent(){
    var count_checked = $('input[name="checkbox_st"]:checked').map(function() {
                                        return this.value;
                                    }).get();
     checked = [];
     $.post("ajax/archiveStudents.php", {
        checked: count_checked
     }, function(data){
        if(data > 0){
            alert("Successfully Archived");
            window.location.reload();
        }else{
            alert("Error");
        }
     })
}
function MoveingUp(){
    var count_checked = $('input[name="checkbox_st"]:checked').map(function() {
                                        return this.value;
                                    }).get();
     checked = [];
     var programMoveUp = $("#programMoveUp").val();
     var year_levelMoveUp = $("#year_levelMoveUp").val();
     var sectionMoveUp = $("#sectionMoveUp").val();
     var sortingType = $("#sortingType").val();
        var course = $("#program").val();
        var year = $("#year_level").val();
        var section = $("#section").val();
     if(count_checked == ''){
         alert("Please Select Student");
     }else{
         $("#btn_move").prop("disabled", true);
         $("#btn_move").html("<span class='fa fa-spin fa-spinner'></span> Loading");
         $.post("ajax/moveup_students.php", {
            checked: count_checked,
            programMoveUp: programMoveUp,
            year_levelMoveUp: year_levelMoveUp,
            sectionMoveUp: sectionMoveUp
         }, function(data){
             $("#moveUP").modal('hide');
            if(data > 0){
                alert("Successfully Updated");
            }else{
                alert("ERROR");
            }
            studentList(course, year, section, sortingType);
         });
     }
}
function sortingType(){
    var sortingType = $("#sortingType").val();
    if(sortingType == 'A'){
        $("#year_level").attr("disabled", true);
        $("#section").attr("disabled", true);
        $("#program").attr("disabled", true);
        $("#section").val("");
        $("#year_level").val("");
        $("#program").val("");
    }else{
        $("#year_level").attr("disabled", false);
        $("#section").attr("disabled", false);
        $("#program").attr("disabled", false)
    }
}
function getSections(){
    var course = $("#program").val();
    var year = $("#year_level").val();

    $.post("ajax/sectionList.php", {
        course: course,
        year: year
    }, function(data){
        $("#section").html(data);
    });
}
function genList(){
    var sortingType = $("#sortingType").val();
    var course = $("#program").val();
    var year = $("#year_level").val();
    var section = $("#section").val();
    studentList(course, year, section, sortingType);
}
function inactiveStat(userID){
    
    $.post("ajax/user_function.php", {
        userID: userID,
        action: action
    }, function(data){
        if(data == 1){
            alert("Success");
        }else{
            alert("Failed");
        }
        userList();
    });
}
function viewProfile(studentID){
    $("#viewStudentProfile").modal('show');


    $.post('ajax/student_profile_data.php',{
        stdID:studentID
    },function(data){
        $("#modal_body").html(data);
         $("#second_form").hide();

    }
    )
    // window.location = 'index.php?page=student-profile&stdID='+studentID;
}
function viewCounselling(id){
    window.location = 'index.php?page=counselling-records&stID='+id;
}
function studentList(course, year, section, sortingType){
    $("#students").DataTable().destroy();
    $('#students').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/students.php",
        "dataSrc":"data",
        "data":{
          course: course,
          year: year,
          section: section,
          sortingType: sortingType
        },
        "type": "POST"
    },
    "columns":[
        {
            "mRender": function(data,type,row){
                return "<input type='checkbox' name='checkbox_st' value='"+row.stdID+"'>";		
            }
        },
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"name"
        },
        {
            "data":"status"
        }
        
    ]   
    });
}

   function printDiv() 
{

    backForm();
    if($('#first_form:visible').length == 0)
    {
        $("#first_form").css('display','block');
    }else{
        $("#second_form").css('display','block');
    }

$("#modal_body .btn-sm").hide();

  var divToPrint=document.getElementById('modal_body');

  var newWin=window.open('','Print-Window');

  newWin.document.open();

  newWin.document.write('<html><link href="assets/css/bootstrap.min.css" rel="stylesheet" media="all"> <link href="assets/css/theme.css" rel="stylesheet" media="all"><body onload="window.print()">'+divToPrint.innerHTML+'</body></html>');


  newWin.document.close();

  setTimeout(function(){

     if($('#first_form:visible').length == 0)
    {
        $("#first_form").css('display','none');
    }else{
        $("#second_form").css('display','none');
    }

    $("#modal_body .btn-sm").show();
    newWin.close();


  },10);

}
</script>