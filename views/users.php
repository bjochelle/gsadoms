<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Users List</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <div class='row'>
                        <div class='col-md-12'>
                            <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target="#usersModal"><span class='fa fa-plus-circle'></span> Add</button>
                        </div>
                        <div class='col-md-12' style='margin-top:10px;overflow: auto;'>
                            <table id='users' class="table table-bordered table-hover" style='margin-top:10px;'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th>#</th>
                                        <th></th>
                                        <th>NAME</th>
                                        <th>USERNAME</th>
                                        <th>EMAIL</th>
                                        <th>CATEGORY</th>
                                        <th>PROGRAM</th>
                                        <th>STATUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>
<script>
$(document).ready( function(){
    userList();
});
function categoryChecker(){
    var categ = $("#categ").val();
    $.post("ajax/check_category.php", {
        categ: categ
    }, function(data){
        (data == 'C')? $("#programs").attr("disabled", false) :  $("#programs").attr("disabled", true), $("#programs").val("");
    })
}
function addNewUser(){
    var username = $("#username").val();
    var categ = $("#categ").val();
    var program = $("#programs").val();
    var email = $("#emailAddress").val();
    var action = 'add';
    $.post("ajax/user_function.php", {
        username: username,
        categ: categ,
        program: program,
        action: action,
        email: email
    }, function(data){
        if(data == 1){
            alert("Success");
        }else if(data == 2){
            alert("Already Exist");
        }else{
            alert("Failed");
        }
        $("#usersModal").modal('hide');
        userList();
    });
}
function inactiveStat(userID){
    var action = 'status';
    $.post("ajax/user_function.php", {
        userID: userID,
        action: action
    }, function(data){
        if(data == 1){
            alert("Success");
        }else{
            alert("Failed");
        }
        userList();
    });
}
function updateRecord(id){
    $("#updateUserModal").modal();
    $.post("ajax/datatables/getUserDetails.php", {
        id: id
    }, function(data){
        var x = JSON.parse(data);
        $("#updt_username").val(x.name);
        $("#updt_emailAddress").val(x.email);
        $("#updt_categ").val(x.category);
        $("#updt_programs").val(x.program);
        $("#updt_userID").val(x.id);
    });
}
function updateUser(){
    var name = $("#updt_username").val();
    var emailAdd = $("#updt_emailAddress").val();
    var cat = $("#updt_categ").val();
    var prgm = $("#updt_programs").val();
    var userid = $("#updt_userID").val();
    $("#btn_update_users").prop("disabled", true);
    $("#btn_update_users").html("<span class='fa fa-spin fa-spinner'></span> Loading");
    $.post("ajax/updateuserdetails.php", {
        name: name,
        emailAdd: emailAdd,
        cat: cat,
        prgm: prgm,
        userid: userid
    }, function(data){
        if(data > 0){
            alert("Successfully Updated");
            $("#updateUserModal").modal('hide');
        }else{
            alert("Error");
        }
        userList();
        $("#btn_update_users").prop("disabled", false);
        $("#btn_update_users").html("<span class='fa fa-check-circle'></span> Continue");
    });
}
function userList(){
    $("#users").DataTable().destroy();
    $('#users').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/users.php",
        "dataSrc":"data"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"name"
        },
        {
            "data":"username"
        },
        {
            "data":"email"
        },
        {
            "data":"category"
        },
        {
            "data":"program"
        },
        {
            "data":"status"
        }
        
    ]   
    });
}
</script>