<?php 
include '../../core/config.php';
$sdate = $_GET['sDate'];
$edate = $_GET['eDate'];
?>
<link href="../../assets/css/bootstrap.min.css" rel="stylesheet" media="all">
<script src="../../assets/js/jquery.min.js"></script>
<style>
@media print{
		body * {
			-webkit-print-color-adjust: exact;
		}
		thead * {
			background-color: #4e6883 !important;
			color: #fff !important;
		}
		tfoot * {
			background-color: #4e6883 !important;
			color: #fff !important;
		}
	}
</style>
<div class='col-md-12' style='text-align:center;margin-top:50px'>
        <img src="../../assets/images/chmsc.png" alt="CHMSC-Alijis Office of the Guidance Services" />
    </div>
    <div class='col-md-12' style='text-align:center;'><h5>OFFICE OF THE GUIDANCE SERVICES</h5></div><br><br>
    <div class='col-md-12' style='text-align:center;'><h6>Application Report</h6></div><br>
<div class='col-md-12' style='margin-top:10px;'>
    <table id='applicationReport' border='1' cellpadding='1' cellspacing='1' class="" style='margin-top:10px;width:100%'>
        <thead style='background-color: #343940 !important;color: white !important;'>
            <tr>
                <th style='text-align: center !important'>#</th>
                <th style='text-align: center !important'>APPLICANT NAME</th>
                <th style='text-align: center !important'>SCORE</th>
                <th style='text-align: center !important'>STANINE</th>
                <th style='text-align: center !important'>STATUS</th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $query = mysql_query("SELECT * FROM tbl_applicants WHERE (`status` = 1 || `status` = 2) AND DATE_FORMAT(date_added, '%Y-%m-%d') BETWEEN '$sdate' AND '$edate'");
                $count = 1;
                while($row = mysql_fetch_array($query)){
            ?>
                <tr>
                    <td><?php echo $count++; ?></td>
                    <td><?php echo $row['lastname'].', '.$row['firstname'].' '.$row['middlename']; ?></td>
                    <td><?php echo $row['score']; ?></td>
                    <td><?php echo $row['stanine']; ?></td>
                    <td><?php echo ($row['status'] == 1)?"<span style='color: green'>PASSED</span>":"<span style='color: red'>FAILED</span>";?></td>
                </tr>

            <?php } ?>
        </tbody>
    </table>
</div>

<script>
$(document).ready( function(){
    // $('#applicationReport').dataTable();
    //print();
})
function printPage(){ 
        print();
    }
</script>