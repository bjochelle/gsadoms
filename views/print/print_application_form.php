<?php 
include('../../core/config.php');
$applicant_id = $_GET['applicant_id'];

$getDetails = mysql_fetch_array(mysql_query("SELECT * FROM tbl_applicants as a, tbl_track_and_strand as b, tbl_program as c WHERE a.applicant_id = '$applicant_id' AND a.strand = b.ts_id AND c.program_id = a.preferred_course"));
?>
<link href="../../assets/css/font-face.css" rel="stylesheet" media="all">
    <link href="../../assets/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="../../assets/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="../../assets/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="../../assets/css/animsition.min.css" rel="stylesheet" media="all">
    <link href="../../assets/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="../../assets/css/animate.css" rel="stylesheet" media="all">
    <link href="../../assets/css/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="../../assets/css/slick.css" rel="stylesheet" media="all">
    <link href="../../assets/css/select2.min.css" rel="stylesheet" media="all">
    <link href="../../assets/css/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link rel="stylesheet" type="text/css" href="../../assets/css/sweetalert.css">
    <link href="../../assets/css/imagePreview.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../../assets/css/theme.css" rel="stylesheet" media="all">

    <script src="../../assets/js/jquery.min.js"></script>
<style>
@media print{
		body * {
			-webkit-print-color-adjust: exact;
		}
		.modal-body{
			page-break-after: always;
		}
		@page  
		{ 
		    margin: 0;  
		} 
	}

}

</style>

	<div class='col-md-12' style='text-align:center;margin-top:100px'>
		<img src="../../assets/images/chmsc.png" alt="CHMSC-Alijis Office of the Guidance Services" />
    </div>
	<div class='col-md-12' style='text-align:center;'><h5>OFFICE OF THE GUIDANCE SERVICES</h5></div><br><br>
	<div class='col-md-12' style='text-align:center;'><h6><?php echo FORMTITLE('EEF')?></h6></div><br>

	<div class='col-md-12' style='padding-left: 50PX !important;'>
	    <h5>APPLICANT'S INFORMATION</h5>
	</div>
	<div class="row" style='padding: 0px 30px 0px 30px !important;'>
		<div class='col-md-9'>
			<div class='row'>
			<div class='col-md-12'> 
				<div class="form-group">
		            <div class="input-group">
		                <div class="input-group-addon">Name: </div>
		                <input type="text" class="form-control" value='<?php echo $getDetails['firstname']?>'>
		                <input type="text" class="form-control" value='<?php echo $getDetails['middlename']?>'>
		                <input type="text" class="form-control" value='<?php echo $getDetails['lastname']?>'>
		            </div>
		        </div>
			</div>
			<div class='col-md-6'>
		        <div class="form-group">
		            <div class="input-group">
		                <div class="input-group-addon">Semester : </div>
		                <div class="input-group-addon"> <input type="checkbox" value='First' <?php echo ($getDetails['semester'] == 'First')?"checked":""?> > First </div>
		                <div class="input-group-addon"> <input type="checkbox" value='Second' <?php echo ($getDetails['semester'] == 'Second')?"checked":""?> > Second </div>
		            </div>
		        </div>
		    </div>
		    <div class='col-md-6'>
		        <div class="form-group">
		            <div class="input-group">
		                <div class="input-group-addon">Academic Year: </div>
		               	<input type="text" class='form-control' value='<?php echo $getDetails['acad_year_from']?>'>
		                <div class='input-group-addon'> to </div>
		                <input value='<?php echo $getDetails['acad_year_to']?>' type="text" class='form-control'>
		            </div>
		        </div>
		    </div>
		    <div class='col-md-12'>
		        <div class="form-group">
		            <div class="input-group">
		                <div class="input-group-addon">Applicant Type : </div>
		                <?php 
		                    $data = "";
						    $count = 1;
						    $query = mysql_query("SELECT * FROM tbl_applicant_type");
						    while($row = mysql_fetch_array($query)){
						        $id = $row['applicant_type_id'];
						        $br = ($count % 4 == 0)?'<br>':'';
						        $checked = ($row['applicant_type_id'] == $getDetails['applicant_type'])?"checked":"";
						        echo '<div class="input-group-addon"> <input '.$checked.' type="checkbox" value="'.$id.'" class="applicantType"> '.$row["type_name"].' </div>';
						        $count++;
						    }
		                ?>
		            </div>
		        </div>
		    </div>
		    </div>
		</div>	
		<div class='col-md-3'>
		   <img id="img_wrap" alt='Recent 2x2 Picture' class="previewImage01 image-wrap" src="../../assets/images/<?php echo $getDetails['applicant_2x2']?>" style="object-fit: cover;border:3px solid #b7b1b1;width:2in;height:2in;">
		</div>
		<div class='col-md-5'>
		    <div class="form-group">
		        <div class="input-group">
		            <div class="input-group-addon">Birthdate: </div>
		            <input type="text" class="form-control"  value='<?php echo date("F d, Y", strtotime($getDetails['acad_year_from']))?>' >
		        </div>
		    </div>
		</div>
		<div class='col-md-7'>
		    <div class="form-group">
		        <div class="input-group">
		            <div class="input-group-addon">Last School Attended: </div>
		            <textarea rows='2' style='resize: none;' class='form-control'><?php echo $getDetails["last_school"] ?></textarea>
		        </div>
		    </div>
		</div>
		<div class='col-md-4'>
		    <div class="form-group">
		        <div class="input-group">
		            <div class="input-group-addon">Sex at Birth: </div>
		            <div class="input-group-addon"> <input <?php echo ($getDetails['gender'] == 'M')?"checked":""?> type="checkbox" id="stSex" placeholder='' name="stSex" class="stSex" value='M'> Male </div>
		            <div class="input-group-addon"> <input <?php echo ($getDetails['gender'] == 'F')?"checked":""?> type="checkbox" id="stSex" placeholder='' name="stSex" class="stSex" value='F'> Female </div>
		        </div>
		    </div>
		</div>
		<div class='col-md-4'>
		    <div class="form-group">
		        <div class="input-group">
		            <div class="input-group-addon">Learner's Reference #: </div>
		            <input type="text" id="stLRN" placeholder='' name="stLRN" class="form-control" value='<?php echo $getDetails["LRN"]?>'>
		        </div>
		    </div>
		</div>
		<div class='col-md-4'>
		    <div class="form-group">
		        <div class="input-group">
		            <div class="input-group-addon">Track and Strand: </div>
		            <textarea rows='2' style='resize: none;' class='form-control'><?php echo $getDetails["ts_name"]?></textarea>
		        </div>
		    </div>
		</div>
		<div class='col-md-4'>
		    <div class="form-group">
		        <div class="input-group">
		            <div class="input-group-addon">Age: </div>
		            <input type="text" class="form-control"  value='<?php echo $getDetails["age"]?>' >
		        </div>
		    </div>
		</div>
		<div class='col-md-4'>
		    <div class="form-group">
		        <div class="input-group">
		            <div class="input-group-addon" >Cellphone #: </div>
		            <input type="text" id="stCPNo" placeholder='' name="stCPNo" class="form-control" value='<?php echo $getDetails["cp_no"]?>'  >
		        </div>
		    </div>
		</div>
		<div class='col-md-4'>
		    <div class="form-group">
		        <div class="input-group">
		            <div class="input-group-addon">Email Address: </div>
		            <textarea rows='2' style='resize: none;' class='form-control'><?php echo $getDetails["email"]?></textarea>
		        </div>
		    </div>
		</div>
		<div class='col-md-4'>
		   
		</div>
		<div class='col-md-4'>
		    <div class="form-group">
		        <div class="input-group">
		            <div class="input-group-addon">Preferred Course: </div>
		             <textarea rows='2' style='resize: none;' class='form-control'><?php echo $getDetails["program_name"]?></textarea>
		        </div>
		    </div>
		</div>
		<div class='col-md-4'>
		    <div class="form-group">
		        <div class="input-group">
		            <div class="input-group-addon">Campus Intended to Enroll: </div>
		            <textarea rows='2' style='resize: none;' class='form-control'><?php echo $getDetails["campus_intended"]?></textarea>
		        </div>
		    </div>
		</div>
		<div class='col-md-12'>
		    <div class="form-group">
		        <div class="input-group">
		            <div class="input-group-addon">Complete Permanent Address: </div>
		             <textarea rows='2' style='resize: none;' class='form-control'><?php echo $getDetails["address_street"]?></textarea>

		             <textarea rows='2' style='resize: none;' class='form-control'><?php echo $getDetails["address_brgy"]?></textarea>

		             <textarea rows='2' style='resize: none;' class='form-control'><?php echo $getDetails["address_city"]?></textarea>

		             <textarea rows='2' style='resize: none;' class='form-control'><?php echo $getDetails["address_province"]?></textarea>
		        </div>
		    </div>
		</div>
		<div class='col-md-6'></div>
		<div class='col-md-6'>
		    <label class="checkbox-inline">
		        <input checked type="checkbox" value="" checked id='checkToRegister'> <i> I hereby certify that the above information is true and correct</i>
		    </label>
		</div>
	</div>


<script>
$(document).ready( function(){
    // $('#applicationReport').dataTable();
    //print();
});
function printPage(){ 
		print();
	}
</script>