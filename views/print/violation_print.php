<?php 
include '../../core/config.php';
$sdate = $_GET['sDate'];
$edate = $_GET['eDate'];
$sortType = $_GET['sortType'];
$program = $_GET['program'];
$student_id = $_GET['student'];

$title = ($sortType == 'A')?"Violation Report By Date":(($sortType == 'C')?"Violation Report By Course":"Violation Report By Student");
?>
<link href="../../assets/css/bootstrap.min.css" rel="stylesheet" media="all">
<script src="../../assets/js/jquery.min.js"></script>
<style>
@media print{
		body * {
			-webkit-print-color-adjust: exact;
		}
		thead * {
			background-color: #4e6883 !important;
			color: #fff !important;
		}
		tfoot * {
			background-color: #4e6883 !important;
			color: #fff !important;
		}
	}
</style>
<div class='col-md-12' style='text-align:center;margin-top:50px'>
        <img src="../../assets/images/chmsc.png" alt="CHMSC-Alijis Office of the Guidance Services" />
    </div>
    <div class='col-md-12' style='text-align:center;'><h6><?=$title?></h6></div><br>
<div class='col-md-12' style='margin-top:10px;'>
    <table id='applicationReport' border='1' cellpadding='3' cellspacing='3' class="" style='margin-top:10px;width:100%'>
        <thead style='background-color: #343940 !important;color: white !important;'>
            <tr>
                <th>#</th>
                <th>STUDENT NAME</th>
                <th>VIOLATION</th>
                <th>PENALTY</th>
                <th>INCIDENT DATE</th>
                <th>REPORT DATE</th>
            </tr>
        </thead>
        <tbody>
            <?php 

               $where = ($sortType == 'A')?" AND v.event_date BETWEEN '$sdate' AND '$edate'":(($sortType == 'S')?" AND v.event_date BETWEEN '$sdate' AND '$edate' AND v.student_id = '$student_id'":" AND v.event_date BETWEEN '$sdate' AND '$edate' AND s.course = '$program'");
                $query = mysql_query("SELECT v.violator_id as v_id, CONCAT(s.student_lname,', ',s.student_fname,' ',s.student_mname) as st_name, vn.name as v_name, p.penalty_name as p_name, v.event_date as e_date, v.report_date as r_date FROM tbl_violators as v, tbl_students as s, `tbl_penalties` as p, tbl_violation as vn WHERE v.student_id = s.student_id AND v.penalty = p.penalty_id AND v.violation = vn.violation_id $where");
                $count = 1;
                while($row = mysql_fetch_array($query)){
            ?>
                <tr>
                    <td><?php echo $count++; ?></td>
                    <td><?php echo $row['st_name']; ?></td>
                    <td><?php echo $row['v_name']; ?></td>
                    <td><?php echo $row['p_name']; ?></td>
                    <td><?php echo date("F d, Y h:i A", strtotime($row['e_date']));?></td>
                    <td><?php echo date("F d, Y h:i A", strtotime($row['r_date']));?></td>
                </tr>

            <?php } ?>
        </tbody>
    </table>
</div>

<script>
$(document).ready( function(){
    // $('#applicationReport').dataTable();
    //print();
})

function printPage(){ 
        print();
    }
</script>