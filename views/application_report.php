<style type="text/css">
    
    @media print{
        @page{
            font-size: 25px !important;
        }
       
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Qualified Applicants Report</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <div class='row'>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Start Date: </div>
                                      <input type="date" id='s_date' class='form-control'>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">End Date: </div>
                                      <input type="date" id='e_date' class='form-control'>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-4'>
                            <div class="form-group">
                                <button class='btn btn-sm btn-primary' type='button' onclick='genReport()'><span class='fa fa-gear'></span> Generate</button>
                                <button class='btn btn-sm btn-success' type='button' onclick='printReport()'><span class='fa fa-print'></span> Print</button>
                            </div>
                        </div>
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='applicationReport' class="table table-bordered table-hover" style='margin-top:10px;'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th>#</th>
                                        <th>APPLICANT NAME</th>
                                        <th>SCORE</th>
                                        <th>STANINE</th>
                                        <th>STATUS</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>

<script>
$(document).ready( function(){
    //genReport();
    $('#JOframe').on('load', function () {
		$('#loader1').hide();
	});
});
function printReport() {
    var s_Date = $("#s_date").val();
    var e_Date = $("#e_date").val();
    if(s_Date == '' || e_Date == ''){
        alert("Unable to print, Please Select date");
    }else{
        $("#application_report").modal('show');
        var url = 'views/print/print_application_report.php?sDate='+s_Date+'&eDate='+e_Date;
	    $('#JOframe').attr('src', url);
    }
	
	// $("#application_report").modal('show');
	//window.location = 'index.php?page=print-payroll-payslip&emp_id='+id;
}
function genReport(){
    var s_Date = $("#s_date").val();
    var e_Date = $("#e_date").val();
    if(s_Date == '' || e_Date == ''){
        alert("Unable to load report, Please Select date");
    }else{
        applicationReport(s_Date, e_Date);
    }
    
}
function applicationReport(s_Date, e_Date){
    $("#applicationReport").DataTable().destroy();
    $('#applicationReport').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/application_report.php",
        "dataSrc":"data",
        "data":{
            s_Date: s_Date,
            e_Date: e_Date
        },
        "type":"POST"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"student"
        },
        {
            "data":"score"
        },
        {
            "data":"stanine"
        },
        {
            "data":"status"
        }
        
    ]   
    });
}
</script>