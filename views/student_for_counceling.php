<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Students</h2>
        </div>
        <hr>
        <input type="hidden" value='<?php echo $user_id ?>' id='userid' name="">
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <div class='row'>
                         <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group" >
                                    <div class="input-group-addon">Type: </div>
                                       <select id='sortingType' class='form-control' onchange="sortingType()">
                                          <option value=""> &mdash; Please Choose &mdash;</option>
                                          <option value='A'> All </option>
                                          <option value='S'> Sort </option>
                                       </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Course: </div>
                                       <select id='program' class='form-control' onchange="getSections()">
                                           <?php echo GETALLPROGRAMS();?>
                                       </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Year: </div>
                                       <select id='year_level' class='form-control' onchange="getSections()">
                                          <option value=""> &mdash; Please Choose &mdash;</option>
                                          <option value='1'> First Year </option>
                                          <option value='2'> Second Year</option>
                                          <option value='3'> Third Year</option>
                                          <option value='4'> Fourth Year</option>
                                          <option value='5'> Fifth Year</option>
                                       </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Section: </div>
                                       <select id='section' class='form-control'>
                                         
                                       </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-12'>
                            <div class="form-group">
                               <!--  <button class='btn btn-success btn-sm pull-right' onclick='schedCounceling()'><span class='fa fa-calendar'></span> Schedule for counseling</button> -->
                               <button class='btn btn-primary btn-sm pull-right' onclick='genList()'><span class='fa fa-gear'></span> Generate</button>
                              
                            </div>
                        </div>
                        <div class='col-md-12' style='margin-top:10px;'>

                            <table id='students' class="table table-bordered table-hover" style='margin-top:10px;'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th>#</th>
                                        <th></th>
                                        <th>NAME</th>
                                        <th>STATUS</th>
                                        <th>ACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>
<script>
$(document).ready( function(){
    genList();

    $("#form_upload_file").submit(function(e){
    e.preventDefault();

    // $("#btn_upload_file").prop('disabled', true);
    // $("#btn_upload_file").html("<span class='fa fa-spin fa-spinner'></span> Loading ...");
    
    $.ajax({
        url:"ajax/upload_student_list.php",
        method:"POST",
        data:new FormData(this),
        contentType:false,          // The content type used when sending data to the server.
        cache:false,                // To unable request pages to be cached
        processData:false,          // To send DOMDocument or non processed data file it is set to false
        success: function(data){
            $("#entry_file").val("");
            $("#uploadList").modal('hide');
            $("#responseModal").modal('show');
            $("#response_cont").html(data);
            studentList();
        }
    });
    });
});
function schedCounceling(){
     var count_checked = $('input[name="checkbox_std"]:checked').map(function() {
                                        return this.value;
                                    }).get();
    if(count_checked == ''){
        alert("Please Select Student");
    }else{
        $("#schedModal").modal();
    }
}
function saveSchedCnclng(){
     var violationID = $("#violationID").val();
     var userid = $("#userid").val();
     var studentID = $("#studentID").val();
     var sortingType = $("#sortingType").val();

    var course = (sortingType == 'A')?"":$("#prgmID").val();
    var year = (sortingType == 'A')?"":$("#year_level").val();
    var section = (sortingType == 'A')?"":$("#section").val();
     if(violationID == ''){
        alert("Please Choose Violation");
     }else{
        $.post("ajax/save_couceling_sched.php", {
            violationID: violationID,
            userid: userid,
            studentID: studentID
        }, function(data){
            if(data > 0){
                alert("Success");
            }else{
                alert("Failed");
            }
            $("#schedModal").modal('hide');
            studentList(sortingType, "", "", "");
        });
     }
}
function sortingType(){
    var sortingType = $("#sortingType").val();
    if(sortingType == 'A'){
        $("#year_level").attr("disabled", true);
        $("#section").attr("disabled", true);
        $("#program").attr("disabled", true);
        $("#section").val("");
        $("#year_level").val("");
        $("#program").val("");
    }else{
        $("#year_level").attr("disabled", false);
        $("#section").attr("disabled", false);
        $("#program").attr("disabled", false)
    }
}
function getSections(){
    var course = $("#program").val();
    var year = $("#year_level").val();

    $.post("ajax/sectionList.php", {
        course: course,
        year: year
    }, function(data){
        $("#section").html(data);
    });
}
function genList(){
    var sortingType = $("#sortingType").val();
    var course = $("#program").val();
    var year = $("#year_level").val();
    var section = $("#section").val();
    studentList(course, year, section, sortingType);
}
function inactiveStat(userID){
    
    $.post("ajax/user_function.php", {
        userID: userID,
        action: action
    }, function(data){
        if(data == 1){
            alert("Success");
        }else{
            alert("Failed");
        }
        userList();
    });
}
function viewCounselling(id){
    window.location = 'index.php?page=counselling-records&stID='+id;
}
function setforReferral(stID){
     $("#schedModal").modal();
     $("#studentID").val(stID);
}
function studentList(course, year, section, sortingType){
    $("#students").DataTable().destroy();
    $('#students').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/students.php",
        "dataSrc":"data",
        "data":{
          course: course,
          year: year,
          section: section,
          sortingType: sortingType
        },
        "type": "POST"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"action"
        },
        {
            "data":"name"
        },
        {
            "data":"status"
        },
        {
            "data":"actions"
        }
        
    ]   
    });
}
</script>