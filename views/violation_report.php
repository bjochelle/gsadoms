<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Violation Report</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                    <div class='row'>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group" >
                                    <div class="input-group-addon">Filter By: </div>
                                       <select id='sortingType' class='form-control' onchange="sortingType()">
                                          <option value=""> &mdash; Please Choose &mdash;</option>
                                          <option value='A'> All </option>
                                          <option value='C'> By Course </option>
                                          <option value='S'> By Student </option>
                                       </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Course: </div>
                                       <select id='program' class='form-control' onchange="getStudentbyCourse()">
                                           <?php echo GETALLPROGRAMS();?>
                                       </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Start Date: </div>
                                      <input type="date" id='s_date' class='form-control'>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">End Date: </div>
                                      <input type="date" id='e_date' class='form-control'>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Students: </div>
                                       <select id='studentID' class='form-control select2'>
                                        <?=getActiveStudents()?>
                                       </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <button class='btn btn-sm btn-primary pull-right' type='button' onclick='genReport()'><span class='fa fa-gear'></span> Generate</button>
                                <button class='btn btn-sm btn-success pull-right' type='button' onclick='printReport()'><span class='fa fa-print'></span> Print</button>
                            </div>
                        </div>
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='violationReport' class="table table-bordered table-hover" style='margin-top:10px;'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th>#</th>
                                        <th>STUDENT NAME</th>
                                        <th>VIOLATION</th>
                                        <th>PENALTY</th>
                                        <th>INCIDENT DATE</th>
                                        <th>REPORT DATE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>
<script>
$(document).ready( function(){
    //genReport();
    $('#JOframe1').on('load', function () {
		$('#loader1').hide();
	});
});
function getSections(){
    var course = $("#program").val();
    var year = $("#year_level").val();

    $.post("ajax/sectionList.php", {
        course: course,
        year: year
    }, function(data){
        $("#section").html(data);
    });
}
// function getStudentbyCourse(){
//     var course = $("#program").val();
//     $.post("ajax/getStudentbyCourse.php", {
//         course: course
//     }, function(data){
//         $("#studentID").html(data);
//     })
// }
function sortingType(){
    var sortingType = $("#sortingType").val();
    if(sortingType == 'S'){
        $("#program").attr("disabled", true);
        $("#studentID").attr("disabled", false);
        $("#program").val("");
    }else if(sortingType == 'C'){
        $("#program").attr("disabled", false);
        $("#studentID").attr("disabled", true);
        $("#studentID").val("");
    }else{
        $("#program").attr("disabled", true);
        $("#studentID").attr("disabled", true);
    }
}
function printReport() {
    var sortingType = $("#sortingType").val();
    var s_Date = $("#s_date").val();
    var e_Date = $("#e_date").val();
    var program = $("#program").val();
    var studentID = $("#studentID").val();
    if(s_Date == '' || e_Date == ''){
        alert("Unable to print, Please Select date");
    }else{
        $("#violation_report").modal('show');
        var url = 'views/print/violation_print.php?sDate='+s_Date+'&eDate='+e_Date+'&sortType='+sortingType+'&program='+program+'&student='+studentID;
	    $('#JOframe1').attr('src', url);
    }
	
	// $("#application_report").modal('show');
	//window.location = 'index.php?page=print-payroll-payslip&emp_id='+id;
}
function genReport(){
    var sortingType = $("#sortingType").val();
    var s_Date = $("#s_date").val();
    var e_Date = $("#e_date").val();
    var program = $("#program").val();
    var studentID = $("#studentID").val();
    if(s_Date == '' || e_Date == ''){
        alert("Unable to load report, Please Select date");
    }else{
        violationReport(s_Date, e_Date, sortingType, program, studentID);
    }
    
}
function violationReport(s_Date, e_Date, sortingType, program, studentID){
    $("#violationReport").DataTable().destroy();
    $('#violationReport').dataTable({
    "processing":true,
    "ajax":{
        "url":"ajax/datatables/violation_report.php",
        "dataSrc":"data",
        "data":{
            s_Date: s_Date,
            e_Date: e_Date,
            sortingType: sortingType,
            program: program,
            studentID: studentID
        },
        "type":"POST"
    },
    "columns":[
        {
            "data":"count"
        },
        {
            "data":"student"
        },
        {
            "data":"violation"
        },
        {
            "data":"penalty"
        },
        {
            "data":"e_date"
        },
        {
            "data":"r_date"
        }
        
    ]   
    });
}
</script>