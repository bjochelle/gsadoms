<div class="row">
    <div class="col-md-12">
        <div class="overview-wrap">
            <h2 class="title-1">Applicant Type</h2>
        </div>
        <hr>
        <div class="col-lg-12">
            <div class="au-card m-b-30">
                <div class="au-card-inner">
                   <div class='row'>
                        <div class='col-md-12'>
                            <button class='btn btn-sm btn-primary pull-right' data-toggle='modal' data-target="#appTypeModal"><span class='fa fa-plus-circle'></span> Add</button>
                            <button class='btn btn-sm btn-danger pull-right' onclick='deletePrgm()' id='deletePrgm'><span class='fa fa-trash'></span> Delete</button>
                        </div>
                        <div class='col-md-12' style='margin-top:10px;'>
                            <table id='applicantType' class="table table-bordered table-hover" style='margin-top:10px;'>
                                <thead style='background-color: #343940;color: white;'>
                                    <tr>
                                        <th></th>
                                        <th>#</th>
                                        <th></th>
                                        <th>APPLICANT TYPE</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                   </div>
                </div>
            </div>
        </div>   
    </div>
</div>


<script>
    $(document).ready( function(){
        applicantTypeLists();
    });
    function deletePrgm(){
        var count_checked = $('input[name="checkbox_appType"]:checked').map(function() {
                                        return this.value;
                                    }).get();
        checked_array = [];
        var action = 'delete';
        var conf = confirm("Are you sure you want delete this?");
        if(conf == true){
            $("#deletePrgm").prop("disabled", true);
            $("#deletePrgm").html("<span class='fas fa-spin fa-spinner'></span> Loading");
            $.post("ajax/apptype_functions.php", {
                action: action,
                checked_array: count_checked
            }, function(data){
                if(data > 0){
                    alert("Successfully Deleted");
                }else{
                    alert("Error");
                }
                applicantTypeLists();
                $("#deletePrgm").prop("disabled", false);
                $("#deletePrgm").html("<span class='fas fa-trash'></span> Delete");
            });
        }
        
    }
    function viewRecord(appTypeID, appTypeName){
        $("#updateappType").modal('show');
        $("#appTypeName").val(appTypeName);
        $("#appTypeID").val(appTypeID);
    }
    function UpdateApptype(){
        var appTypeName = $("#appTypeName").val();
        var appTypeID = $("#appTypeID").val();
        var action = 'update';
        $("#btn_update_type").prop("disabled", true);
        $("#btn_update_type").html("<span class='fas fa-spin fa-spinner'></span> Loading");
        $.post("ajax/apptype_functions.php", {
            appTypeName: appTypeName,
            appTypeID: appTypeID,
            action: action
        }, function(data){
            $("#updateappType").modal('hide');
            if(data == 1){
                alert("Successfully Updated");
            }else{
                alert("Error");
            }
            applicantTypeLists();
            $("#btn_update_type").prop("disabled", false);
            $("#btn_update_type").html("<span class='fas fa-check-circle'></span> Continue");
        });
    }
    function addNewAppType(){
        var appType = $("#appType").val();
        var action = 'add';
        $("#btn_add_type").prop('disabled', true);
        $("#btn_add_type").html("<span class='fas fa-spin fa-spinner'></span> Loading ");
        $.post("ajax/apptype_functions.php", {
            appType: appType,
            action: action
        }, function(data){
            $("#appTypeModal").modal('hide');
            if(data == 1){
                alert("Success");
            }else{
                alert("Failed");
            }
            $("#btn_add_type").prop('disabled', false);
            $("#btn_add_type").html("<span class='fas fa-check-circle'></span> Continue ");
            $("#appType").val("");
            applicantTypeLists();
        }); 
    }
    function applicantTypeLists(){
        $("#applicantType").DataTable().destroy();
        $('#applicantType').dataTable({
        "processing":true,
        "ajax":{
            "url":"ajax/datatables/applicant_type_list.php",
            "dataSrc":"data"
        },
        "columns":[
            {
                "mRender": function(data,type,row){
                    return "<input type='checkbox' name='checkbox_appType' value='"+row.at_id+"'>";		
                }
            },
            {
                "data":"count"
            },
            {
                "data":"action"
            },
            {
                "data":"type"
            }
            
        ]   
        });
    }
</script>