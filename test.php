<h2 style='text-align:center'>Registration Complete!</h2>
<h4 style='text-align:center'>We will send you a schedule for exam via text message or Email. Thank You!</h4>


<?php
include 'core/config.php';
?>
<style>
    .divider{
        border: 1px solid #d0cccc;
        margin-top: 10px;
        border-style: dashed;
    }
    /* .input-group-addon {
        padding: .5rem .75rem;
        margin-bottom: 0;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.25;
        color: #000000;
        text-align: center;
        -webkit-border-radius: .25rem;
        -moz-border-radius: .25rem;
        border-radius: .25rem;
        background-color: #ffffff;
        border: 1px solid rgb(255 255 255);
    }
    .borderNone{
        border-top: none;
        border-left: none;
        border-right: none;
        border-bottom: 1px solid black;
    } */
</style>
<hr>
<form id="registerStudent" method="POST" action="" enctype="multipart/form-data">
<div class="col-lg-12">
    <div class="au-card m-b-30">
        <div class="au-card-inner">
            <div class='row'>
                <div class='col-md-12' style='text-align:center;'>
                    <img src="assets/images/chmsc.png" alt="CHMSC-Alijis Office of the Guidance Services" />
                </div>
                <div class='col-md-12' style='text-align:center;'><h5>OFFICE OF THE GUIDANCE SERVICES</h5></div><br><br>
                <div class='col-md-12' style='text-align:center;'><h6>STUDENT'S INDIVIDUAL INVENTORY</h6></div>
                <div class='col-md-12'>
                    <input type="checkbox" class='studentType' name='studentType'> Freshmen <br>
                    <input type="checkbox" class='studentType' name='studentType'> Shiftee <br>
                    <input type="checkbox" class='studentType' name='studentType'> Transferee <br>
                    <input type="checkbox" class='studentType' name='studentType'> Returnee
                </div><br>
                <div class='col-md-12'>
                    <h5>I. PERSONAL INFORMATION</h5>
                </div><br>
                <div class='col-md-9'>
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Name: </div>
                                    <input type="text" id="stLname" placeholder='Family Name' name="stLname" class="form-control">
                                    <input type="text" id="stFname" placeholder='First Name' name="stFname" class="form-control">
                                    <input type="text" id="stMname" placeholder='Middle Name' name="stMname" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Course, Year, Section: </div>
                                    <input type="text" id="cys" placeholder='' name="cys" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Contact No.: </div>
                                    <input type="text" id="contactNo" placeholder='' name="contactNo" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class='col-md-4'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Sex: </div>
                                    <div class="input-group-addon"> <input type="checkbox" id="stAge" placeholder='' name="stAge" class="stSex"> Male </div>
                                    <div class="input-group-addon"> <input type="checkbox" id="stAge" placeholder='' name="stAge" class="stSex"> Female </div>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-3'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Age: </div>
                                    <input type="text" id="stAge" placeholder='' name="stAge" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class='col-md-5'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Civil Status.: </div>
                                        <select class='form-control' name="stcivilStat" id="stcivilStat">
                                            <option value=""> &mdash; Please Choose &mdash;</option>
                                            <option value="s"> Single </option>
                                            <option value="m"> Married </option>
                                            <option value="w"> Widowed </option>
                                            <option value=""> Gin Bayaan </option>
                                        </select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-12'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Solo Parent? : </div>
                                    <div class="input-group-addon"> <input type="checkbox" id="stAge" placeholder='' name="stAge" class="stSP"> Yes </div>
                                    <div class="input-group-addon"> <input type="checkbox" id="stAge" placeholder='' name="stAge" class="stSP"> No </div>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                </div>
                
                <div class='col-md-3' style='margin-bottom: 10px;'>
                    <div class='col-md-12'>
                        <img id="img_wrap" alt='Recent 2x2 Picture' class="previewImage01 image-wrap" src="assets/images/2x2.png" style="object-fit: cover;border:3px solid #b7b1b1;width:2in;height:2in;">

                        <div class="image-upload" style="margin-top: 5px;margin-left: 52px;">
                        <input type="file" name="avatar" id="files" required class="btn-inputfile share" />
                        <label for="files" class="btn default" style="font-size: 16px;"><i class="fa fa-file-image-o"></i> Select </label>
                        </div>
                    </div>
                </div>

                <div class='col-md-12'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Sexual Orientation : </div>
                            <div class="input-group-addon" style='margin-right: 0.5%;'> <input type="checkbox" id="stAge" placeholder='' name="stAge" class="stSO"> HeteroSexual </div>
                            <div class="input-group-addon" style='margin-right: 0.5%;'> <input type="checkbox" id="stAge" placeholder='' name="stAge" class="stSO"> Lesbian </div>
                            <div class="input-group-addon" style='margin-right: 0.5%;'> <input type="checkbox" id="stAge" placeholder='' name="stAge" class="stSO"> Transgender </div> 
                            <div class="input-group-addon" style='margin-right: 0.5%;'> <input type="checkbox" id="stAge" placeholder='' name="stAge" class="stSO"> Gay </div>
                            <div class="input-group-addon" style='margin-right: 0.5%;'> <input type="checkbox" id="stAge" placeholder='' name="stAge" class="stSO"> Bisexual </div>
                            <div class="input-group-addon"> <input type="checkbox" id="stAge" placeholder='' name="stAge" class="stSO"> Others: </div><input type="text" id="stAge" placeholder='' name="stAge" class="form-control" style='max-width: 15%;' readonly>
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Height: </div>
                            <input type="text" id="stAge" placeholder='' name="stAge" class="form-control">
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Weight: </div>
                            <input type="text" id="stAge" placeholder='' name="stAge" class="form-control borderNone">
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Date of Birth: </div>
                            <input type="date" id="stAge" placeholder='' name="stAge" class="form-control">
                        </div>
                    </div>
                </div>
                <div class='col-md-6'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Birthplace: </div>
                            <input type="text" id="stAge" placeholder='' name="stAge" class="form-control">
                        </div>
                    </div>
                </div>
                <div class='col-md-6'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Nationality: </div>
                            <input type="text" id="stAge" placeholder='' name="stAge" class="form-control">
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Home Address: </div>
                            <textarea name="" id="" rows="3" class='form-control' style='resize:none'></textarea>
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Email Address: </div>
                            <textarea name="" id="" rows="3" class='form-control' style='resize:none'></textarea>
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">City Address: </div>
                            <textarea name="" id="" rows="3" class='form-control' style='resize:none'></textarea>
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">General Average (HS/SH/COLLEGE): </div>
                            <input type="text" id="stAge" placeholder='' name="stAge" class="form-control">
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Strand/Course: </div>
                            <input type="text" id="stAge" placeholder='' name="stAge" class="form-control">
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Religion: </div>
                            <input type="text" id="stAge" placeholder='' name="stAge" class="form-control">
                        </div>
                    </div>
                </div>
                <div class='col-md-12'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Last School Attended: </div>
                            <input type="text" id="stAge" placeholder='' name="stAge" class="form-control">
                        </div>
                    </div>
                </div>
                <div class='col-md-12'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Person to be contacted, In case of emergency: </div>
                            <input type="text" id="stAge" placeholder='' name="stAge" class="form-control">
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Address: </div>
                            <input type="text" id="stAge" placeholder='' name="stAge" class="form-control">
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Contact No.: </div>
                            <input type="text" id="stAge" placeholder='' name="stAge" class="form-control">
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Relationship: </div>
                            <input type="text" id="stAge" placeholder='' name="stAge" class="form-control">
                        </div>
                    </div>
                </div>
                <div class='col-md-12 divider'></div>
                <div class='col-md-12' style='margin-top: 10px;margin-bottom: 10px;'>
                    <h5>II. EDUCATIONAL BACKGROUND</h5>
                </div><br>
                <div class='col-md-12'>
                    <table class='table table-striped table-condensed table-bordered'>
                        <thead>
                            <tr>
                                <th>LEVEL</th>
                                <th>SCHOOL</th>
                                <th>YEAR COVERED</th>
                                <th>PUBLIC/PRIVATE</th>
                                <th>HONOR RECEIVED</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php echo educBack_list(); ?>
                        </tbody>
                    </table>
                </div>

                <div class='col-md-12 divider'></div>
                <div class='col-md-12' style='margin-top: 10px;margin-bottom: 10px;'>
                    <h5>III. HOME AND FAMILY BACKGROUND</h5>
                </div><br>

                <div class='col-md-8'>
                    <table class='table table-striped table-condensed table-bordered'>
                        <thead>
                            <tr>
                                <th></th>
                                <th>FATHER</th>
                                <th>MOTHER</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php echo familyBackground(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 
</form>
<script>
   $(document).ready( function(){
    $('input.stSO').on('change', function() {
        $('input.stSO').not(this).prop('checked', false);  
    });
    $('input.stSP').on('change', function() {
        $('input.stSP').not(this).prop('checked', false);  
    });
    $('input.studentType').on('change', function() {
        $('input.studentType').not(this).prop('checked', false);  
    });
    $('input.stSex').on('change', function() {
        $('input.stSex').not(this).prop('checked', false);  
    });
    $('input.stSPP').on('change', function() {
        $('input.stSPP').not(this).prop('checked', false);  
    });
    $("#registerStudent").on('submit',(function(e) {
    e.preventDefault();
        $.ajax({
         url:"ajax/register_student.php",
         type: "POST",
         data:  new FormData(this),
         beforeSend: function(){},
         contentType: false,
         cache: false,
         processData:false,
         success: function(data){
            $("#reg_wrapper").html("<h2 style='text-align:center'>Registration Complete!</h2><h4 style='text-align:center'>We will send you a schedule for exam via text message or Email. Thank You!</h4>");
        },error: function(){
            alert("Error");
         }           
       });
        
    }));
       
   });

   $(".btn-inputfile").change(function () {
        $("#btn-edit").prop("disabled", false);
        var input = document.getElementById('files');
        previewFile(input);
    });
  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
</script>