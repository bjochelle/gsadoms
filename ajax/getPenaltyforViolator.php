<?php
include '../core/config.php';

$violationID = $_POST['violationID'];
$studentID = $_POST['studentID'];

$countViolation = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_violators as v, tbl_violation as vn, tbl_penalties as p WHERE v.violation = vn.violation_id AND v.penalty = p.penalty_id AND v.student_id = '$studentID' AND v.violation = '$violationID'"));

$getFirstOffenseLevel = mysql_fetch_array(mysql_query("SELECT first_offense_level FROM tbl_violation  WHERE violation_id = '$violationID'"));

$level = $countViolation[0] + 1;
$level_v2 =  $getFirstOffenseLevel[0] + $countViolation[0];
 $suffix = ($level == 1)?"st":(($level == 2)?"nd":(($level == 3)?"rd":(($level == 4)?"th":"")));

?>
<div class="form-group">
    <div class="input-group">
        <div class="input-group-addon">Penalty: </div>
        <select id='penaltyID' class='form-control' disabled>
            <?php 
            	echo "<option value=''> &mdash; Please Choose &mdash; </option>";
			    $query = mysql_query("SELECT * FROM tbl_penalties");
			    while($row = mysql_fetch_array($query)){
			    	$selected = ($row['level'] == $level_v2)?"selected":"";
			        echo "<option ".$selected." value='".$row['penalty_id']."'>".$row['penalty_name']."</option>";
			    }
            ?>
        </select>
        <div class="input-group-addon"><?php echo $level.$suffix." Offense (Level: $level_v2)"?></div>
    </div>
</div>