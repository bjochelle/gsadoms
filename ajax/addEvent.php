<?php 
include '../core/config.php';
$userid = $_SESSION['user_id'];
$eventName = $_POST['eventName'];
$daterange = explode("-", $_POST['daterange']);
$from = date("Y-m-d", strtotime($daterange[0]));
$to = date("Y-m-d", strtotime($daterange[1]));
$addEvent_time = $_POST['addEvent_time'];

$dateadded = date("Y-m-d H:i:s", strtotime(getCurrentDate()));

$data = array("event_name" => $eventName, "event_start_date" => $from , "event_end_date" => $to, "event_time" => $addEvent_time, "event_added_by" => $userid, "date_added" => $dateadded);
$return = insert_query("tbl_events",$data,"N");

echo $return;