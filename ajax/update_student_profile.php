<?php 
include('../core/config.php');
$stid = clean($_POST['stid']);
$fname = clean($_POST['fname']);
$mname = clean($_POST['mname']);
$lname = clean($_POST['lname']);
$program = clean($_POST['program']);
$year_level = clean($_POST['year_level']);
$section = clean($_POST['section']);
$stSex = clean($_POST['stSex']);
$stAge = clean($_POST['stAge']);
$stcontact = clean($_POST['stcontact']);
$civil_stat = clean($_POST['civil_stat']);
$soloP = clean($_POST['soloP']);
$sexOrient = clean($_POST['sexOrient']);
$height = clean($_POST['height']);
$weight = clean($_POST['weight']);
$bday = clean($_POST['bday']);
$bplace = clean($_POST['bplace']);
$nationlity = clean($_POST['nationlity']);
$homeAdd = clean($_POST['homeAdd']);
$emailAdd = clean($_POST['emailAdd']);
$cityAdd = clean($_POST['cityAdd']);
$genAdd = clean($_POST['genAdd']);
$stTS = clean($_POST['stTS']);
$rel = clean($_POST['rel']);
$lsa = clean($_POST['lsa']);
$ioe_person = clean($_POST['ioe_person']);
$ioe_address = clean($_POST['ioe_address']);
$ioe_contact = clean($_POST['ioe_contact']);
$ioe_relation = clean($_POST['ioe_relation']);
$pmr_res = clean($_POST['pmr_res']);
$pmr_spec = clean($_POST['otherSpec']);
$ordinal_pos = clean($_POST['ordinal_pos']);
$num_child = clean($_POST['num_child']);
$num_brothers = clean($_POST['num_brothers']);
$num_sisters = clean($_POST['num_sisters']);
$num_bs_employed = clean($_POST['num_bs_employed']);
$num_bs_employed_support = clean($_POST['num_bs_employed_support']);
$weekly_allowance = clean($_POST['weekly_allowance']);
$resType = clean($_POST['resType']);
$studyPlace = clean($_POST['studyPlace']);
$remarks = clean($_POST['remarks']);
$withwhom = clean($_POST['withwhom']);
$share_room = clean($_POST['share_room']);
$problem = clean($_POST['problem']);
$problem_specify = clean($_POST['problem_specify']);
$discuss_problem = clean($_POST['discuss_problem']);
$student_remarks = clean($_POST['student_remarks']);
$herebyTrue = $_POST['herebyTrue'];

$hIncome = $_POST['hIncome'];
$incomeOthers = $_POST['incomeOthers'];

$recurring_ailment = clean($_POST['recurring_ailment']);
$recurring_ailment_specify = clean($_POST['recurring_ailment_specify']);
$accident = clean($_POST['accident']);
$accident_specify = clean($_POST['accident_specify']);
$psych = clean($_POST['psych']);
$psych_specify = clean($_POST['psych_specify']);

$array_ed = $_POST['array_ed'];
$array_sibs = $_POST['array_sibs'];
$array_test = $_POST['array_test'];
$array_deceased = $_POST['array_deceased'];
$array_health = $_POST['array_health'];
$array_finance = $_POST['array_finance'];
$array_act = $_POST['array_act'];

$array_data = array(
    "student_fname" => $fname,
    "student_mname" => $mname,
    "student_lname" => $lname,
    "level" => $year_level,
    "course" => $program,
    "section" => $section,
    "sex" => $stSex,
    "contact_num" => $stcontact,
    "age" => $stAge,
    "civil_stat" => $civil_stat,
    "solo_parent" => $soloP,
    "sex_orient" => $sexOrient,
    "height" => $height,
    "weight" => $weight,
    "birthplace" => $bplace,
    "dob" => $bday,
    "nationality" => $nationlity,
    "home_address" => $homeAdd,
    "email_address" => $emailAdd,
    "city_address" => $cityAdd,
    "gen_ave" => $genAdd,
    "strand_course" => $stTS,
    "religion" => $rel,
    "last_sch_att" => $lsa,
    "ioe_person" => $ioe_person,
    "ioe_address" => $ioe_address,
    "ioe_contact" => $ioe_contact,
    "relationship" => $ioe_relation,
    "pmr" => $pmr_res,
    "pmr_specify" => $pmr_spec,
    "ordinal_pos" => $ordinal_pos,
    "num_child" => $num_child,
    "num_brothers" => $num_brothers,
    "num_sisters" => $num_sisters,
    "num_bs_employed" => $num_bs_employed,
    "num_bs_employed_support" => $num_bs_employed_support,
    "weekly_allowance" => $weekly_allowance,
    "household_income" => $hIncome,
    "household_income_others" => $incomeOthers,
    "residence_school" => $resType,
    "study_place" => $studyPlace,
    "study_place_specify" => $remarks,
    "share_room" => $share_room,
    "share_room_whom" => $withwhom,
    "recurring_ailment" => $recurring_ailment,
    "recurring_ailment_specify" => $recurring_ailment_specify,
    "accident" => $accident,
    "accident_specify" => $accident_specify,
    "psych" => $psych,
    "psych_specify" => $psych_specify,
    "problem" => $problem,
    "problem_specify" => $problem_specify,
    "discuss_problem" => $discuss_problem,
    "student_remarks" => $student_remarks,
    "check_hereby"  => $herebyTrue
);

$return = update_query("tbl_students",$array_data,"WHERE student_id = '$stid'");

//ACTIVITIES
if(count($array_act) > 0){
    foreach ($array_act as $key) {
        $exp2 = explode(":", $key);
        $checkbox2 = $exp2[0];
        $specify2 = $exp2[1];

        $checker2 = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_student_interested_org WHERE student_id = '$stid' AND org = '$checkbox2'"));
        if($checker2[0] > 0){
            $act_array = array("org_specify" => $specify2);
            $return = update_query("tbl_student_interested_org",$act_array,"WHERE student_id = '$stid' AND org = '$checkbox2'");
        }else{
            $act_array = array("student_id" => $stid, "org" => $checkbox2, "org_specify" => $specify2);
            if($checkbox2 != 'a'){
                $return = insert_query("tbl_student_interested_org",$act_array,"N");
            }
           
        }
    }
}
//MOTHER
$numM = 1;
$Mname = clean($_POST['Mname']);
$Mcontact = clean($_POST['Mcontact']);
$Mbplace = clean($_POST['Mbplace']);
$Mrel = clean($_POST['Mrel']);
$Mnatl = clean($_POST['Mnatl']);
$Mlord = clean($_POST['Mlord']);
$Meduc = clean($_POST['Meduc']);
$Moccu = clean($_POST['Moccu']);
$Mne = clean($_POST['Mne']);
$MaddE = clean($_POST['MaddE']);
$checker = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_student_family_background WHERE student_id = '$stid' AND m_f = '$numM'"));
if($checker[0] > 0){
    $fm_data = array("name" => $Mname,"contact" => $Mcontact,"birthplace" => $Mbplace,"religion" => $Mrel,"nationality" => $Mnatl,"l_d" => $Mlord,"educ_attainment" => $Meduc,"occupation" => $Moccu,"name_of_employer" => $Mne,"address_of_employer" => $MaddE);

    $update = update_query("tbl_student_family_background",$fm_data,"WHERE student_id = '$stid' AND m_f = '$numM'");
}else{
    $fm_data = array("student_id" => $stid, "m_f" => $numM, "name" => $Mname,"contact" => $Mcontact,"birthplace" => $Mbplace,"religion" => $Mrel,"nationality" => $Mnatl,"l_d" => $Mlord,"educ_attainment" => $Meduc,"occupation" => $Moccu,"name_of_employer" => $Mne,"address_of_employer" => $MaddE);

    $update = insert_query("tbl_student_family_background",$fm_data,"N");
}


// FATHER
$numF = 2;
$Fname = clean($_POST['Fname']);
$Fcontact = clean($_POST['Fcontact']);
$Fbplace = clean($_POST['Fbplace']);
$Frel = clean($_POST['Frel']);
$Fnatl = clean($_POST['Fnatl']);
$Flord = clean($_POST['Flord']);
$Feduc = clean($_POST['Feduc']);
$Foccu = clean($_POST['Foccu']);
$Fne = clean($_POST['Fne']);
$FaddE = clean($_POST['FaddE']);
$checker = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_student_family_background WHERE student_id = '$stid' AND m_f = '$numF'"));
if($checker[0] > 0){
    $fm_data = array("name" => $Fname,"contact" => $Fcontact,"birthplace" => $Fbplace,"religion" => $Frel,"nationality" => $Fnatl,"l_d" => $Flord,"educ_attainment" => $Feduc,"occupation" => $Foccu,"name_of_employer" => $Fne,"address_of_employer" => $FaddE);

    $update = update_query("tbl_student_family_background",$fm_data,"WHERE student_id = '$stid' AND m_f = '$numF'");
}else{
    $fm_data = array("student_id" => $stid, "m_f" => $numF, "name" => $Fname,"contact" => $Fcontact,"birthplace" => $Fbplace,"religion" => $Frel,"nationality" => $Fnatl,"l_d" => $Flord,"educ_attainment" => $Feduc,"occupation" => $Foccu,"name_of_employer" => $Fne,"address_of_employer" => $FaddE);

    $update = insert_query("tbl_student_family_background",$fm_data,"N");
}


$cat = "A";
//1

$num = 1;
$positionOrg = $_POST['positionOrg'];
$checkerIH = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_student_interest_hobbies WHERE student_id = '$stid' AND cat = '$cat' AND `number` = '$num'"));
if($checkerIH[0] > 0){
    $ih_data = array("ih_values" => $positionOrg);
    $update = update_query("tbl_student_interest_hobbies",$ih_data,"WHERE student_id = '$stid' AND cat = '$cat' AND `number` = '$num'");
}else{
    $ih_data = array("student_id" => $stid, "cat" => $cat, "number" => $num, "ih_values" => $positionOrg); 
    $update = insert_query("tbl_student_interest_hobbies",$ih_data,"N");   
}
//2
$num2 = 2;
$favSubj = $_POST['favSubj'];
$checkerIH = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_student_interest_hobbies WHERE student_id = '$stid' AND cat = '$cat' AND `number` = '$num2'"));
if($checkerIH[0] > 0){
    $ih_data = array("ih_values" => $favSubj);
    $update = update_query("tbl_student_interest_hobbies",$ih_data,"WHERE student_id = '$stid' AND cat = '$cat' AND `number` = '$num2'");
}else{
    $ih_data = array("student_id" => $stid, "cat" => $cat, "number" => $num2, "ih_values" => $favSubj); 
    $update = insert_query("tbl_student_interest_hobbies",$ih_data,"N");   
}
//3
$num3 = 3;
$leastSubj = $_POST['leastSubj'];
$checkerIH = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_student_interest_hobbies WHERE student_id = '$stid' AND cat = '$cat' AND `number` = '$num3'"));
if($checkerIH[0] > 0){
    $ih_data = array("ih_values" => $leastSubj);
    $update = update_query("tbl_student_interest_hobbies",$ih_data,"WHERE student_id = '$stid' AND cat = '$cat' AND `number` = '$num3'");
}else{
    $ih_data = array("student_id" => $stid, "cat" => $cat, "number" => $num3, "ih_values" => $leastSubj); 
    $update = insert_query("tbl_student_interest_hobbies",$ih_data,"N");   
}

$cat2 = "E";
$Num1 = 1;
$hobbies = $_POST['hobbies'];
$checkerIH2 = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_student_interest_hobbies WHERE student_id = '$stid' AND cat = '$cat2' AND `number` = '$Num1'"));
if($checkerIH2[0] > 0){
    $ih_data2 = array("ih_values" => $hobbies);
    $update = update_query("tbl_student_interest_hobbies",$ih_data2,"WHERE student_id = '$stid' AND cat = '$cat2' AND `number` = '$Num1'");
}else{
    $ih_data2 = array("student_id" => $stid, "cat" => $cat2, "number" => $Num1, "ih_values" => $hobbies); 
    $update = insert_query("tbl_student_interest_hobbies",$ih_data2,"N");   
}
    
// Educational
if(count($array_ed) > 0){
    foreach ($array_ed as $key) {
        $exp = explode(":", $key);
        $school = ($exp[0] == 'a')?"":clean($exp[0]);
        $year = ($exp[1] == 'a')?"":clean($exp[1]);
        $pp = ($exp[2] == 'a')?"":clean($exp[2]);
        $hr = ($exp[3] == 'a')?"":clean($exp[3]);
        $ret = $exp[4];

        $checker = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_student_educational_background WHERE student_id = '$stid' AND level = '$ret'"));
        if($checker[0] > 0){
            $ed_array = array("school" => $school, "year_covered" => $year, "public_private" => $pp, "honor_received" => $hr);
            $return = update_query("tbl_student_educational_background",$ed_array,"WHERE student_id = '$stid' AND level = '$ret'");
        }else{
            $ed_array = array("student_id" => $stid, "level" => $ret, "school" => $school, "year_covered" => $year, "public_private" => $pp, "honor_received" => $hr);
            $return = insert_query("tbl_student_educational_background",$ed_array,"N");
        }
    }
}

// Siblings
if(count($array_sibs) > 0){
    foreach ($array_sibs as $values) {
       $explode = explode(":", $values);
       $name = $explode[0];
       $sequence = $explode[1];

       $checker = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_student_siblings WHERE student_id = '$stid' AND `sequence` = '$sequence'"));
       if($checker[0] > 0){
            $sibs_array = array("name" => $name);
            $return = update_query("tbl_student_siblings",$sibs_array,"WHERE student_id = '$stid' AND `sequence` = '$sequence'");
       }else{
        if($name != 'q'){
            $ed_array = array("student_id" => $stid, "name" => $name, "sequence" => $sequence);
            $return = insert_query("tbl_student_siblings",$ed_array,"N");
        }
       }
    }
}

// Psych Test
if(count($array_test) > 0){
    foreach ($array_test as $key) {
        $exp = explode(":", $key);
        $date = ($exp[0] == 'a')?"":clean($exp[0]);
        $test = ($exp[1] == 'a')?"":clean($exp[1]);
        $result = ($exp[2] == 'a')?"":clean($exp[2]);
        $interp = ($exp[3] == 'a')?"":clean($exp[3]);
        $ret = $exp[4];

        $checker = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_student_psych_test WHERE student_id = '$stid' AND test_count = '$ret'"));
        if($checker[0] > 0){
            $test_array = array("date" => $date, "test_name" => $test, "result" => $result, "interpretation" => $interp);
            $return = update_query("tbl_student_psych_test",$test_array,"WHERE student_id = '$stid' AND test_count = '$ret'");
        }else{
            $ed_array = array("student_id" => $stid, "test_count" => $ret, "date" => $date, "test_name" => $test, "result" => $result, "interpretation" => $interp);
            if($ret != 0){
                $return = insert_query("tbl_student_psych_test",$ed_array,"N");
            }
           
        }
    }
}
//DECEASED
if(count($array_deceased) > 0){
    foreach ($array_deceased as $key) {
        $exp = explode(":", $key);
        $checkbox = $exp[0];
        $date = $exp[1];
        $reason = $exp[2];
        $member = $exp[3];
        $checker = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_student_deceased_member WHERE student_id = '$stid' AND member = '$member'"));
        if($checker[0] > 0){
            $test_array = array("date" => $date, "reason" => $reason);
            $return = update_query("tbl_student_deceased_member",$test_array,"WHERE student_id = '$stid' AND member = '$member'");
        }else{
            $ed_array = array("student_id" => $stid, "member" => $member, "date" => $date, "reason" => $reason);
            if($checkbox != 'a'){
                $return = insert_query("tbl_student_deceased_member",$ed_array,"N");
            }
           
        }
    }
}
//Health
if(count($array_health) > 0){
    foreach ($array_health as $key) {
        $exp = explode(":", $key);
        $checkbox = $exp[0];
        $specify = $exp[1];

        $checker = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_student_health WHERE student_id = '$stid' AND health = '$checkbox'"));
        if($checker[0] > 0){
            $test_array = array("heath_specify" => $specify);
            $return = update_query("tbl_student_health",$test_array,"WHERE student_id = '$stid' AND health = '$checkbox'");
        }else{
            $ed_array = array("student_id" => $stid, "health" => $checkbox, "heath_specify" => $specify);
            if($checkbox != 'a'){
                $return = insert_query("tbl_student_health",$ed_array,"N");
            }
           
        }
    }
}
//Health
if(count($array_finance) > 0){
    foreach ($array_finance as $key) {
        $exp = explode(":", $key);
        $checkbox = $exp[0];
        $specify = $exp[1];

        $checker = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_student_finance_schooling WHERE student_id = '$stid' AND financer = '$checkbox'"));
        if($checker[0] > 0){
            $test_array = array("financer_specify" => $specify);
            $return = update_query("tbl_student_health",$test_array,"WHERE student_id = '$stid' AND financer = '$checkbox'");
        }else{
            $ed_array = array("student_id" => $stid, "financer" => $checkbox, "financer_specify" => $specify);
            if($checkbox != 'a'){
                $return = insert_query("tbl_student_finance_schooling",$ed_array,"N");
            }
           
        }
    }
}

echo $return;