<?php
include '../core/config.php';
$curdate = getCurrentDate();
// Personal Info
$fname = $_POST['stFname'];
$mname = $_POST['stMname'];
$lname = $_POST['stLname'];

$sem = $_POST['sem'];

$sy_from = $_POST['sy_from'];
$sy_to = $_POST['sy_to'];

$applicantType = $_POST['applicantType'];

$stBod = $_POST['stBod'];
$stLSA = $_POST['stLSA'];
$stSex = $_POST['stSex'];
$stLRN = $_POST['stLRN'];
$stTS = $_POST['stTS'];

$stAge = $_POST['stAge'];
$stCPNo = $_POST['stCPNo'];
$stEmail = $_POST['stEmail'];
$stPrefCourse = $_POST['stPrefCourse'];
$stCIE = $_POST['stCIE'];

$stSN = $_POST['streetName'];
$stBrgy = $_POST['brgy'];
$stCity = $_POST['city'];
$stProv = $_POST['province'];

if(is_array($_FILES)) {
    if(is_uploaded_file($_FILES['avatar']['tmp_name'])) {
        $sourcePath = $_FILES['avatar']['tmp_name'];
        $targetPath = "../assets/images/".$_FILES['avatar']['name'];
        if(move_uploaded_file($sourcePath,$targetPath)) {
            $img = $_FILES['avatar']['name'];
            $array_data = array(
                                "firstname" => $fname,
                                "middlename" => $mname,
                                "lastname" => $lname,
                                "semester" => $sem,
                                "acad_year_from" => $sy_from,
                                "acad_year_to" => $sy_to,
                                "applicant_type" => $applicantType,
                                "applicant_2x2" => $img,
                                "birthdate" => $stBod,
                                "last_school" => $stLSA,
                                "gender" => $stSex,
                                "LRN" => $stLRN,
                                "strand" => $stTS,
                                "age" => $stAge,
                                "cp_no" => $stCPNo,
                                "email" => $stEmail,
                                "preferred_course" => $stPrefCourse,
                                "campus_intended" => $stCIE,
                                "address_street" => $stSN,
                                "address_brgy" => $stBrgy,
                                "address_city" => $stCity,
                                "address_province" => $stProv,
                                "date_added" => $curdate,
                                "status" => 0
            );
            $return = insert_query("tbl_applicants",$array_data,"Y");
            $guidance = mysql_fetch_array(mysql_query("SELECT user_id FROM tbl_users WHERE category_id = 0"));
            $notif_data = array("notif_sender" => $return , "notif_receiver" => $guidance[0] , "notif_content" => "You Have New Applicant ($fname $mname $lname)", "notif_datetime" => $curdate, "notif_type" => "A", "notif_status" => 0);

            $return1 = insert_query("tbl_notification",$notif_data,"N");
        }
    }
}
echo $return;
?>
