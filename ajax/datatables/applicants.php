<?php 
include '../../core/config.php';

$sortingType = $_POST['sortingType'];
$curdate = date("Y-m-d", strtotime(getCurrentDate()));
if($sortingType == 'N'){
   $where = " WHERE is_archived = 0";
}else{
   $where = " WHERE is_archived = 1";
}
$query = mysql_query("SELECT * FROM tbl_applicants $where ");
$count = 1;
$response['data'] = array(); 

while($row = mysql_fetch_array($query)){

    $examDate = getExamDate($row['applicant_id']);
    $list = array();
    $list['applicant_id'] = $row['applicant_id'];
    $list['count'] = $count++;
    $list['fullname'] = applicantName($row['applicant_id']);
    $list['address'] = applicantAddress($row['applicant_id']);
    $list['contact'] = $row['cp_no'];
    $list['score'] = $row['score'];
    $list['stanine'] = $row['stanine'];
    $list['date_reg'] = date("F d, Y", strtotime($row['date_added']));
    $list['date_exam'] = (GETEXAMSCHEDULE($row['applicant_id']) == 'a')?"<span style='color: red'> No Schedule Yet</span>":GETEXAMSCHEDULE($row['applicant_id']);

    if(GETEXAMSCHEDULE($row['applicant_id']) == 'a'){
          $list['status'] = "<span style='color: orange'>PENDING</span>";
          $list['stat'] = 1;
    }else{
       if($row['status'] == 0 && $examDate < $curdate){
          $list['status'] = "<span style='color: red'>CANCELLED</span>";
          $list['stat'] = 0;
       }else if($row['status'] == 0 && $examDate >= $curdate){
          $list['status'] = "<span style='color: orange'>PENDING</span>";
          $list['stat'] = 1;
       }else if($row['status'] == 1){
          $list['status'] = "<span style='color: green'>PASSED</span>";
          $list['stat'] = 0;
       }else if($row['status'] == 2){
         $list['status'] = "<span style='color: red'>FAILED</span>";
         $list['stat'] = 0;
      }else{
         $list['status'] = "<span style='color: green'>ENROLLED</span>";
          $list['stat'] = 0;
       }
    }

    array_push($response['data'],$list);
}
	echo json_encode($response);