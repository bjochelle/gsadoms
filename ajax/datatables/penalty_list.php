<?php 
include '../../core/config.php';

$query = mysql_query("SELECT * FROM tbl_penalties");
$count = 1;
$response['data'] = array(); 

while($row = mysql_fetch_array($query)){
    $list = array();
    $list['ts_id'] = $row['penalty_id'];
    $list['count'] = $count++;
    $list['tsName'] = $row['penalty_name'];
    $suffix = ($row['level'] == 1)?"st":(($row['level'] == 2)?"nd":(($row['level'] == 3)?"rd":(($row['level'] == 4)?"th":"")));
    $list['level'] = $row['level'].$suffix." Offense";

    $list['action'] = "<center>
                        <li class='dropdown' style='list-style: none; font-size: 18px; color: #FFF;'>
                            <a href='#' class='dropdown-toggle' data-toggle='dropdown' style='color: #607D8B;'><strong><span class='fa fa-gear'></span></strong></a>
                                <ul class='dropdown-menu'>
                                    <ul style='background: #444; border: 1px solid #333; padding: 5px 10px;'>
                                        <li style='list-style:none;color:#FFF;'><a href='#' onclick='viewRecord(\"".$row['penalty_id']."\",\"".$row['penalty_name']."\");' style='color: #fff;'><span class='fas fa-edit' style='font-size: 14px;'></span> Edit Record </a></li>
                                    </ul>
                                </ul>
                        </li>
                    </center>";

    array_push($response['data'],$list);
}
	echo json_encode($response);