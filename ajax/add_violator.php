<?php 
include '../core/config.php';

$studentID = $_POST['studentID'];
$violationID = $_POST['violationID'];
$penaltyID = $_POST['penaltyID'];
$incedentDate = $_POST['incedentDate'];
$incedentTime = $_POST['incedentTime'];
$reportedDate = $_POST['reportedDate'];
$reportedTime = $_POST['reportedTime'];
$reportedBy = $_POST['reportedBy'];

$datetime_reported = date("Y-m-d", strtotime($reportedDate))." ".$reportedTime;
$datetime_incident = date("Y-m-d", strtotime($incedentDate))." ".$incedentTime;

$dateadded = date("Y-m-d H:i:s", strtotime(getCurrentDate()));

$data = array("student_id" => $studentID, "violation" => $violationID , "penalty" => $penaltyID, "event_date" => $datetime_incident, "date_added" => $dateadded, "added_by" => $reportedBy, "report_date" => $datetime_reported);
$return = insert_query("tbl_violators",$data,"N");

echo $return;