<?php
include 'core/config.php';
?>
<style>
    .divider{
        border: 1px solid #d0cccc;
        margin-top: 10px;
        border-style: dashed;
    }
    /* .input-group-addon {
        padding: .5rem .75rem;
        margin-bottom: 0;
        font-size: 1rem;
        font-weight: 400;
        line-height: 1.25;
        color: #000000;
        text-align: center;
        -webkit-border-radius: .25rem;
        -moz-border-radius: .25rem;
        border-radius: .25rem;
        background-color: #ffffff;
        border: 1px solid rgb(255 255 255);
    }
    .borderNone{
        border-top: none;
        border-left: none;
        border-right: none;
        border-bottom: 1px solid black;
    } */
</style>
<hr>
<div id='reg_wrapper'>
<form id="registerStudent" method="POST" action="" enctype="multipart/form-data">
<div class="col-lg-12">
    <div class="au-card m-b-30">
        <div class="au-card-inner">
            <div class='row'>
                <div class='col-md-12'>
                     <button class="btn btn-primary btn-sm pull-right" onclick="window.location.reload()" id="backButton" type="button"><span class="fas fa-arrow-left"></span> Back </button>
                </div>
                <div class='col-md-12' style='text-align:center;'>
                    <img src="assets/images/chmsc.png" alt="CHMSC-Alijis Office of the Guidance Services" />
                </div>
                <div class='col-md-12' style='text-align:center;'><h5>OFFICE OF THE GUIDANCE SERVICES</h5></div><br><br>
                <div class='col-md-12' style='text-align:center;'><h6><?php echo FORMTITLE('EEF')?></h6></div><br>
               
                <div class='col-md-12'>
                    <h5>APPLICANT'S INFORMATION</h5>
                </div><br>
                <div class='col-md-9'>
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Name: </div>
                                    <input type="text" id="stLname" placeholder='Last' name="stLname" class="form-control" required>
                                    <input type="text" id="stFname" placeholder='First' name="stFname" class="form-control" required>
                                    <input type="text" id="stMname" placeholder='Middle' name="stMname" class="form-control" required>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Semester : </div>
                                    <div class="input-group-addon"> <input type="checkbox" id="sem" placeholder='' name="sem" class="sem" value='First'> First </div>
                                    <div class="input-group-addon"> <input type="checkbox" id="sem" placeholder='' name="sem" class="sem" value='Second'> Second </div>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Academic Year: </div>
                                    <select class='form-control' id='sy_from' name='sy_from'  required><?php echo GETYEAR_value(); ?></select>
                                    <div class='input-group-addon'> to </div>
                                    <select class='form-control' id='sy_to' name='sy_to'  required><?php echo GETYEAR_value(); ?></select>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-12'>
                            <div class="form-group">
                                <div class="input-group">
                                    <div class="input-group-addon">Applicant Type : </div>
                                    <?php echo APPLICANTTYPES() ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class='col-md-3' style='margin-bottom: 10px;'>
                    <div class='col-md-12'>
                        <img id="img_wrap" alt='Recent 2x2 Picture' class="previewImage01 image-wrap" src="assets/images/2x2.png" style="object-fit: cover;border:3px solid #b7b1b1;width:2in;height:2in;">

                        <div class="image-upload" style="margin-top: 5px;margin-left: 52px;">
                        <input type="file" name="avatar" id="files" class="btn-inputfile share"  required/>
                        <label for="files" class="btn default" style="font-size: 16px;"><i class="fa fa-file-image-o"></i> Select </label>
                        </div>
                    </div>
                </div>
                <div class='col-md-5'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Birthdate: </div>
                            <input type="date" id="stBod" placeholder='' name="stBod" class="form-control" required >
                        </div>
                    </div>
                </div>
                <div class='col-md-7'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Last School Attended: </div>
                            <input type="text" id="stLSA" placeholder='' name="stLSA" class="form-control" >
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Sex at Birth: </div>
                            <div class="input-group-addon"> <input type="checkbox" id="stSex" placeholder='' name="stSex" class="stSex" value='M'> Male </div>
                            <div class="input-group-addon"> <input type="checkbox" id="stSex" placeholder='' name="stSex" class="stSex" value='F'> Female </div>
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Learner's Reference #: </div>
                            <input type="text" id="stLRN" placeholder='' name="stLRN" class="form-control">
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Track and Strand: </div>
                            <select name="stTS" id="stTS" class='form-control'>
                            <?php echo strandList() ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Age: </div>
                            <input type="text" id="stAge" placeholder='' name="stAge" class="form-control" required>
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Cellphone #: </div>
                            <input type="text" id="stCPNo" placeholder='' name="stCPNo" class="form-control"  required>
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Email Address: </div>
                            <input type="text" id="stEmail" placeholder='' name="stEmail" class="form-control"  required>
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                   
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Preferred Course: </div>
                            <select name="stPrefCourse" id="stPrefCourse" class='form-control' required >
                            <?php echo GETALLPROGRAMS(); ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class='col-md-4'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Campus Intended to Enroll: </div>
                            <input type="text" id="stCIE" placeholder='' name="stCIE" class="form-control">
                        </div>
                    </div>
                </div style='margin-top: 10px;'>
                <div class='col-md-12'>
                    <div class="form-group">
                        <div class="input-group">
                            <div class="input-group-addon">Complete Permanent Address: </div>
                            <select class="form-control select2" id="province" name="province" onchange='getProvince()'>
                                <option value="">&mdash; Please Choose Province &mdash;</option>
                                <?php 
                                $getProvince = mysql_query("SELECT * FROM refprovince");
                                while($row_province = mysql_fetch_array($getProvince)){
                                    echo "<option value='".$row_province['id']."'>".$row_province['provDesc']."</option>";
                                }
                                ?>
                            </select>
                            <select class="form-control select2" id="city" name="city" onchange='getCities()'>
                                
                            </select>
                            <select class="form-control select2" id="brgy" name="brgy">
                                
                            </select>
                            <input type="text" id="streetName" placeholder='Street' name="streetName" class="form-control" required >
                            <!-- <input type="text" id="stBrgy" placeholder='Brgy/District/Locally' name="stBrgy" class="form-control"  required>
                            <input type="text" id="stCity" placeholder='City/Municipality' name="stCity" class="form-control"  required>
                            <input type="text" id="stProv" placeholder='Province' name="stProv" class="form-control" required > -->
                        </div>
                    </div>
                </div>
                <div class='col-md-6'></div>
                <div class='col-md-6'>
                    <label class="checkbox-inline">
                        <input type="checkbox" value="" onchange='infoCheck()' id='checkToRegister'  > <i> I hereby certify that the above information is true and correct</i>
                    </label>
                </div>
                <div class='col-md-12'>

                        <button disabled class="btn btn-primary btn-sm pull-right" id="btn_reg" type="submit"><span class="fas fa-check-circle"></span> Register</button>
                       
                  
                </div>
            </div>
        </div>
    </div>
</div> 
</form>
</div>

<script>
    function getProvince(){
        var province = $("#province").val();
        $.post("ajax/getAllCity.php", {
            province: province
        }, function(data){
            $("#city").html(data);
        })
    }
    function getCities(){
        var city = $("#city").val();
        $.post("ajax/getAllBrgy.php", {
            city: city
        }, function(data){
            $("#brgy").html(data);
        });
    }
   $(document).ready( function(){
    $(".select2").select2();
     $('#JOframe2').on('load', function () {
        $('#loader1').hide();
    });
    $('input.stSO').on('change', function() {
        $('input.stSO').not(this).prop('checked', false);  
    });
    $('input.sem').on('change', function() {
        $('input.sem').not(this).prop('checked', false);  
    });
    $('input.applicantType').on('change', function() {
        $('input.applicantType').not(this).prop('checked', false);  
    });
    $('input.stSex').on('change', function() {
        $('input.stSex').not(this).prop('checked', false);  
    });
    $('input.stSPP').on('change', function() {
        $('input.stSPP').not(this).prop('checked', false);  
    });
    $("#registerStudent").on('submit',(function(e) {
    e.preventDefault();
        $("#btn_reg").prop("disabled", true);
        $("#btn_reg").html("<span class='fa fa-spin fa-spinner'></span> Loading... ");
        $.ajax({
         url:"ajax/register_student.php",
         type: "POST",
         data:  new FormData(this),
         beforeSend: function(){},
         contentType: false,
         cache: false,
         processData:false,
         success: function(data){ 
             if(data > 0){

                var conf = confirm("Would you like to print this form ?");
                if(conf == true){
                    $("#application_form").modal();
                    var url = 'views/print/print_application_form.php?applicant_id='+data;
                    $('#JOframe2').attr('src', url);
                    
                }
                $("#reg_wrapper").html("<h2 style='text-align:center'>Registration Complete!</h2><h4 style='text-align:center'>We will send you a schedule for exam via text message or Email. Thank You!</h4><center><button class='btn btn-sm btn-success' onclick='finishReg()'><span class='fa fa-check-circle'></span> Click to Finish </button></center>");
             }else{
                alert("Error");
             }
        },error: function(){
            alert("Error");
         }           
       });
        
    }));
       
   });
   function printIframe(id){
        var iframe = document.frames ? document.frames[id] : document.getElementById(id);
        var ifWin = iframe.contentWindow || iframe;
        iframe.focus();
        ifWin.printPage();
        return false;
    }
   function modalReload(){
        $('#application_form').on('hidden.bs.modal', function (e) { 
            $("#reg_wrapper").html("<h2 style='text-align:center'>Registration Complete!</h2><h4 style='text-align:center'>We will send you a schedule for exam via text message or Email. Thank You!</h4><center><button class='btn btn-sm btn-success' onclick='finishReg()'><span class='fa fa-check-circle'></span> Click to Finish </button></center>");
        });
    }
   function finishReg(){
       window.location.reload();
   }
   $(".btn-inputfile").change(function () {
        $("#btn-edit").prop("disabled", false);
        var input = document.getElementById('files');
        previewFile(input);
    });

  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
  function infoCheck(){
    var test = ($("#checkToRegister").prop("checked"))?$("#btn_reg").attr("disabled", false):$("#btn_reg").attr("disabled", true);
  }
</script>