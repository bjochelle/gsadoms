<?php 
include 'core/config.php';
$user_id = $_SESSION['user_id'];
$category_id = $_SESSION['category_id'];
$catCode = $_SESSION['cat_code'];
$programID = $_SESSION['program_id'];
checkSession();
$page = (isset($_GET['page']) && $_GET['page'] !='') ? $_GET['page'] : '';
$view = (isset($_GET['view']) && $_GET['view'] !='') ? $_GET['view'] : '';
$sub = (isset($_GET['sub']) && $_GET['sub'] !='') ? $_GET['sub'] : '';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>GSADOMS</title>
    <link rel="icon" type="image/png" href="assets/images/CHMSC_icon.jpg">
    <!-- CSS -->
    <link href="assets/css/font-face.css" rel="stylesheet" media="all">
    <link href="assets/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="assets/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="assets/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" media="all">
    <link href="assets/css/animsition.min.css" rel="stylesheet" media="all">
    <link href="assets/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="assets/css/animate.css" rel="stylesheet" media="all">
    <link href="assets/css/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="assets/css/slick.css" rel="stylesheet" media="all">
    <link href="assets/css/select2.min.css" rel="stylesheet" media="all">
    <link href="assets/css/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link href="assets/css/bootstrap-notify.css" rel="stylesheet">
    <link href="assets/css/theme.css" rel="stylesheet" media="all">
    <link href="assets/css/dataTables.bootstrap4.css" rel="stylesheet">
    <link href="assets/css/bootstrap-tagsinput.css" rel="stylesheet">
    <link href='assets/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
    <link href='assets/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    <link href="assets/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/daterangepicker-bs3.css">
    <link rel="stylesheet" type="text/css" href="assets/css/timepicker.min.css">
  

     <!-- JS -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/jquery.dataTables.js"></script>
    <script src="assets/js/dataTables.bootstrap4.js"></script>
    <script src="assets/js/bootstrap-tagsinput.js"></script>
    <script src='assets/fullcalendar/lib/moment.min.js'></script>
    <script src="assets/fullcalendar/fullcalendar.min.js"></script>
    

  
</head>
<style>
    .navbar-sidebar .navbar__list li.active > a {
        background-color: #a0a0a052;
        border-radius: 10px;
    }
    .sidebar_menus{
        padding-left: 10px;
        font-size: 11px;
    }
    .navbar-sidebar .navbar__list li a i {
        margin-right: 0;
    }
    /* .nav-tabs .nav-link {
        border: 1px solid transparent;
        border-top-left-radius: .25rem;
        border-top-right-radius: .25rem;
        background-color: #e5e5e5;
    } */
    .nav-tabs .nav-item.show .nav-link, .nav-tabs .nav-link.active {
        color: #495057;
        background-color: #fff;
        border-color: #ababab #ababab #fff;
    }
    .nav-tabs .nav-link {
        border: 1px solid #ababab;
        border-top-left-radius: .25rem;
        border-top-right-radius: .25rem;
    }
.select2-container {
    display: block;
    max-width: 100% !important;
    /* width: auto !important; */
    outline: none;
}
.select2-selection{
  height: 38px;
}

</style>
<body class="animsition">
    <div class="page-wrapper">
        <!-- MENU SIDEBAR-->
        <aside class="menu-sidebar d-none d-lg-block">
            <div class="logo">
                <a href="#">
                    <img src="assets/images/chmsc.png" alt="CHMSC-Alijis Office of the Guidance Services" />
                </a>
            </div>
            <?php include 'core/sidebar.php'; ?>
        </aside>
        <!-- END MENU SIDEBAR-->
        <!-- PAGE CONTAINER-->
        <div class="page-container">
            <!-- HEADER DESKTOP-->
            <?php include 'core/header.php'; ?>
            <!-- HEADER DESKTOP-->
            <!-- MAIN CONTENT-->
            <div class="main-content">
                <div class="section__content section__content--p30">
                    <div class="container-fluid">
                    <?php include 'core/routes.php';?>
                    </div>
                </div>
            </div>
            <!-- END MAIN CONTENT-->
            <!-- END PAGE CONTAINER-->
        </div>

    </div>
    <?php require 'views/modals/add_violation_others.php'; ?>
    <?php require 'views/modals/violation_report_modal.php'; ?>
    <?php require 'views/modals/add_direct_referral.php'; ?>
    <?php require 'views/modals/view_event_modal.php'; ?>
    <?php require 'views/modals/set_counseling_schedule.php'; ?>
    <?php require 'views/modals/view_student_profile.php'; ?>
    <?php require 'views/modals/add_classification_modal.php'; ?>
    <?php require 'views/modals/update_class_modal.php'; ?>
    <?php require 'views/modals/add_programs_modal.php'; ?>
    <?php require 'views/modals/update_program_modal.php'; ?>
    <?php require 'views/modals/add_violation_modal.php'; ?>
    <?php require 'views/modals/update_violation_modal.php'; ?>
    <?php require 'views/modals/set_schedule_modal.php'; ?>
    <?php require 'views/modals/add_applicant_type_modal.php'; ?>
    <?php require 'views/modals/update_app_type_modal.php'; ?>
    <?php require 'views/modals/add_track_and_strand.php'; ?>
    <?php require 'views/modals/update_track_strand_modal.php'; ?>
    <?php require 'views/modals/scheduled_exam_list_modal.php'; ?>
    <?php require 'views/modals/add_user_modal.php'; ?>
    <?php require 'views/modals/update_user_modal.php'; ?>
    <?php require 'views/modals/upload_student_list_modal.php'; ?>
    <?php require 'views/modals/responseModal.php'; ?>
    <?php require 'views/modals/add_penalty_modal.php'; ?>
    <?php require 'views/modals/update_penalty_modal.php'; ?>
    <?php require 'views/modals/previous_violations_modal.php'; ?>
    <?php require 'views/modals/counceling_sched_modal.php'; ?>
    <?php require 'views/modals/counselling_feedback_modal.php'; ?>
    <?php require 'views/modals/changeCred_modal.php'; ?>
    <?php require 'views/modals/moveup_students_modal.php'; ?>
    <?php require 'views/modals/change_avatar_modal.php'; ?>
    <?php require 'views/modals/add_event_modal.php'; ?>
    <?php require 'views/modals/application_report_print_modal.php'; ?>

    <?php require 'views/modals/application_form_print_modal.php'; ?>



    
</body>
<!-- Jquery JS-->
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/slick.min.js"></script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/animsition.min.js"></script>
    <script src="assets/js/bootstrap-progressbar.min.js"></script>
    <script src="assets/js/jquery.waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js"></script>
    <script src="assets/js/circle-progress.min.js"></script>
    <script src="assets/js/perfect-scrollbar.js"></script>
    <script src="assets/js/select2.min.js"></script>
    <script src="assets/js/main.js"></script>
    <script type="text/javascript" src="assets/js/daterangepicker.js"></script>
    <script type="text/javascript" srcjs="assets/js/bootstrap-datetimepicker.min.js"></script>
    <script src="assets/js/timepicker.min.js"></script>
   
   
<script>
$(document).ready( function(){
    $(".select2").select2();
    $('#daterange').daterangepicker();
    $(".timepicker").timepicker({
        timeFormat: 'h:mm p'
    }); 

    $("#avatarUpload").on('submit',(function(e) {
    e.preventDefault();
        $.ajax({
         url:"ajax/avatar_upload.php",
         type: "POST",
         data:  new FormData(this),
         beforeSend: function(){},
         contentType: false,
         cache: false,
         processData:false,
         success: function(data)
            {
              alert("Successfully Uploaded");
                window.location.reload();
            },
             error: function() 
         {
            alert("Error");
         }           
       });
        
    }));
})
    function printIframe(id){
        var iframe = document.frames ? document.frames[id] : document.getElementById(id);
        var ifWin = iframe.contentWindow || iframe;
        iframe.focus();
        ifWin.printPage();
        return false;
    }
    function logout(){
        window.location.href = 'ajax/logout.php';
    }
    $(".btn-inputfile").change(function () {
      $("#btn-edit").prop("disabled", false);
      var input = document.getElementById('files');
      previewFile(input);
  });

  function previewFile(input) {
    var file = input.files[0];
    var reader  = new FileReader();
    reader.addEventListener("load", function () {
      $('.previewImage01').attr('src', reader.result);

    }, false);
    if (file) {
      reader.readAsDataURL(file);
    }
  }
    function gotoTrans(id,type,student){
        var location = (type == 'A') ? window.location='index.php?page=student-applicants' : ((type == 'C')? window.location='index.php?page=counceling': ((type == 'R')?window.location='index.php?page=dashboard':window.location='index.php?page=counselling-records&stID='+student))
        $.post("ajax/read_notif_.php", {
            id: id
        }, function(data){
            if(data > 0){
                location
            }else{
                alert("Failed");
            }
        })
       
    }
    function changeCredModal(){
        $("#changeCred").modal();
    }
    function changeAvatarModal(){
        $("#changeAvatar").modal();
    }
    function updateCred(){
        var newP = $("#newPass").val();
        var newU = $("#newuser").val();
        $("#btn_cred").prop("disabled", true);
        $("#btn_cred").html("<span class='fa fa-spin fa-spinner'></span> Loading");
        $.post("ajax/change_cred.php", {
            newP: newP,
            newU: newU
        }, function(data){
            if(data > 0){
                var conf = confirm("Do you want to login again?");
                if(conf == true){
                    logout();
                }else{
                    window.location.reload();
                }
            }else{
                alert("Failed");
            }

            $("#btn_cred").prop("disabled", false);
            $("#btn_cred").html("<span class='fas fa-check-circle'></span> Save Changes");
        })
    }
    function checkconfPass(){
        var newP = $("#newPass").val();
        var confP = $("#confPass").val();
        if(confP == newP){
            $("#btn_cred").attr("disabled", false);
            $("#iconResultc").html("<span class='fa fa-check-circle' style='color:green'></span>");
        }else{
            $("#btn_cred").attr("disabled", true);
            $("#iconResultc").html("<span class='fa fa-close' style='color:red'></span>");
        }
    }
    function checkOldPass(){
        var oldPass = $("#oldPass").val();
        $.post("ajax/password_checker.php", {
            oldPass: oldPass
        }, function(data){
            if(data > 0){
                $("#newPass").attr("readonly", false);
                $("#confPass").attr("readonly", false);
                $("#btn_cred").attr("disabled", false);
                $("#iconResult").html("<span class='fa fa-check-circle' style='color:green'></span>");
            }else{
                $("#newPass").attr("readonly", true);
                $("#confPass").attr("readonly", true);
                $("#newPass").val("");
                $("#confPass").val("");
                $("#btn_cred").attr("disabled", true);
                $("#iconResult").html("<span class='fa fa-close' style='color:red'></span>");
            }
        });
    }
    function alert_custom(icon_not,title,message,type_not){
        $.notify({
            icon: icon_not,
            title: title,
            message: message
        },{
            type: type_not,
            placement: {
                from: "bottom",
                align: "right"
            }
        })
    }
    // function gotoUsers(){
    //     alert("Test")
    //     window.location = 'index.php?page=users';
    // }
</script>

</html>
<!-- end document-->
