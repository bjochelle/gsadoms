<?php
if($page == 'dashboard'){
	require 'views/dashboard.php';
}else if($page == 'classifications'){
	require 'views/user_classification.php';
}else if($page == 'programs'){
	require 'views/course_programs.php';
}else if($page == 'stanine-standard'){
	require 'views/stanine_standard.php';
}else if($page == 'violation-lists'){
	require 'views/violation_lists.php';
}else if($page == 'student-applicants'){
	require 'views/applicants.php';
}else if($page == 'applicant-type-lists'){
	require 'views/applicant_type_list.php';
}else if($page == 'track-and-strand'){
	require 'views/track_and_strand.php';
}else if($page == 'users'){
	require 'views/users.php';
}else if($page == 'students'){
	require 'views/students.php';
}else if($page == 'student-profile'){
	require 'views/student_profile.php';
}else if($page == 'penalty-lists'){
	require 'views/penalty_list.php';
}else if($page == 'violator-lists'){
	require 'views/violator_list.php';
}else if($page == 'add-violator'){
	require 'views/add_violator.php';
}else if($page == 'student-per-program'){
	require 'views/student_per_course.php';
}else if($page == 'counceling'){
	require 'views/counceling.php';
}else if($page == 'students-list'){
	require 'views/student_for_counceling.php';
}else if($page == 'counselling-records'){
	require 'views/counselling_records.php';
}else if($page == 'applicants-per-program'){
	require 'views/applicants_per_program.php';
}else if($page == 'violation-report'){
	require 'views/violation_report.php';
}else if($page == 'application-report'){
	require 'views/application_report.php';
}else if($page == 'notifications'){
	require 'views/notifications.php';
}else if($page == 'profiling-report'){
	require 'views/profiling_report.php';
}else if($page == 'summary-applicants'){
	require 'views/summary_applicants.php';
}else{	
	require 'views/404.php';
}
?>