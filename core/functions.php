<?php 
function checkSession(){
	if(!isset($_SESSION['user_id'])){
	header("Location: login.php");
	}
}
function getCurrentDate(){
	ini_set('date.timezone','UTC');
	date_default_timezone_set('UTC');
	$today = date('H:i:s');
	$date = date('Y-m-d H:i:s', strtotime($today)+28800);
	
	return $date;
}

function clean($str) {
    $str = @trim($str);
    if(get_magic_quotes_gpc()) {
        $str = stripslashes($str);
    }
    return mysql_real_escape_string($str);
}
function select_query($type , $table , $params){

	$select_query = mysql_query("SELECT $type FROM $table WHERE $params")or die(mysql_error());
	$fetch = mysql_fetch_assoc($select_query);
	return $fetch;

}
function insert_query($table_name, $form_data , $last_id){
    $fields = array_keys($form_data);

    $sql = "INSERT INTO ".$table_name."
    (`".implode('`,`', $fields)."`)
    VALUES('".implode("','", $form_data)."')";

    $return_insert = mysql_query($sql)or die(mysql_error());
    $lastID = mysql_insert_id();

    if($last_id == 'Y'){
        if($return_insert){
            $val = $lastID;
        }else{
            $val = 0;
        }
    }else{
        if($return_insert){
            $val = 1;
        }else{
            $val = 0;
        }
    }

    return $val;
}

function delete_query($table_name, $where_clause=''){
    $whereSQL = '';
    if(!empty($where_clause)){
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE'){
            $whereSQL = " WHERE ".$where_clause;
        }else{
            $whereSQL = " ".trim($where_clause);
        }
    }
    $sql = "DELETE FROM ".$table_name.$whereSQL;
    
    $return_delete = mysql_query($sql);
    
    if($return_delete){
    	echo 1;
    }else{
    	echo 0;
    }
}

function update_query($table_name, $form_data, $where_clause=''){
    $whereSQL = '';
    if(!empty($where_clause)){
        if(substr(strtoupper(trim($where_clause)), 0, 5) != 'WHERE'){
            $whereSQL = " WHERE ".$where_clause;
        }else{
            $whereSQL = " ".trim($where_clause);
        }
    }
    $sql = "UPDATE ".$table_name." SET ";
    $sets = array();
    foreach($form_data as $column => $value)
    {
         $sets[] = "`".$column."` = '".$value."'";
    }
    $sql .= implode(', ', $sets);
    $sql .= $whereSQL;

    $return_query = mysql_query($sql);
    if($return_query){
    	echo 1;
    }else{
    	echo 0;
    }
}
function GETPRGM_STANINE($programID){
    $val = mysql_fetch_array(mysql_query("SELECT stanine_val FROM tbl_stanine WHERE program_id = '$programID'"));

    return $val[0];
}
function GETCATEGORIES(){
    $data = "<option value=''> &mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_classification WHERE class_code != 'S'");
    while($row = mysql_fetch_array($query)){
        $data .= "<option value='".$row['class_id']."'>".$row['classification_name']."</option>";
    }

    return $data;
}
function GETYEAR_value(){
    $currentYear = date("Y", strtotime("+1 Year", strtotime(getCurrentDate())));
    $startYear = $currentYear - 10;
    $option = "<option value=''>  Year </option> ";
    do{
        $option .= "<option value='$currentYear'>$currentYear</option>";
        $currentYear--;
    }while($currentYear >= $startYear);

    return $option;
}

function GETALLPROGRAMS(){
    $option = "<option value=''> &mdash; Please Select &mdash; </option> ";
    $query = mysql_query("SELECT * FROM tbl_program");
    while($row = mysql_fetch_array($query)){
        $prgmID = $row['program_id'];
        $prgmNAME = $row['program_name'];
        $option .= "<option value='$prgmID'> $prgmNAME </option> ";
    }
    return $option;
}
function GETEXAMSCHEDULE($stdID){
    $query = mysql_fetch_array(mysql_query("SELECT * FROM tbl_exam_schedules WHERE student_id = '$stdID'"));
    $sch = date("F d, Y", strtotime($query['sch_date']))." ".date("h:i A", strtotime($query['sch_time']));

    $sched = (!empty($query['sch_date']))?$sch:"a";

    return $sched;
}
function educBack_list(){
    $data = "";
    $list = array("COLLEGE","VOCATIONAL","HIGH SCHOOL","ELEMENTARY");
    foreach($list as $name){
        $data .= "<tr>";
            $data .= "<td>$name</td>";
            $data .= "<td><textarea class='form-control' rows='3' style='resize:none'></textarea></td>";
            $data .= "<td><div class='form-group'>
                            <div class='input-group'>
                                <select class='form-control'>".GETYEAR_value()."</select>
                                <div class='input-group-addon'> &mdash; </div>
                                <select class='form-control'>".GETYEAR_value()."</select>
                            </div>
                        </div>
                    </td>";
            $data .= "<td>
                        <div class='form-group'>
                            <div class='input-group'>
                                <div class='input-group-addon' style='margin-right: 0.5%;'> <input type='checkbox' class='stSPP'> Public </div>
                                <div class='input-group-addon' style='margin-right: 0.5%;'> <input type='checkbox' class='stSPP'> Private </div>
                            </div>
                        </div>
                    </td>";
            $data .= "<td><textarea class='form-control' rows='3' style='resize:none'></textarea></td>";
        $data .= "</tr>";
    }

    return $data;
}
function familyBackground(){
    $data = "";
    $list = array("NAME","AGE","CONTACT NO.","BIRTHPLACE","RELIGION","NATIONALITY","LIVING OR DECEASED","EDUCATIONAL ATTAINMENT","OCCUPATION","NAME OF EMPLOYER","ADDRESS OF EMPLOYER");
    foreach($list as $name){
        $data .= "<tr>";
            $data .= "<td>$name</td>";
            $data .= "<td><input type='text' class='form-control'></td>";
            $data .= "<td><input type='text' class='form-control'></td>";
            // $data .= "<td><textarea class='form-control' rows='3' style='resize:none'></textarea></td>";
            // $data .= "<td>
            //             <div class='form-group'>
            //                 <div class='input-group'>
            //                     <div class='input-group-addon' style='margin-right: 0.5%;'> <input type='checkbox' class='stSPP'> Public </div>
            //                     <div class='input-group-addon' style='margin-right: 0.5%;'> <input type='checkbox' class='stSPP'> Private </div>
            //                 </div>
            //             </div>
            //         </td>";
            // $data .= "<td><textarea class='form-control' rows='3' style='resize:none'></textarea></td>";
        $data .= "</tr>";
    }

    return $data;
}
function FORMTITLE($cat){
    switch($cat){
        case 'EEF':
            $data = "CARLOS HILADO MEMORIAL STATE COLLEGE ENTRANCE TEST APPLICATION FORM <br> (CHMSCETAF)";
        break;
        case 'SII':
            $data = "STUDENT'S INDIVIDUAL INVENTORY";
            break;
    }

    return $data;
}
function APPLICANTTYPES(){
    $data = "";
    $count = 1;
    $query = mysql_query("SELECT * FROM tbl_applicant_type");
    while($row = mysql_fetch_array($query)){
        $id = $row['applicant_type_id'];
        $br = ($count % 4 == 0)?'<br>':'';
        $data .= '<div class="input-group-addon"> <input type="checkbox" value="'.$id.'" id="applicantType" name="applicantType" class="applicantType"> '.$row["type_name"].' </div>';
        $count++;
    }

    return $data;
}
function strandList(){
    $option = "<option value=''> &mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_track_and_strand");
    while($row = mysql_fetch_array($query)){
        $id = $row['ts_id'];
        $name = $row['ts_name'];
        $option .= "<option value='$id'>$name</option>";
    }

    return $option;
}
function applicantName($applicantID){
    $query = mysql_fetch_array(mysql_query("SELECT CONCAT(lastname,' , ',firstname,' ',middlename) FROM tbl_applicants WHERE applicant_id = '$applicantID'"));

    return $query[0];
}

function applicantAddress($applicantID){
    $query = mysql_fetch_array(mysql_query("SELECT CONCAT(address_street,' ',address_brgy,' ',address_city,' ',address_province) FROM tbl_applicants WHERE applicant_id = '$applicantID'"));

    return $query[0];
}
function getExamDate($applicantID){
    $query = mysql_fetch_array(mysql_query("SELECT sch_date FROM tbl_exam_schedules WHERE student_id = '$applicantID'"));

    return date("Y-m-d", strtotime($query[0]));
}
function getName($userID){
    $name = mysql_fetch_array(mysql_query("SELECT name FROM tbl_users WHERE user_id = '$userID'"));

    return $name[0];
}
function getAccess($userID){
    $access = mysql_fetch_array(mysql_query("SELECT * FROM tbl_users as u,tbl_classification as c WHERE u.category_id = c.class_id AND u.user_id = '$userID'"));

    $accessName = ($access['category_id'] == 0)?"Guidance Counselor":$access['classification_name'];

    return $accessName;
}
function getCategName($category_id){
    $query = mysql_fetch_array(mysql_query("SELECT classification_name FROM tbl_classification WHERE class_id = '$category_id'"));

    return $query[0];
}
function getProgramName($userID){
    $query = mysql_fetch_array(mysql_query("SELECT * FROM tbl_users as u, tbl_program as p, tbl_classification as c WHERE u.program_id = p.program_id AND u.category_id = c.class_id AND u.user_id = '$userID'"));
    $prgm = ($query['class_code'] == 'C')?$query['program_name']:"<span style='color: red'>N/A</span>";

    return $prgm;
}
function getActiveStudents(){
    $data = "<option value=''> &mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_students WHERE is_active = 0");
    while($row = mysql_fetch_array($query)){
        $NAME = $row['student_fname']." ".$row['student_lname'];
        $data .= "<option value='".$row['student_id']."'>".$NAME."</option>";
    }

    return $data;
}
function getViolations(){
    $data = "<option value=''> &mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_violation");
    while($row = mysql_fetch_array($query)){
        $data .= "<option value='".$row['violation_id']."'>".$row['name']."</option>";
    }

    return $data;
}
function getPenalty(){
    $data = "<option value=''> &mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_penalties");
    while($row = mysql_fetch_array($query)){
        $data .= "<option value='".$row['penalty_id']."'>".$row['penalty_name']."</option>";
    }

    return $data;
}
function getTodayCounselling(){
    $curdate = date("Y-m-d", strtotime(getCurrentDate()));
    $data = "";
    $query = mysql_query("SELECT CONCAT(s.student_lname,', ',s.student_fname,' ',s.student_mname) as student FROM tbl_student_counceling as c, tbl_students as s WHERE c.st_id = s.student_id AND c.sc_date = '$curdate'");
    $count = mysql_num_rows($query);
    if($count > 0){
        while($row = mysql_fetch_array($query)){
            $data .= "<li>$row[0]</li>";
        }
    }else{
        $data = "<h5>No Counseling for today.</h5>";
    }
    

    return $data;

}
function getTodayExam(){
    $curdate = date("Y-m-d", strtotime(getCurrentDate()));
    $data = "";
    $query = mysql_query("SELECT CONCAT(s.student_lname,', ',s.student_fname,' ',s.student_mname) as student FROM tbl_exam_schedules as e, tbl_students as s WHERE e.student_id = s.student_id AND DATE_FORMAT(e.sch_date,'%Y-%m-%d') = '$curdate'");
    $count = mysql_num_rows($query);
    if($count > 0){
        while($row = mysql_fetch_array($query)){
            $data .= "<li>$row[0]</li>";
        }
    }else{
        $data = "<h5>No Scheduled Exam for today.</h5>";
    }
    

    return $data;

}
function getAllNotif($receiver){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_notification WHERE notif_receiver = '$receiver' AND notif_status = 0 ORDER BY notif_datetime DESC");
    while($row = mysql_fetch_array($query)){
        $data .= " <div class='notifi__item' onclick='gotoTrans(\"".$row['notif_id']."\",\"".$row['notif_type']."\",\"".$row['student_id']."\")'>
                    <div class='bg-c1 img-cir img-40'>
                        <i class='zmdi zmdi-notifications'></i>
                    </div>
                    <div class='content'>
                        <p>".$row['notif_content']."</p>
                        <span class='date'>".date("F d, Y h:i A", strtotime($row['notif_datetime']))."</span>
                    </div></div>";
    }

    return $data;
   
}
function NotifCounter($receiver){
    $query = mysql_fetch_array(mysql_query("SELECT count(*) FROM tbl_notification WHERE notif_receiver = '$receiver' AND notif_status = 0"));
    

    return $query[0];
   
}
function levels(){
    $array = array("COLLEGE" => 1, "VOCATIONAL" => 2, "HIGHSCHOOL" => 3, "ELEMENTARY" => 4);

    return $array;
}
function tableContent($st_id){
    $list = levels();
    foreach ($list as $key => $value) {
        $getVal = mysql_fetch_array(mysql_query("SELECT * FROM tbl_student_educational_background WHERE student_id = '$st_id' AND `level` = '$value'"));
        $savedVal = (!empty($getVal['student_id']))?$getVal['school'].":".$getVal['year_covered'].":".$getVal['public_private'].":".$getVal['honor_received'].":".$value:"a:a:a:a:a";
        $public = ($getVal['public_private'] == 'public')?"selected":"";
        $private = ($getVal['public_private'] == 'private')?"selected":"";
        //<textarea style='resize: none' id='pp$value'onkeyup='getVal(\"".$value."\")' row='5' class='form-control'>".$getVal['public_private']."</textarea>
        echo "<tr>";
            echo "<td>$key</td>";
            echo "<td><textarea style='resize: none' id='sch$value' onkeyup='getVal(\"".$value."\")' row='5' class='form-control'>".$getVal['school']."</textarea></td>";
            echo "<td><textarea style='resize: none' id='yc$value' onkeyup='getVal(\"".$value."\")' row='5' class='form-control'>".$getVal['year_covered']."</textarea></td>";
            echo "<td><select id='pp$value' class='form-control' onchange='getPPVal(\"".$value."\")'>
                        <option  value=''>&mdash; Select &mdash; </option>
                        <option $public value='public'>Public</option>
                        <option $private value='private'>Private</option>
                    </select>
                </td>";
            echo "<td><textarea style='resize: none' id='hr$value' onkeyup='getVal(\"".$value."\")' row='5' class='form-control'>".$getVal['honor_received']."</textarea></td>";
        echo "</tr>";
        echo "<input type='hidden' value='$savedVal' name='edBack' id='val$value' class='form-control'></textarea>";
    }
}
function tableContentPsych($st_id){
    for ($i=1; $i <= 5 ; $i++) { 
        $getVal = mysql_fetch_array(mysql_query("SELECT * FROM tbl_student_psych_test WHERE student_id = '$st_id' AND `test_count` = '$i'"));
        $savedVal = (!empty($getVal['student_id']))?$getVal['date'].":".$getVal['test_name'].":".$getVal['result'].":".$getVal['interpretation'].":".$i:"a:a:a:a:0";
        echo "<tr>";
            echo "<td><input type='date' id='dateTest".$i."' value='".$getVal['date']."' onchange='getDateTest(".$i.")' class='form-control'></td>";
            echo "<td><textarea style='resize: none' onkeyup='getTestResult(".$i.")' id='test".$i."' row='5' class='form-control'>".$getVal['test_name']."</textarea></td>";
            echo "<td><textarea style='resize: none' onkeyup='getTestResult(".$i.")' id='result".$i."' row='5' class='form-control'>".$getVal['result']."</textarea></td>";
            echo "<td><textarea style='resize: none' onkeyup='getTestResult(".$i.")' id='interp".$i."' row='5' class='form-control'>".$getVal['interpretation']."</textarea></td>";
        echo "</tr>";
        echo "<input type='hidden' value='$savedVal' name='Testresult' id='Testresult$i' class='form-control'></textarea>";
    }
}
function family_head(){
    $array = array("" => "", "Mother" => 1, "Father" => 2);

    return $array;
}
function family_body(){
    $array = array(
                    "Name" => 1,
                    "Contact" => 2,
                    "Birthplace" => 3,
                    "Religion" => 4,
                    "Nationality" => 5,
                    "Living or Deceased" => 6,
                    "Education Attainment" => 7,
                    "Occupation" => 8,
                    "Name of Employer" => 9,
                    "Address of Employer" => 10
    );

    return $array;
}
function householdMonthlyIncomeList($stid){
    $data = "";
    $query = mysql_query("SELECT * FROM tbl_income_range");
    while($row = mysql_fetch_array($query)){
        $getId = mysql_fetch_array(mysql_query("SELECT household_income FROM tbl_students WHERE student_id = '$stid'"));
        $from = ($row['range_from'] == 0)?"Below":$row['range_from'];
        $to = ($row['range_to'] == 0)?"Above":$row['range_to'];
        $range = "&#8369; ".$from." - &#8369;".$to;
        $checked = ($row['range_id'] == $getId[0])?"checked":"";
        $data .= "<div class='input-group-addon'> <input type='checkbox' $checked id='' placeholder='' name='hIncome' class='hIncome' value='".$row['range_id']."'> $range </div>";
    }
    $getOthers = mysql_fetch_array(mysql_query("SELECT household_income,household_income_others FROM tbl_students WHERE student_id = '$stid'"));
    $otherCheck = ($getOthers[0] < 0)?"checked":"";
    $data .= "<div class='input-group-addon'> <input type='checkbox' $otherCheck onchange='inputIncomeOthers()' id='hIncome' placeholder='' name='hIncome' class='hIncome' value='-1'> Others <input type'number' value='".$getOthers[1]."' readonly id='incomeOthers' class='form-control'> </div>";

    return $data;
}

function health(){
    $array = array("Vision" => 1, "Hearing" => 2, "Speech" => 3, "Health in General" => 4, "Others" => 5);
    return $array;
}
function healthList($stID){
    $list = health();
    foreach ($list as $name => $val) {

        $getVal = mysql_fetch_array(mysql_query("SELECT * FROM tbl_student_health WHERE student_id = '$stID' AND `health` = '$name'"));
        $checked = ($getVal['health'] == $name)?"checked":"";
        $savedVal = (!empty($getVal['student_id']))?$getVal['health'].":".$getVal['heath_specify']:"a:a";

        echo "<div class='input-group-addon'> <input type='checkbox' onchange='getHealthVal(".$val.")' id='healthVal$val' name='healthVal' class='healthVal' $checked value='$name'> $name  <div class='input-group-addon'> Specify <input type='text' id='getHealthVal$val' value='".$getVal['heath_specify']."' class='form-control' onkeyup='getHealthValue(".$val.")' name='getHealthVal' class='getHealthVal'> <input type='hidden' id='savedResult$val' name='savedResultHealth' value='$savedVal' class='savedResult'></div></div>";
    }
}
function activities(){
    $array = array("Sports" => 1, "Club" => 2, "Religion" => 3, "Others" => 4);
    return $array;
}

function activityList($stid){
    $list = activities();
    foreach ($list as $name => $val) {

        $getVal = mysql_fetch_array(mysql_query("SELECT * FROM tbl_student_interested_org WHERE student_id = '$stid' AND `org` = '$name'"));
        $checked = ($getVal['org'] == $name)?"checked":"";
        $savedVal = (!empty($getVal['student_id']))?$getVal['org'].":".$getVal['org_specify']:"a:a";

        echo "<div class='input-group-addon' style='width: 100%;'> <input type='checkbox' onchange='getactList(".$val.")' id='actList$val' name='actList' $checked class='actList' value='$name'> $name  <div class='input-group-addon'> Specify <input type='text' onkeyup='getactListVal(".$val.")' value='".$getVal['org_specify']."' id='actSpec$val' class='form-control' name='actSpec' class='actSpec'> <input type='hidden' id='savedResultAct$val' name='savedResultAct' value='$savedVal' class='savedResultAct'></div></div>";
    }
}

function deceasedParents($stID){
    $array = array("Mother" => 1, "Father" => 2);
    foreach ($array as $key => $value) {
        $getVal = mysql_fetch_array(mysql_query("SELECT * FROM tbl_student_deceased_member WHERE student_id = '$stID' AND `member` = '$value'"));
        $checked = ($getVal['member'] == $value)?"checked":"";
        $savedVal = (!empty($getVal['student_id']))?"1:".$getVal['date'].":".$getVal['reason'].":".$value:"a:a:a:a";
        echo "<div class='input-group-addon' style='width: 315px;'> 
                <input type='checkbox' onchange='checkifYes(".$value.")' id='deceasedCheck$value' name='deceasedCheck' class='deceasedCheck' $checked value='$value'>
                $key
                <div class='input-group'><div class='input-group-addon'>Date: </div><input onchange='checkifYes(".$value.")' type='date' value='".$getVal['date']."' id='dateDec$value' class='form-control'></div>
                <div class='input-group'><div class='input-group-addon'>Cause of Death: </div><textarea id='CauseDec$value' style='resize:none' onkeyup='causeDeceased(".$value.")' rows='4' class='form-control'>".$getVal['reason']."</textarea></div>
                <input type='hidden' id='saveResult$value' value='$savedVal' name='saveResult'>
            </div>";
    }
}
function financeSchooling($stID){
    $array = array("Parents" => 1, "Relative" => 2, "Spouse" => 3, "Brother/Sister" => 4, "Scholarship" => 5, "Self Supporting/Working Student" => 6);
    foreach ($array as $key => $value) {

        $getVal = mysql_fetch_array(mysql_query("SELECT * FROM tbl_student_finance_schooling WHERE student_id = '$stID' AND `financer` = '$key'"));
        $checked = ($getVal['financer'] == $key)?"checked":"";
        $savedVal = (!empty($getVal['student_id']))?$getVal['financer'].":".$getVal['financer_specify']:"a:a";

        $text = ($value == 6 || $value == 5)?"<input type='text' value='".$getVal['financer_specify']."' onkeyup='getSpecFinance(".$value.")' class='form-control' id='financeSpec$value'>":"";
        
        echo "<div class='input-group-addon'> <input type='checkbox' onchange='getFinance(".$value.")' id='financer$value'  name='financer' $checked class='financer' value='$key'> $key $text</div>";
        echo "<input type='hidden' id='saveResultFS$value' value='a:a' name='saveResultFS'>";
    }
}
function getAvatar($userid){
    $ret = mysql_fetch_array(mysql_query("SELECT avatar FROM tbl_users WHERE user_id = '$userid'"));
    return $ret[0];
}
function student_lists(){
    $data = "<option value=''> &mdash; Please Choose &mdash; </option>";
    $student = mysql_query("SELECT * FROM tbl_students");
    while($row = mysql_fetch_array($student)){
        $data .= "<option value='".$row['student_id']."'>".$row['student_fname']." ".$row['student_lname']."</option>";
    }
    return $data;
}
function getViolationLevels(){
    $data = "<option value=''>&mdash; Please Choose &mdash; </option>";
    $query = mysql_query("SELECT * FROM tbl_penalties");
    while($row = mysql_fetch_array($query)){
        $data .= "<option value='".$row['penalty_id']."'>".$row['level']."</option>";
    }
    return $data;
}
