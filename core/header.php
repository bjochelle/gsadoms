<style>
.notifi-dropdown{
    max-height: 350px;
    overflow: auto;
}
</style>
<header class="header-desktop">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
            <div class="header-button pull-right">
                <div class="noti-wrap">
                    <div class="noti__item js-item-menu">
                        <i class="zmdi zmdi-notifications"></i>
                        <span class="quantity"><?php echo NotifCounter($user_id) ?></span>
                        <div class="notifi-dropdown js-dropdown">
                            <div class="notifi__title">
                                <p>You have <?php echo NotifCounter($user_id) ?> Notifications</p>
                            </div>
                            
                                <?php echo getAllNotif($user_id); ?>
                        
                            <div class="notifi__footer">
                                <a href="#" onclick="window.location='index.php?page=notifications'">All notifications</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="account-wrap">
                    <div class="account-item clearfix js-item-menu">
                        <div class="image">
                        <?php if(!empty(getAvatar($user_id))) { ?>
                            <img src="assets/images/<?php echo getAvatar($user_id)?>" style="object-fit: cover; height: 54px;width: 56px;border-radius: 50%;" alt="John Doe" />
                        <?php }else { ?>
                            <img src="assets/images/avatar.png" style="object-fit: cover; height: 54px;width: 56px;border-radius: 50%;" alt="John Doe" />
                        <?php } ?>
                        </div>
                        <div class="content">
                            <a class="js-acc-btn" href="#"><?php echo getName($user_id); ?></a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="info clearfix">
                                <div class="image">
                                    <a href="#">
                                    <?php if(!empty(getAvatar($user_id))) { ?>
                                        <img src="assets/images/<?php echo getAvatar($user_id)?>" style="object-fit: cover; height: 54px;width: 56px;border-radius: 50%;" alt="John Doe" />
                                    <?php }else { ?>
                                        <img src="assets/images/avatar.png" style="object-fit: cover; height: 54px;width: 56px;border-radius: 50%;" alt="John Doe" />
                                    <?php } ?>
                                    </a>
                                </div>
                                <div class="content">
                                    <h5 class="name">
                                        <a href="#"><?php echo getName($user_id); ?></a>
                                    </h5>
                                    <span class="email"><?php echo getAccess($user_id)?></span>
                                </div>
                            </div>
                            <?php if($category_id == 0){ ?>
                            <div class="account-dropdown__body">
                                <div class="account-dropdown__item">
                                    <a href="#" onclick="window.location='index.php?page=users'">
                                        <i class="zmdi zmdi-account"></i>Users</a>
                                </div>
                            </div>
                        <?php } ?>
                            <div class="account-dropdown__body">
                                <div class="account-dropdown__item">
                                    <a href="#" onclick="changeCredModal()">
                                        <i class="zmdi zmdi-key"></i>Change Credentials</a>
                                </div>
                            </div>
                            <div class="account-dropdown__body">
                                <div class="account-dropdown__item">
                                    <a href="#" onclick="changeAvatarModal()">
                                        <i class="zmdi zmdi-comment-image"></i>Change Avatar</a>
                                </div>
                            </div>
                            <div class="account-dropdown__footer">
                                <a href="#" onclick='logout()'><i class="zmdi zmdi-power"></i>Logout</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>