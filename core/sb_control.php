<?php
if($page == 'dashboard'){
  $menuActive_dash = 'active';
}else if($page == 'programs'){
  $hasSubOpen= 'open';
  $menuActive_prgm = 'active';
  $display = 'display:block';
}else if($page == 'stanine-standard'){
  $hasSubOpen= 'open';
  $menuActive_ss = 'active';
  $display = 'display:block';
}else if($page == 'student-applicants'){
  $hasSubOpen= 'open';
  $menuActive_sa = 'active';
  $display_sa = 'display:block';
}else if($page == 'applicant-type-lists'){
  $hasSubOpen= 'open';
  $menuActive_atl = 'active';
  $display = 'display:block';
}else if($page == 'track-and-strand'){
  $hasSubOpen= 'open';
  $menuActive_ts = 'active';
  $display = 'display:block';
}else if($page == 'students'){
  $hasSubOpen= 'open';
  $menuActive_std = 'active';
  $display_sa = 'display:block';
}else{
  $menuActive_dash = '';
  $menuActive_prgm = '';
  $menuActive_ss = '';
  $menuActive_sa = '';
  $menuActive_atl = '';
  $menuActive_ts = '';
  $menuActive_std = '';
  $display_sa = 'display:none';
  $display_atl = 'display:none';
}

// else if($page == 'classifications'){
//   $hasSubOpen = 'open';
//   $menuActive_class = 'active';
//   $display = 'display:block';
//}
?>
