<?php require 'sb_control.php'; ?>
<div class="menu-sidebar__content js-scrollbar1">
    <nav class="navbar-sidebar">
        <ul class="list-unstyled navbar__list">
            <li class=''>
                <a href="index.php?page=dashboard" style='padding:5px;'>
                    <i class="fas fa-tachometer-alt" style='padding-left: 10px;'></i> Dashboard</a>
            </li>
            <?php if($category_id == 0){ ?>
            <li class="has-sub">
                <a class="js-arrow " href="#">
                    <i class="fas fa-files-o" style='padding-left: 16px;'></i> Master Data</a>
                <ul class="list-unstyled navbar__sub-list js-sub-list" style=''>
                   <!--  <li class='<?php echo $menuActive_class; ?>'>
                        <a href="index.php?page=classifications" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Classification</a>
                    </li> -->
                    <li class=''>
                        <a href="index.php?page=programs" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Programs</a>
                    </li>
                    <li class=''>
                        <a href="index.php?page=stanine-standard" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Stanine </a>
                    </li>
                    <li class=''>
                        <a href="index.php?page=applicant-type-lists" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Applicant Type </a>
                    </li>
                    <li class=''>
                        <a href="index.php?page=track-and-strand" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Track and Strand </a>
                    </li>
                </ul>
            </li>
            <li class="has-sub" style="margin-top: -13px">
                <a class="js-arrow" href="#">
                    <i class="fas fa-users" style='padding-left: 16px;'></i> Students</a>
                <ul class="list-unstyled navbar__sub-list js-sub-list" style=''>
                    <li class=''>
                        <a href="index.php?page=student-applicants" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Applicants</a>
                    </li>
                    <li class=''>
                        <a href="index.php?page=students" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Existing Students</a>
                    </li>
                </ul>
            </li>
            <li class=''>
                <a href="index.php?page=counceling" style='padding:5px;'>
                    <i class="fas fa-users" style='padding-left: 10px;'></i> Counseling</a>
            </li>
            <li class="has-sub">
                <a class="js-arrow " href="#">
                    <i class="fas fa-print" style='padding-left: 16px;'></i> Reports</a>
                <ul class="list-unstyled navbar__sub-list js-sub-list" style=''>
                    <li class=''>
                        <a href="index.php?page=application-report" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Application </a>
                    </li>
                   
                    <li class=''>
                        <a href="index.php?page=profiling-report" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Profiling </a>
                    </li>
                    
                </ul>
            </li>
            <?php } ?>

            <?php if($catCode == 'DO') { ?>
                <li class="has-sub">
                <a class="js-arrow " href="#">
                    <i class="fas fa-files-o" style='padding-left: 16px;'></i> Master Data</a>
                <ul class="list-unstyled navbar__sub-list js-sub-list" style=''>
                    <li class=''>
                        <a href="index.php?page=violation-lists" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Violations </a>
                    </li>
                    <li class=''>
                        <a href="index.php?page=penalty-lists" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Penalties </a>
                    </li>
                </ul>
            </li>
            <?php } ?>
            
            <?php if($catCode == 'DO' || $catCode == 'D'){ ?>
            <li class=''>
                <a href="index.php?page=violator-lists" style='padding:5px;'>
                    <i class="fas fa-th-list" style='padding-left: 10px;'></i> Violators</a>
            </li>
            <li class=''>
                <a href="index.php?page=students-list" style='padding:5px;'> <i class="fas fa-users" style='padding-left: 10px;'></i> Students</a>
            </li>
            <?php } ?>
            <?php if($catCode == 'C'){ ?>
            <li class=''>
                <a href="index.php?page=applicants-per-program" style='padding:5px;'>
                    <i class="fas fa-th-list" style='padding-left: 10px;'></i> Applicants </a>
            </li>
            <li class=''>
                <a href="index.php?page=student-per-program" style='padding:5px;'>
                    <i class="fas fa-users" style='padding-left: 10px;'></i> Students</a>
            </li>
            <?php } ?>

            <?php if($catCode == 'D') { ?>
                <li class="has-sub">
                    <a class="js-arrow " href="#">
                        <i class="fas fa-print" style='padding-left: 16px;'></i> Reports</a>
                    <ul class="list-unstyled navbar__sub-list js-sub-list" style=''>
                    <!--  <li class='<?php echo $menuActive_class; ?>'>
                            <a href="index.php?page=classifications" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Classification</a>
                        </li> -->
                    
                        <li class=''>
                            <a href="index.php?page=profiling-report" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Profiling </a>
                            
                        </li>

                        <li class=''>
                            <a href="index.php?page=summary-applicants" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Applicants Summary</a>
                        </li>
                        
                    </ul>
                </li>
            <?php } ?>
            <?php if($catCode == 'DO') { ?>
                <li class="has-sub">
                <a class="js-arrow " href="#">
                    <i class="fas fa-print" style='padding-left: 16px;'></i> Reports</a>
                <ul class="list-unstyled navbar__sub-list js-sub-list" style=''>
                    <li class=''>
                        <a href="index.php?page=violation-report" style='padding:5px;'> <i class="fas fa-circle sidebar_menus"></i> Violation</a>
                    </li>
                    
                </ul>
            </li>
            <?php } ?>
        </ul>
    </nav>
</div>