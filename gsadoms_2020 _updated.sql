-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 18, 2020 at 04:45 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.39

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gsadoms_2020`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_applicants`
--

CREATE TABLE `tbl_applicants` (
  `applicant_id` int(11) UNSIGNED NOT NULL,
  `firstname` varchar(100) NOT NULL DEFAULT '',
  `middlename` varchar(100) NOT NULL DEFAULT '',
  `lastname` varchar(100) NOT NULL DEFAULT '',
  `semester` varchar(10) NOT NULL DEFAULT '',
  `acad_year_from` year(4) NOT NULL,
  `acad_year_to` year(4) NOT NULL,
  `applicant_type` int(11) NOT NULL,
  `applicant_2x2` varchar(100) NOT NULL DEFAULT '',
  `birthdate` date NOT NULL,
  `last_school` text NOT NULL,
  `gender` varchar(20) NOT NULL DEFAULT '',
  `LRN` varchar(50) NOT NULL DEFAULT '',
  `strand` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `cp_no` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(50) NOT NULL DEFAULT '',
  `preferred_course` int(11) NOT NULL,
  `campus_intended` text NOT NULL,
  `address_street` text NOT NULL,
  `address_brgy` text NOT NULL,
  `address_city` text NOT NULL,
  `address_province` text NOT NULL,
  `date_added` datetime NOT NULL,
  `score` int(11) NOT NULL,
  `stanine` decimal(12,3) NOT NULL,
  `status` int(11) NOT NULL,
  `is_archived` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_applicants`
--

INSERT INTO `tbl_applicants` (`applicant_id`, `firstname`, `middlename`, `lastname`, `semester`, `acad_year_from`, `acad_year_to`, `applicant_type`, `applicant_2x2`, `birthdate`, `last_school`, `gender`, `LRN`, `strand`, `age`, `cp_no`, `email`, `preferred_course`, `campus_intended`, `address_street`, `address_brgy`, `address_city`, `address_province`, `date_added`, `score`, `stanine`, `status`, `is_archived`) VALUES
(1, 'Lalisa', 'Kim', 'Manoban', 'First', 2019, 2020, 6, 'black-pink-lisa.jpg', '2000-03-29', 'Seoul International University', 'F', '1234520', 3, 20, '09503785131', 'lilimanoban94@gmail.com', 6, 'Alijis', 'Lizares St', '40', 'Bacolod City', 'Neg Occ', '2020-09-28 14:05:09', 50, '6.000', 3, 1),
(2, 'Jennie', 'Ruby', 'Kim', 'First', 2019, 2020, 4, 'jennie.jpg', '1996-01-16', 'ACG Parnell College', 'F', '123452001', 2, 24, '09385250309', 'miracle.reign101@gmail.com', 3, 'Alijis', 'Araneta', '19', 'Bacolod City', 'Neg Occ', '2020-09-28 14:17:45', 78, '9.000', 3, 1),
(3, 'Ji', 'Soo', 'Kim', 'First', 2020, 2021, 8, 'jisoo.jpg', '1995-01-09', 'Seoul International University', 'F', '0000000000', 3, 25, '09503785131', 'lilimanoban94@gmail.com', 2, 'Alijis', 'Lizares St', '40', 'Bacolod City', 'Neg Occ', '2020-09-30 22:58:12', 45, '5.000', 3, 1),
(4, 'teeessst', 'testt', 'test', 'First', 2020, 2021, 8, 'logo.jpg', '1994-10-22', 'BCNHS', 'M', '000000001', 2, 25, '09503785131', 'miracle.reign101@gmail.com', 4, 'Alijis', '123', 'espanya', 'Isabela City', 'Neg Occ', '2020-10-01 15:58:35', 35, '4.000', 3, 1),
(5, 'teeeeeeesssssst', 'tessssst', 'Test', 'First', 2020, 2021, 8, 'logo1.jpg', '2000-02-12', 'BCNHS', 'F', '00000002', 2, 20, '09503785131', 'miracle.reign101@gmail.com', 2, 'Alijis', '123', '4', 'escalante', 'neg occ', '2020-10-02 10:18:21', 50, '6.000', 3, 1),
(6, 'Christine Joy', 'Lee', 'Teruel', 'First', 2018, 2021, 2, '2.jpg.png', '1994-05-02', 'Seoul International University', 'F', '546245789545', 3, 25, '09101024113', 'rjoenajean@yahoo.com', 6, 'Alijis', 'Deca Homes', 'Cabog', 'Bacolod City', 'Neg Occ', '2020-10-02 23:23:22', 75, '8.000', 3, 1),
(7, 'Jean', 'A', 'Rodrigo', 'First', 2018, 2021, 4, '7.jpg.png', '1998-07-08', 'Seoul International University', 'F', '012345678912', 3, 22, '09101024113', 'rjoenajean@yahoo.com', 3, 'Alijis', 'SouthBay', 'Busay', 'Bago City', 'Neg Occ', '2020-10-02 23:33:17', 40, '4.000', 3, 1),
(8, 'Mallari', 'Roa', 'Steffany', 'First', 2020, 2021, 12, 'rose.jpg', '1994-10-22', 'Negros Occidental High School', 'F', '00000002', 3, 25, '09503785131', 'tala.luna15@gmail.com', 1, 'Alijis', 'Block 10 Lot 24 Country Homes', 'Alijis', 'Bacolod City', 'Negros Occidental', '2020-10-03 16:38:01', 0, '0.000', 0, 0),
(9, 'Tees', 'Tesst', 'Test1', 'First', 2020, 2021, 12, 'john.jpg', '2001-08-03', 'Negros Occidental High School', 'M', '000000003', 2, 19, '09503785131', 'tala.luna15@gmail.com', 3, 'Alijis', 'Purok 123', 'Brgy Kamatis', 'Bago City', 'Negros Occidental', '2020-10-04 01:25:15', 70, '8.000', 1, 0),
(10, 'Chaeyoung', ' ', 'Son', 'First', 2020, 2021, 12, 'CHMSC_LOGO.jpg', '2020-09-30', 'LMNHS', 'F', '12312312', 3, 17, '09772645142', 'ibayonabel@gmail.com', 2, 'Alijis', 'Km5 Purok Roadside', 'Tangub', 'Bacolod', 'Neg.Occ', '2020-10-04 09:28:57', 0, '0.000', 0, 0),
(11, 'Sampol', 'Sampel', 'Sample', 'First', 2020, 2021, 12, 'john.jpg', '2000-10-22', 'Negros Occidental High School', 'M', '000000004', 2, 20, '09503785131', 'tala.luna15@gmail.com', 3, 'Alijis', 'Purok 1234', 'Brgy Inayawan', 'Pulupandan', 'Negros Occidental', '2020-10-04 10:19:48', 0, '0.000', 0, 0),
(12, 'Test', 'Teest', 'Testing', 'First', 2020, 2021, 12, 'john.jpg', '2000-10-19', 'Our Lady of Mercy Academy', 'M', '000000005', 3, 20, '09503785131', 'tala.luna15@gmail.com', 1, 'Alijis', 'Bangga Arwit', 'Green beach', 'Pulupandan', 'Negros Occidental', '2020-10-04 14:00:04', 0, '0.000', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_applicant_score_stanine`
--

CREATE TABLE `tbl_applicant_score_stanine` (
  `ss_id` int(11) NOT NULL,
  `applicant_id` int(11) NOT NULL,
  `score` decimal(12,3) NOT NULL,
  `stanine` decimal(12,3) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_applicant_type`
--

CREATE TABLE `tbl_applicant_type` (
  `applicant_type_id` int(11) UNSIGNED NOT NULL,
  `type_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_applicant_type`
--

INSERT INTO `tbl_applicant_type` (`applicant_type_id`, `type_name`) VALUES
(2, 'Returnee'),
(3, 'Shiftee'),
(4, 'Transferee'),
(5, 'TCP'),
(9, 'Lifelong Learner (For Freshmen)'),
(10, 'Master (For Graduate School)'),
(11, 'Doctor/PhD (For Graduate School)'),
(12, 'Grade 12 Graduate (For Freshmen)');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_classification`
--

CREATE TABLE `tbl_classification` (
  `class_id` int(11) UNSIGNED NOT NULL,
  `classification_name` varchar(50) NOT NULL DEFAULT '',
  `class_code` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_classification`
--

INSERT INTO `tbl_classification` (`class_id`, `classification_name`, `class_code`) VALUES
(1, 'Dean', 'D'),
(2, 'Student', 'S'),
(11, 'Disciplinary Officer', 'DO'),
(12, 'Chairperson', 'C');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_counselling_feedback`
--

CREATE TABLE `tbl_counselling_feedback` (
  `feedback_id` int(11) NOT NULL,
  `counceling_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `action_taken` text NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_counselling_feedback`
--

INSERT INTO `tbl_counselling_feedback` (`feedback_id`, `counceling_id`, `student_id`, `action_taken`, `remarks`) VALUES
(1, 2, 8, 'suspended for 3 days', 'The student will be suspended in 3 days.'),
(2, 3, 1, 'rewsdf sdfs dfsd fsd', ' fsd fsd fsd fsd fsdf ds'),
(3, 4, 1, 'dasdas', 'asdasdas'),
(4, 5, 15, 'adssfdfsd f', 'sd fds fsd fsd fds'),
(5, 6, 14, 'test', 'test'),
(6, 8, 1, 'dasdas', 'dsadasdasdas'),
(7, 9, 5, 'sadasda', 's das das das');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_events`
--

CREATE TABLE `tbl_events` (
  `event_id` int(11) UNSIGNED NOT NULL,
  `event_name` text NOT NULL,
  `event_start_date` date NOT NULL,
  `event_end_date` date NOT NULL,
  `event_time` time NOT NULL,
  `event_added_by` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_exam_schedules`
--

CREATE TABLE `tbl_exam_schedules` (
  `sch_id` int(11) UNSIGNED NOT NULL,
  `student_id` int(11) NOT NULL,
  `sch_date` datetime NOT NULL,
  `sch_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_exam_schedules`
--

INSERT INTO `tbl_exam_schedules` (`sch_id`, `student_id`, `sch_date`, `sch_time`) VALUES
(1, 1, '2020-09-28 00:00:00', '15:00:00'),
(2, 3, '2020-10-01 00:00:00', '10:00:00'),
(3, 4, '2020-10-02 00:00:00', '10:00:00'),
(4, 5, '2020-10-02 00:00:00', '11:30:00'),
(5, 2, '2020-10-03 00:00:00', '10:30:00'),
(6, 6, '2020-10-03 00:00:00', '11:11:00'),
(7, 7, '2020-10-02 00:00:00', '12:00:00'),
(8, 8, '2020-10-05 00:00:00', '11:30:00'),
(9, 9, '2020-10-04 00:00:00', '10:30:00'),
(10, 10, '2020-10-08 00:00:00', '10:00:00'),
(11, 11, '2020-10-05 00:00:00', '10:00:00'),
(12, 12, '2020-10-05 00:00:00', '10:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_income_range`
--

CREATE TABLE `tbl_income_range` (
  `range_id` int(11) NOT NULL,
  `range_from` decimal(12,3) NOT NULL,
  `range_to` decimal(12,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_income_range`
--

INSERT INTO `tbl_income_range` (`range_id`, `range_from`, `range_to`) VALUES
(1, '0.000', '5000.000'),
(2, '5001.000', '10000.000'),
(3, '10001.000', '15000.000'),
(4, '15001.000', '20000.000'),
(5, '20001.000', '25000.000'),
(6, '25001.000', '30000.000'),
(7, '30001.000', '35000.000'),
(8, '35001.000', '40000.000'),
(9, '40001.000', '45000.000'),
(10, '45001.000', '50000.000'),
(11, '50001.000', '0.000');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_notification`
--

CREATE TABLE `tbl_notification` (
  `notif_id` int(11) NOT NULL,
  `notif_sender` int(11) NOT NULL,
  `notif_receiver` int(11) NOT NULL,
  `notif_content` text NOT NULL,
  `notif_datetime` datetime NOT NULL,
  `notif_type` varchar(5) NOT NULL DEFAULT '',
  `notif_status` int(1) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_notification`
--

INSERT INTO `tbl_notification` (`notif_id`, `notif_sender`, `notif_receiver`, `notif_content`, `notif_datetime`, `notif_type`, `notif_status`, `student_id`) VALUES
(1, 1, 1, 'You Have New Applicant (Lalisa Kim Manoban)', '2020-09-28 14:05:09', 'A', 1, 0),
(2, 2, 1, 'You Have New Applicant (Jennie Ruby Kim)', '2020-09-28 14:17:45', 'A', 1, 0),
(3, 3, 1, 'You Have New Applicant (Ji Soo Kim)', '2020-09-30 22:58:12', 'A', 1, 0),
(4, 4, 1, 'You Have New Applicant (teeessst testt test)', '2020-10-01 15:58:35', 'A', 1, 0),
(5, 5, 1, 'You Have New Applicant (teeeeeeesssssst tessssst Test)', '2020-10-02 10:18:21', 'A', 1, 0),
(6, 6, 1, 'You Have New Applicant (Christine Joy Lee Teruel)', '2020-10-02 23:23:22', 'A', 1, 0),
(7, 7, 1, 'You Have New Applicant (Jean A Rodrigo)', '2020-10-02 23:33:17', 'A', 1, 0),
(8, 8, 1, 'You Have New Applicant (Mallari Roa Steffany)', '2020-10-03 16:38:01', 'A', 1, 0),
(9, 9, 1, 'You Have New Applicant (Tees Tesst Test1)', '2020-10-04 01:25:15', 'A', 1, 0),
(10, 10, 1, 'You Have New Applicant (Chaeyoung   Son)', '2020-10-04 09:28:57', 'A', 1, 0),
(11, 11, 1, 'You Have New Applicant (Sampol Sampel Sample)', '2020-10-04 10:19:48', 'A', 1, 0),
(12, 9, 1, 'You Have New Counselling Schedule (Lalisa Manoban)', '2020-10-04 10:58:25', 'C', 1, 0),
(13, 12, 1, 'You Have New Applicant (Test Teest Testing)', '2020-10-04 14:00:04', 'A', 1, 0),
(14, 7, 1, 'You Have New Counselling Schedule (Test teest)', '2020-10-04 14:07:17', 'C', 1, 0),
(15, 1, 7, 'Received Feedback from guidance office (Student: Test teest)', '2020-10-04 14:13:02', 'F', 1, 0),
(16, 7, 1, 'You Have New Counselling Schedule (Lalisa Manoban)', '2020-10-18 22:14:20', 'C', 1, 0),
(17, 1, 7, 'Received Feedback from guidance office (Student: Lalisa Manoban)', '2020-10-18 22:17:29', 'F', 1, 0),
(18, 7, 1, 'You Have New Counselling Schedule (Lalisa Manoban)', '2020-10-18 22:25:32', 'C', 1, 0),
(19, 1, 7, 'Received Feedback from guidance office (Student: Lalisa Manoban)', '2020-10-18 22:25:59', 'F', 1, 0),
(20, 7, 1, 'You Have New Counselling Schedule (joena jean rodrigo)', '2020-10-18 22:26:58', 'C', 1, 0),
(21, 1, 7, 'Received Feedback from guidance office (Student: joena jean rodrigo)', '2020-10-18 22:27:18', 'F', 1, 0),
(22, 7, 1, 'You Have New Counselling Schedule (christine joy teruel)', '2020-10-18 22:33:12', 'C', 1, 0),
(23, 1, 7, 'Received Feedback from guidance office (Student: christine joy teruel)', '2020-10-18 22:33:45', 'F', 1, 14),
(24, 9, 1, 'You Have New Counselling Schedule (Jennie Kim)', '2020-10-18 22:34:54', 'C', 1, 0),
(25, 9, 1, 'You Have New Counselling Schedule (Lalisa Manoban)', '2020-10-18 22:36:37', 'C', 0, 0),
(26, 1, 9, 'Received Feedback from guidance office (Student: Lalisa Manoban)', '2020-10-18 22:38:22', 'F', 1, 1),
(27, 10, 1, 'You Have New Counselling Schedule (Ji Kim)', '2020-10-18 22:39:13', 'C', 1, 0),
(28, 1, 10, 'Received Feedback from guidance office (Student: Ji Kim)', '2020-10-18 22:40:14', 'F', 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_penalties`
--

CREATE TABLE `tbl_penalties` (
  `penalty_id` int(11) NOT NULL,
  `penalty_name` varchar(100) NOT NULL,
  `level` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_penalties`
--

INSERT INTO `tbl_penalties` (`penalty_id`, `penalty_name`, `level`) VALUES
(1, 'written reprimand w/ 1hr community service', '1'),
(2, 'suspension (minor - 2days, major-5days)', '2'),
(3, 'dropping/ dismissal', '3'),
(4, 'expulsion', '4');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_program`
--

CREATE TABLE `tbl_program` (
  `program_id` int(11) UNSIGNED NOT NULL,
  `program_name` varchar(100) NOT NULL DEFAULT '',
  `program_short_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_program`
--

INSERT INTO `tbl_program` (`program_id`, `program_name`, `program_short_name`) VALUES
(1, 'Bachelor of Science in Information Systems', 'BSIS'),
(2, 'Bachelor of Science in Information Technology', 'BS Info Tech'),
(3, 'Bachelor of Science in Industrial Technology', 'BSIT'),
(4, 'Bachelor of Technical Teacher Education', 'BTTE'),
(5, 'Bachelor of Science in Electronics Engineering', 'BS ECE'),
(6, 'Bachelor of Science in Computer Engineering', 'BS CPE');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_stanine`
--

CREATE TABLE `tbl_stanine` (
  `stanine_id` int(11) UNSIGNED NOT NULL,
  `stanine_val` decimal(12,3) NOT NULL,
  `program_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_stanine`
--

INSERT INTO `tbl_stanine` (`stanine_id`, `stanine_val`, `program_id`) VALUES
(1, '4.000', 1),
(2, '4.000', 2),
(3, '3.000', 3),
(4, '4.000', 4),
(5, '4.000', 5),
(6, '4.000', 6);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_students`
--

CREATE TABLE `tbl_students` (
  `student_id` int(11) NOT NULL,
  `student_fname` varchar(100) NOT NULL DEFAULT '',
  `student_mname` varchar(50) NOT NULL DEFAULT '',
  `student_lname` varchar(50) NOT NULL DEFAULT '',
  `level` int(1) NOT NULL,
  `section` varchar(10) NOT NULL,
  `course` int(11) NOT NULL,
  `sex` varchar(10) NOT NULL DEFAULT '',
  `contact_num` varchar(11) NOT NULL DEFAULT '',
  `age` int(3) NOT NULL,
  `civil_stat` varchar(10) NOT NULL DEFAULT '',
  `solo_parent` varchar(5) NOT NULL,
  `sex_orient` varchar(20) NOT NULL,
  `height` varchar(10) NOT NULL,
  `weight` varchar(10) NOT NULL,
  `birthplace` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `nationality` varchar(50) NOT NULL,
  `home_address` text NOT NULL,
  `email_address` varchar(50) NOT NULL,
  `city_address` text NOT NULL,
  `gen_ave` decimal(12,3) NOT NULL,
  `strand_course` int(11) NOT NULL,
  `religion` varchar(50) NOT NULL,
  `last_sch_att` text NOT NULL,
  `ioe_person` varchar(100) NOT NULL,
  `ioe_address` text NOT NULL,
  `ioe_contact` varchar(11) NOT NULL,
  `relationship` varchar(50) NOT NULL,
  `pmr` varchar(20) NOT NULL DEFAULT '',
  `pmr_specify` varchar(50) NOT NULL DEFAULT '',
  `deceased` varchar(2) NOT NULL DEFAULT '',
  `date_deceased` date NOT NULL,
  `cause_deceased` text NOT NULL,
  `ordinal_pos` varchar(10) NOT NULL DEFAULT '',
  `num_child` int(11) NOT NULL,
  `num_brothers` int(11) NOT NULL,
  `num_sisters` int(11) NOT NULL,
  `num_bs_employed` int(11) NOT NULL,
  `num_bs_employed_support` int(11) NOT NULL,
  `weekly_allowance` decimal(12,3) NOT NULL,
  `household_income` int(11) NOT NULL,
  `household_income_others` decimal(12,3) NOT NULL,
  `residence_school` varchar(20) NOT NULL DEFAULT '',
  `study_place` varchar(10) NOT NULL DEFAULT '',
  `study_place_specify` varchar(50) NOT NULL DEFAULT '',
  `share_room` varchar(10) NOT NULL DEFAULT '',
  `share_room_whom` varchar(50) NOT NULL DEFAULT '',
  `recurring_ailment` varchar(5) NOT NULL DEFAULT '',
  `recurring_ailment_specify` varchar(50) NOT NULL DEFAULT '',
  `accident` varchar(5) NOT NULL,
  `accident_specify` varchar(100) NOT NULL DEFAULT '',
  `psych` varchar(5) NOT NULL DEFAULT '',
  `psych_specify` text NOT NULL,
  `problem` varchar(5) NOT NULL DEFAULT '',
  `problem_specify` text NOT NULL,
  `discuss_problem` varchar(5) NOT NULL DEFAULT '',
  `student_remarks` text NOT NULL,
  `avatar` varchar(50) NOT NULL DEFAULT '',
  `is_active` int(1) NOT NULL,
  `is_archived` int(1) NOT NULL,
  `check_hereby` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_students`
--

INSERT INTO `tbl_students` (`student_id`, `student_fname`, `student_mname`, `student_lname`, `level`, `section`, `course`, `sex`, `contact_num`, `age`, `civil_stat`, `solo_parent`, `sex_orient`, `height`, `weight`, `birthplace`, `dob`, `nationality`, `home_address`, `email_address`, `city_address`, `gen_ave`, `strand_course`, `religion`, `last_sch_att`, `ioe_person`, `ioe_address`, `ioe_contact`, `relationship`, `pmr`, `pmr_specify`, `deceased`, `date_deceased`, `cause_deceased`, `ordinal_pos`, `num_child`, `num_brothers`, `num_sisters`, `num_bs_employed`, `num_bs_employed_support`, `weekly_allowance`, `household_income`, `household_income_others`, `residence_school`, `study_place`, `study_place_specify`, `share_room`, `share_room_whom`, `recurring_ailment`, `recurring_ailment_specify`, `accident`, `accident_specify`, `psych`, `psych_specify`, `problem`, `problem_specify`, `discuss_problem`, `student_remarks`, `avatar`, `is_active`, `is_archived`, `check_hereby`) VALUES
(1, 'Lalisa', 'Kim', 'Manoban', 2, 'A', 1, 'F', '09503785131', 20, 'Single', '', 'Heterosexual', '5\'7\'\'', '118lbs', 'London', '2000-03-29', 'filipino', 'Lizares St 40 Bacolod City Neg Occ', 'lilimanoban94@gmail.com', 'Bacolod City', '89.110', 3, 'Christian', 'Seoul International University', 'Park Bo Gum', 'Lizares St., Bacolod City', '09101024113', 'Mother', 'MLT', '', '', '0000-00-00', '', '3rd', 3, 1, 1, 2, 2, '1000.000', 5, '0.000', 'FM', 'Yes', '', 'No', '', 'No', '', 'No', '', '', '', '', '', '', '', 'black-pink-lisa.jpg', 0, 0, 0),
(4, 'Jennie', 'Ruby', 'Kim', 1, 'A', 3, 'F', '09385250309', 24, 'Single', 'No', 'Heterosexual', '5\'4', '100lbs', 'Kabankalan City', '1996-01-16', 'Filipino', 'Araneta 19 Bacolod City Neg Occ', 'miracle.reign101@gmail.com', 'Bacolod City', '87.050', 2, 'Pentecostal', 'ACG Parnell College', 'Lili Kim', 'Araneta 19 Bacolod City Neg Occ', '09112345879', 'Mother', 'SP', '', '', '0000-00-00', '', '1st', 4, 2, 1, 2, 2, '600.000', 5, '0.000', 'BH', 'Yes', '', 'Yes', '', 'No', '', 'No', '', '', '', 'No', '', 'No', '', 'jennie.jpg', 0, 0, 0),
(5, 'Ji', 'Soo', 'Kim', 1, 'B', 2, 'F', '09503785131', 25, 'Married', 'No', 'Heterosexual', '5\'6', '100lbs', 'La Carlota City', '1995-01-09', 'Filipino', '12 St., Cabatangan, Neg Occ', 'lilimanoban94@gmail.com', 'La Carlota City', '90.450', 3, 'Roman Catholic', 'Seoul International University', 'Joey Kim', '12 St., Cabatangan, La Carlota, Neg Occ', '09887945612', 'Husband', 'MLT', '', '', '0000-00-00', '', '2nd', 3, 0, 2, 1, 1, '600.000', 3, '0.000', 'R', 'Yes', '', 'Yes', 'Spouse', 'No', '', 'No', '', '', '', 'No', '', 'No', '', 'jisoo.jpg', 0, 0, 1),
(6, 'teeessst', 'testt', 'test', 3, 'B', 3, 'M', '09503785131', 25, 'Single', 'No', 'Gay', '5\'4\'\'', '116lbs.', 'Mandaue, Cebu City', '1994-10-22', 'Filipino', '123 espanya Isabela City Neg Occ', 'miracle.reign101@gmail.com', 'Isabela City', '88.060', 2, 'Jehovah Witnesses', 'BCNHS', 'Ma. Elena Test', 'Isabela city', '09101024113', 'Mother', 'MLT', '', '', '0000-00-00', '', '1st', 1, 0, 0, 0, 0, '2000.000', 9, '0.000', 'FM', 'Yes', '', 'No', '', 'No', '', 'No', '', '', '', 'No', '', 'No', '', 'logo.jpg', 0, 0, 1),
(7, 'teeeeeeesssssst', 'tessssst', 'Test', 1, 'A', 2, 'F', '09503785131', 20, 'Single', 'No', 'Heterosexual', '5\'6\'\'', '120lbs.', 'Pontevedra', '2000-02-12', 'Filipino', '123 4 escalante neg occ', 'miracle.reign101@gmail.com', 'escalante', '90.000', 2, 'Born Again', 'BCNHS', 'Merlinda One', 'Pontevedra', '09101011324', 'Mother', 'MLT', '', '', '0000-00-00', '', '2nd', 2, 1, 0, 1, 1, '1500.000', 5, '0.000', 'FM', 'Yes', '', 'No', '', 'No', '', 'No', '', '', '', 'No', '', 'No', '', 'logo1.jpg', 0, 0, 1),
(8, 'Test', 'test', 'teest', 1, 'C', 1, '', '', 0, '', '', '', '', '', '', '0000-00-00', '', '', '', '', '0.000', 0, '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', 0, 0, 0, 0, 0, '0.000', 0, '0.000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0),
(9, 'Test 2', 'test', 'teest', 1, 'B', 1, '', '', 0, '', '', '', '', '', '', '0000-00-00', '', '', '', '', '0.000', 0, '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', 0, 0, 0, 0, 0, '0.000', 0, '0.000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0),
(10, 'Test 3', 'test', 'teest', 1, 'C', 1, '', '', 0, '', '', '', '', '', '', '0000-00-00', '', '', '', '', '0.000', 0, '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', 0, 0, 0, 0, 0, '0.000', 0, '0.000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0),
(11, 'Test 4', 'test', 'teest', 1, 'A', 1, '', '', 0, '', '', '', '', '', '', '0000-00-00', '', '', '', '', '0.000', 0, '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', 0, 0, 0, 0, 0, '0.000', 0, '0.000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0),
(12, 'Test 5', 'test', 'teest', 1, 'B', 1, '', '', 0, '', '', '', '', '', '', '0000-00-00', '', '', '', '', '0.000', 0, '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', 0, 0, 0, 0, 0, '0.000', 0, '0.000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0),
(13, 'Test 6', 'test', 'teest', 1, 'C', 1, '', '', 0, '', '', '', '', '', '', '0000-00-00', '', '', '', '', '0.000', 0, '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', 0, 0, 0, 0, 0, '0.000', 0, '0.000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0),
(14, 'christine joy', 'lee', 'teruel', 4, 'B', 1, '', '', 0, '', '', '', '', '', '', '0000-00-00', '', '', '', '', '0.000', 0, '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', 0, 0, 0, 0, 0, '0.000', 0, '0.000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0),
(15, 'joena jean', 'abelarde', 'rodrigo', 4, 'B', 1, '', '', 0, '', '', '', '', '', '', '0000-00-00', '', '', '', '', '0.000', 0, '', '', '', '', '', '', '', '', '', '0000-00-00', '', '', 0, 0, 0, 0, 0, '0.000', 0, '0.000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0),
(16, 'Christine Joy', 'Lee', 'Teruel', 0, '', 0, 'F', '09101024113', 25, '', '', '', '', '', '', '1994-05-02', '', 'Deca Homes Cabog Bacolod City Neg Occ', 'rjoenajean@yahoo.com', 'Bacolod City', '0.000', 3, '', 'Seoul International University', '', '', '', '', '', '', '', '0000-00-00', '', '', 0, 0, 0, 0, 0, '0.000', 0, '0.000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '2.jpg.png', 0, 0, 0),
(17, 'Jean', 'A', 'Rodrigo', 0, '', 0, 'F', '09101024113', 22, '', '', '', '', '', '', '1998-07-08', '', 'SouthBay Busay Bago City Neg Occ', 'rjoenajean@yahoo.com', 'Bago City', '0.000', 3, '', 'Seoul International University', '', '', '', '', '', '', '', '0000-00-00', '', '', 0, 0, 0, 0, 0, '0.000', 0, '0.000', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '7.jpg.png', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_counceling`
--

CREATE TABLE `tbl_student_counceling` (
  `sc_id` int(11) NOT NULL,
  `sc_date` date NOT NULL,
  `sc_time` time NOT NULL,
  `added_by` int(11) NOT NULL,
  `concern` int(11) NOT NULL,
  `remarks` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_counceling`
--

INSERT INTO `tbl_student_counceling` (`sc_id`, `sc_date`, `sc_time`, `added_by`, `concern`, `remarks`) VALUES
(1, '2020-10-05', '13:00:00', 9, 0, ''),
(2, '2020-10-06', '10:00:00', 7, 0, ''),
(3, '2020-10-20', '10:00:00', 7, 2, ''),
(4, '2020-10-21', '10:00:00', 7, 12, ''),
(5, '2020-10-23', '10:00:00', 7, 13, ''),
(6, '2020-10-26', '10:00:00', 7, 14, ''),
(7, '2020-10-27', '10:00:00', 9, 0, ''),
(8, '2020-10-27', '22:00:00', 9, 14, ''),
(9, '2020-10-30', '10:00:00', 10, 4, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_counceling_details`
--

CREATE TABLE `tbl_student_counceling_details` (
  `sc_detail_id` int(11) NOT NULL,
  `sc_header_id` int(11) NOT NULL,
  `sc_st_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_counceling_details`
--

INSERT INTO `tbl_student_counceling_details` (`sc_detail_id`, `sc_header_id`, `sc_st_id`) VALUES
(1, 1, 1),
(2, 2, 8),
(3, 3, 1),
(4, 4, 1),
(5, 5, 15),
(6, 6, 14),
(7, 7, 4),
(8, 8, 1),
(9, 9, 5);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_deceased_member`
--

CREATE TABLE `tbl_student_deceased_member` (
  `deceased_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `member` varchar(5) NOT NULL DEFAULT '',
  `date` date NOT NULL,
  `reason` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_deceased_member`
--

INSERT INTO `tbl_student_deceased_member` (`deceased_id`, `student_id`, `member`, `date`, `reason`) VALUES
(1, 4, '2', '2020-05-05', 'Cardiac Arrest'),
(2, 5, '1', '2010-04-17', 'Colon Cancer'),
(3, 5, '2', '2015-06-05', 'Cardiac Arrest ');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_educational_background`
--

CREATE TABLE `tbl_student_educational_background` (
  `seb_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `level` int(5) NOT NULL,
  `school` text NOT NULL,
  `year_covered` varchar(20) NOT NULL DEFAULT '',
  `public_private` varchar(20) NOT NULL DEFAULT '',
  `honor_received` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_educational_background`
--

INSERT INTO `tbl_student_educational_background` (`seb_id`, `student_id`, `level`, `school`, `year_covered`, `public_private`, `honor_received`) VALUES
(1, 1, 0, '', '', '', ''),
(2, 1, 3, 'N O H S', '2010-2014', '', ''),
(3, 1, 4, 'Sum ag E S', '2004-2010', '', ''),
(4, 4, 0, '', '', '', ''),
(5, 4, 3, 'St. Joseph La Salle', '2008-2012', 'private', ''),
(6, 4, 4, 'St. Joseph La Salle', '2002-2008', 'private', ''),
(7, 5, 0, '', '', '', ''),
(8, 5, 3, 'La Carlota City National High School', '2007-2011', 'public', 'With Honors'),
(9, 5, 4, 'La Carlota Elementary School', '2001-2007', 'public', 'Salutatorian'),
(10, 6, 0, '', '', '', ''),
(11, 6, 3, 'Bcolod City National High School', '2015-2019', 'public', 'with highest Honor'),
(12, 6, 4, 'Rizal Elementary School', '2010-2015', 'public', 'Valedictorian'),
(13, 7, 0, '', '', '', ''),
(14, 7, 3, 'Pontevedra NHS', '2019-2015', 'Public', ''),
(15, 7, 4, 'Pontevedra ES', '2015-2011', 'Public', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_family_background`
--

CREATE TABLE `tbl_student_family_background` (
  `family_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `m_f` varchar(10) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `contact` varchar(20) NOT NULL DEFAULT '',
  `birthplace` varchar(100) NOT NULL DEFAULT '',
  `religion` varchar(50) NOT NULL DEFAULT '',
  `nationality` varchar(50) NOT NULL DEFAULT '',
  `l_d` varchar(20) NOT NULL DEFAULT '',
  `educ_attainment` varchar(50) NOT NULL DEFAULT '',
  `occupation` varchar(100) NOT NULL DEFAULT '',
  `name_of_employer` varchar(100) NOT NULL DEFAULT '',
  `address_of_employer` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_family_background`
--

INSERT INTO `tbl_student_family_background` (`family_id`, `student_id`, `m_f`, `name`, `contact`, `birthplace`, `religion`, `nationality`, `l_d`, `educ_attainment`, `occupation`, `name_of_employer`, `address_of_employer`) VALUES
(1, 1, '1', 'Mona Lisa Manoban', '09121312312', 'London city', 'Christian', 'Filipino', 'Living', 'College', 'Housewife', '', ''),
(2, 1, '2', 'Aristotle\' Manoban', '09876567876', 'Newyork City', 'Christian', 'Filipino', 'Living', 'College', 'Chief', '', ''),
(3, 4, '1', 'Lili Kim', '09123645789', 'Kanlaon City', 'Pentecostal', 'Filipino', 'Living', 'College Level', 'Housewife', '', ''),
(4, 4, '2', 'Andrew Kim', '09152356897', 'Hinobaan City', 'Roman Catholic', 'Filipino', 'Deceased', 'College Level', '', '', ''),
(5, 5, '1', 'Jana Kim', '', 'La Carlota City', 'Jehovah\'s Witnesses', 'Filipino', 'Deceased', 'College Level', '', '', ''),
(6, 5, '2', 'Leonard Kim', '', 'La Castellana City', 'Jehovah\'s Witnesses', 'Filipino', 'Deceased', 'College Level', '', '', ''),
(7, 6, '1', 'Ma. Elena Test', '09121344676', 'Seoul Korea', 'Jehovah\'s Witnesses', 'Filipino', 'Living', 'College', 'Call Center Agent', 'concentrix', 'bacolod city'),
(8, 6, '2', 'Little One Test', '09236746722', 'Seoul Korea', 'Jehovah\'s Witnesses', 'Filipino', 'Living', 'College', 'Call Center Agent', 'iqor', 'bacolod city'),
(9, 7, '1', 'Merlinda One', '09128127121', 'Thailand', 'Christian', 'Filipino', 'Living', 'College', 'Housewife', '', ''),
(10, 7, '2', 'Merlindo One', '09478573452', 'Malaysia', 'Christian', 'Filipino', 'Living', 'College', 'Call Center Agent', 'Concentrix', 'Bacolod City');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_finance_schooling`
--

CREATE TABLE `tbl_student_finance_schooling` (
  `fs_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `financer` varchar(50) NOT NULL DEFAULT '',
  `financer_specify` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_finance_schooling`
--

INSERT INTO `tbl_student_finance_schooling` (`fs_id`, `student_id`, `financer`, `financer_specify`) VALUES
(1, 1, 'Parents', ''),
(2, 4, 'Brother/Sister', ''),
(3, 5, 'Spouse', ''),
(4, 5, '', ''),
(5, 6, 'Parents', ''),
(6, 7, 'Parents', '');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_health`
--

CREATE TABLE `tbl_student_health` (
  `health_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `health` varchar(50) NOT NULL DEFAULT '',
  `heath_specify` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_health`
--

INSERT INTO `tbl_student_health` (`health_id`, `student_id`, `health`, `heath_specify`) VALUES
(1, 4, 'Vision', 'Near Sighted'),
(2, 5, 'Vision', 'Far sighted');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_interested_org`
--

CREATE TABLE `tbl_student_interested_org` (
  `io_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `org` varchar(20) NOT NULL DEFAULT '',
  `org_specify` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_interested_org`
--

INSERT INTO `tbl_student_interested_org` (`io_id`, `student_id`, `org`, `org_specify`) VALUES
(1, 5, 'Sports', 'Volleyball');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_interest_hobbies`
--

CREATE TABLE `tbl_student_interest_hobbies` (
  `ih_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `cat` varchar(2) NOT NULL DEFAULT '',
  `number` int(1) NOT NULL,
  `ih_values` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_interest_hobbies`
--

INSERT INTO `tbl_student_interest_hobbies` (`ih_id`, `student_id`, `cat`, `number`, `ih_values`) VALUES
(1, 1, 'A', 1, ''),
(2, 1, 'A', 2, ''),
(3, 1, 'A', 3, ''),
(4, 1, 'E', 1, ''),
(5, 4, 'A', 1, 'CAVocals'),
(6, 4, 'A', 2, 'English'),
(7, 4, 'A', 3, 'Math'),
(8, 4, 'E', 1, 'Singing,Dancing,Rap'),
(9, 5, 'A', 1, 'CAPA'),
(10, 5, 'A', 2, 'Science'),
(11, 5, 'A', 3, 'English'),
(12, 5, 'E', 1, 'Singing,Dancing'),
(13, 6, 'A', 1, 'Rosary club, Math Club, English Club'),
(14, 6, 'A', 2, 'Math,Science, English'),
(15, 6, 'A', 3, 'History'),
(16, 6, 'E', 1, 'Playing Chess'),
(17, 7, 'A', 1, 'Chess Club, Rotary club'),
(18, 7, 'A', 2, 'Science, Math'),
(19, 7, 'A', 3, 'History'),
(20, 7, 'E', 1, 'Singing, Dancing');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_psych_test`
--

CREATE TABLE `tbl_student_psych_test` (
  `test_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `test_count` int(11) NOT NULL,
  `date` date NOT NULL,
  `test_name` varchar(100) NOT NULL DEFAULT '',
  `result` text NOT NULL,
  `interpretation` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_siblings`
--

CREATE TABLE `tbl_student_siblings` (
  `sibling_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `sequence` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_student_siblings`
--

INSERT INTO `tbl_student_siblings` (`sibling_id`, `student_id`, `name`, `sequence`) VALUES
(1, 1, 'Jenny Manoban', 1),
(2, 1, 'Leonardo Manoban', 2),
(3, 4, 'Jerry Kim', 1),
(4, 4, 'Ainnie Kim', 2),
(5, 4, 'Miguel Kim', 3),
(6, 5, 'Rose Kim', 1),
(7, 5, 'Jelly Kim', 2),
(8, 7, 'Kim hyun One', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_track_and_strand`
--

CREATE TABLE `tbl_track_and_strand` (
  `ts_id` int(11) UNSIGNED NOT NULL,
  `ts_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_track_and_strand`
--

INSERT INTO `tbl_track_and_strand` (`ts_id`, `ts_name`) VALUES
(1, 'BAM'),
(2, 'HESS'),
(3, 'STEM');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(100) NOT NULL DEFAULT '',
  `username` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `email_add` varchar(50) NOT NULL,
  `category_id` int(11) NOT NULL,
  `program_id` int(11) NOT NULL,
  `avatar` varchar(50) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `name`, `username`, `password`, `email_add`, `category_id`, `program_id`, `avatar`, `is_active`) VALUES
(1, 'Guidance', 'guidance', 'g', '', 0, 0, '', 1),
(7, 'chairperson1', 'chairperson1', '12345', '', 12, 1, '', 0),
(8, 'chairperson2', 'chairperson2', '12345', '', 12, 2, '', 0),
(9, 'disciplinary officer', 'DO', '00000', '', 11, 0, '', 0),
(10, 'dean alijis', 'dean', '12345', '', 1, 0, '', 0),
(11, 'chairperson3', 'chairperson3', '12345', '', 12, 3, '', 0),
(12, 'chairperson4', 'chairperson4', '12345', 'lilimanoban@gmail.com', 12, 5, '', 0),
(13, 'chairperson5', 'chairperson5', '12345', 'tala.luna15@gmail.com', 12, 6, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_violation`
--

CREATE TABLE `tbl_violation` (
  `violation_id` int(11) UNSIGNED NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_violation`
--

INSERT INTO `tbl_violation` (`violation_id`, `name`) VALUES
(2, 'Improper wearing /mutilation or tampering of ID.'),
(3, 'Improper wearing of school uniform.'),
(4, 'Creating alarm and scandal inside the school premises.'),
(5, 'Discourteous to school official, teachers and fellow students.'),
(6, 'Using vulgar or abusive language or doing rude acts directed against others.'),
(7, 'Disrespect and disobedience in all policies, order and instruction of the school.'),
(8, 'Gambling within the school and its immediate vicinity.'),
(9, 'Fighting, violence-oriented fraternities, drug abuse and trafficking, immortality and related activities.'),
(10, 'Bringing deadly weapon and indecent materials inside the campus.'),
(11, 'Engaging in drinking heavy liquor and/ or entering the school campus under the influence of the same.'),
(12, 'Smoking inside the school.'),
(13, 'Theft of school properties.'),
(14, 'Wearing earrings (for male) inside the school premises.'),
(15, 'Sporting of unclean and/or indecent hair for male.');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_violators`
--

CREATE TABLE `tbl_violators` (
  `violator_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL,
  `violation` int(11) NOT NULL,
  `penalty` int(11) NOT NULL,
  `event_date` datetime NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_violators`
--

INSERT INTO `tbl_violators` (`violator_id`, `student_id`, `violation`, `penalty`, `event_date`, `date_added`) VALUES
(1, 4, 2, 1, '2020-10-04 00:00:00', '2020-10-04 10:57:03'),
(2, 7, 3, 1, '2020-10-02 00:00:00', '2020-10-04 14:32:34'),
(3, 4, 2, 1, '2020-10-05 00:00:00', '2020-10-04 17:32:58'),
(4, 5, 4, 2, '2020-10-01 00:00:00', '2020-10-04 17:54:00'),
(5, 6, 12, 1, '2020-10-02 00:00:00', '2020-10-04 17:54:39'),
(6, 9, 3, 1, '2020-10-03 00:00:00', '2020-10-04 17:55:22'),
(7, 4, 2, 3, '2020-10-03 00:00:00', '2020-10-04 20:55:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_applicants`
--
ALTER TABLE `tbl_applicants`
  ADD PRIMARY KEY (`applicant_id`);

--
-- Indexes for table `tbl_applicant_score_stanine`
--
ALTER TABLE `tbl_applicant_score_stanine`
  ADD PRIMARY KEY (`ss_id`);

--
-- Indexes for table `tbl_applicant_type`
--
ALTER TABLE `tbl_applicant_type`
  ADD PRIMARY KEY (`applicant_type_id`);

--
-- Indexes for table `tbl_classification`
--
ALTER TABLE `tbl_classification`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `tbl_counselling_feedback`
--
ALTER TABLE `tbl_counselling_feedback`
  ADD PRIMARY KEY (`feedback_id`);

--
-- Indexes for table `tbl_events`
--
ALTER TABLE `tbl_events`
  ADD PRIMARY KEY (`event_id`);

--
-- Indexes for table `tbl_exam_schedules`
--
ALTER TABLE `tbl_exam_schedules`
  ADD PRIMARY KEY (`sch_id`);

--
-- Indexes for table `tbl_income_range`
--
ALTER TABLE `tbl_income_range`
  ADD PRIMARY KEY (`range_id`);

--
-- Indexes for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  ADD PRIMARY KEY (`notif_id`);

--
-- Indexes for table `tbl_penalties`
--
ALTER TABLE `tbl_penalties`
  ADD PRIMARY KEY (`penalty_id`);

--
-- Indexes for table `tbl_program`
--
ALTER TABLE `tbl_program`
  ADD PRIMARY KEY (`program_id`);

--
-- Indexes for table `tbl_stanine`
--
ALTER TABLE `tbl_stanine`
  ADD PRIMARY KEY (`stanine_id`);

--
-- Indexes for table `tbl_students`
--
ALTER TABLE `tbl_students`
  ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `tbl_student_counceling`
--
ALTER TABLE `tbl_student_counceling`
  ADD PRIMARY KEY (`sc_id`);

--
-- Indexes for table `tbl_student_counceling_details`
--
ALTER TABLE `tbl_student_counceling_details`
  ADD PRIMARY KEY (`sc_detail_id`);

--
-- Indexes for table `tbl_student_deceased_member`
--
ALTER TABLE `tbl_student_deceased_member`
  ADD PRIMARY KEY (`deceased_id`);

--
-- Indexes for table `tbl_student_educational_background`
--
ALTER TABLE `tbl_student_educational_background`
  ADD PRIMARY KEY (`seb_id`);

--
-- Indexes for table `tbl_student_family_background`
--
ALTER TABLE `tbl_student_family_background`
  ADD PRIMARY KEY (`family_id`);

--
-- Indexes for table `tbl_student_finance_schooling`
--
ALTER TABLE `tbl_student_finance_schooling`
  ADD PRIMARY KEY (`fs_id`);

--
-- Indexes for table `tbl_student_health`
--
ALTER TABLE `tbl_student_health`
  ADD PRIMARY KEY (`health_id`);

--
-- Indexes for table `tbl_student_interested_org`
--
ALTER TABLE `tbl_student_interested_org`
  ADD PRIMARY KEY (`io_id`);

--
-- Indexes for table `tbl_student_interest_hobbies`
--
ALTER TABLE `tbl_student_interest_hobbies`
  ADD PRIMARY KEY (`ih_id`);

--
-- Indexes for table `tbl_student_psych_test`
--
ALTER TABLE `tbl_student_psych_test`
  ADD PRIMARY KEY (`test_id`);

--
-- Indexes for table `tbl_student_siblings`
--
ALTER TABLE `tbl_student_siblings`
  ADD PRIMARY KEY (`sibling_id`);

--
-- Indexes for table `tbl_track_and_strand`
--
ALTER TABLE `tbl_track_and_strand`
  ADD PRIMARY KEY (`ts_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_violation`
--
ALTER TABLE `tbl_violation`
  ADD PRIMARY KEY (`violation_id`);

--
-- Indexes for table `tbl_violators`
--
ALTER TABLE `tbl_violators`
  ADD PRIMARY KEY (`violator_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_applicants`
--
ALTER TABLE `tbl_applicants`
  MODIFY `applicant_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_applicant_score_stanine`
--
ALTER TABLE `tbl_applicant_score_stanine`
  MODIFY `ss_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_applicant_type`
--
ALTER TABLE `tbl_applicant_type`
  MODIFY `applicant_type_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_classification`
--
ALTER TABLE `tbl_classification`
  MODIFY `class_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_counselling_feedback`
--
ALTER TABLE `tbl_counselling_feedback`
  MODIFY `feedback_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tbl_events`
--
ALTER TABLE `tbl_events`
  MODIFY `event_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_exam_schedules`
--
ALTER TABLE `tbl_exam_schedules`
  MODIFY `sch_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tbl_income_range`
--
ALTER TABLE `tbl_income_range`
  MODIFY `range_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tbl_notification`
--
ALTER TABLE `tbl_notification`
  MODIFY `notif_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `tbl_penalties`
--
ALTER TABLE `tbl_penalties`
  MODIFY `penalty_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_program`
--
ALTER TABLE `tbl_program`
  MODIFY `program_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_stanine`
--
ALTER TABLE `tbl_stanine`
  MODIFY `stanine_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_students`
--
ALTER TABLE `tbl_students`
  MODIFY `student_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tbl_student_counceling`
--
ALTER TABLE `tbl_student_counceling`
  MODIFY `sc_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_student_counceling_details`
--
ALTER TABLE `tbl_student_counceling_details`
  MODIFY `sc_detail_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tbl_student_deceased_member`
--
ALTER TABLE `tbl_student_deceased_member`
  MODIFY `deceased_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_student_educational_background`
--
ALTER TABLE `tbl_student_educational_background`
  MODIFY `seb_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_student_family_background`
--
ALTER TABLE `tbl_student_family_background`
  MODIFY `family_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tbl_student_finance_schooling`
--
ALTER TABLE `tbl_student_finance_schooling`
  MODIFY `fs_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tbl_student_health`
--
ALTER TABLE `tbl_student_health`
  MODIFY `health_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_student_interested_org`
--
ALTER TABLE `tbl_student_interested_org`
  MODIFY `io_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_student_interest_hobbies`
--
ALTER TABLE `tbl_student_interest_hobbies`
  MODIFY `ih_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_student_psych_test`
--
ALTER TABLE `tbl_student_psych_test`
  MODIFY `test_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tbl_student_siblings`
--
ALTER TABLE `tbl_student_siblings`
  MODIFY `sibling_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_track_and_strand`
--
ALTER TABLE `tbl_track_and_strand`
  MODIFY `ts_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `tbl_violation`
--
ALTER TABLE `tbl_violation`
  MODIFY `violation_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tbl_violators`
--
ALTER TABLE `tbl_violators`
  MODIFY `violator_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
