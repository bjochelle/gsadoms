<?php 
include 'core/config.php';
if(isset($_SESSION['user_id'])){
  header("Location: index.php");
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Login | CHMSC</title>

    <!-- Fontfaces CSS-->
    <link href="assets/css/font-face.css" rel="stylesheet" media="all">
    <link href="assets/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="assets/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="assets/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="assets/css/animsition.min.css" rel="stylesheet" media="all">
    <link href="assets/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="assets/css/animate.css" rel="stylesheet" media="all">
    <link href="assets/css/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="assets/css/slick.css" rel="stylesheet" media="all">
    <link href="assets/css/select2.min.css" rel="stylesheet" media="all">
    <link href="assets/css/perfect-scrollbar.css" rel="stylesheet" media="all">
    <link rel="stylesheet" type="text/css" href="assets/css/sweetalert.css">
    <link href="assets/css/imagePreview.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="assets/css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <div class="test">
            <div class="container">
                <div class="login-wrap">
                    <div class="login-content">
                        <div class="login-logo">
                                <img src="assets/images/chmsc.png" alt="CHMSC-Alijis Office of the Guidance Services" style="width: 100%;">
                                <h5>Guidance Services and Discipline Office Management System</h5>
                                
                                <p> Log in to start a session</p>
                        </div>
                        <div class="login-form">
                            <form id="loginForm" method='POST'>
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input class="au-input au-input--full" autofocus type="text" id='username' name="username" placeholder="Username" required="" autocomplete='off' >
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input class="au-input au-input--full" type="password" id='password' name="password" placeholder="Password" required>
                                    </div>
                                    <div class='form-group'>
                                        <button class="btn btn-success btn-block" style='margin-top: 10px;' type="submit" name="submit" id="btn_sign_in"><span class='fas fa-check-circle'></span> Sign in</button>
                                    </div>
                        </div>
                        <span class=''><a href='#' onclick='forgotPassword()'> Forgot Password?</a></span>
                            </form>
                            <div class="register-link">
                                <p>
                                    Application for Student <span class='fas fa-hand-o-right'></span>
                                    <a href="#" onclick='generateRegistrationForm()'>Click Here</a>
                                </p>
                            </div>
                            <div id='showAlert'>
                        
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalFP" tabindex="-1" role="dialog" aria-labelledby="modalFPLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><span class='fas fa-check-circle'></span> Retrieve Password</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
            <div id='cont_wrap'>
                <div class="form-group">
                    <div class="input-group">
                        <div class="input-group-addon">Email: </div>
                        <input type="email" id="emailAdd" name="emailAdd" class="form-control" autocomplete='off'>
                    </div>
                </div>
            </div>
                
            </div>
            <div class="modal-footer">
                <span class="btn-group">
					<button class="btn btn-primary btn-sm" id="btn_add_type" type="button" onclick='sendEmailtoGetPassword()'><span class="fas fa-check-circle"></span> Send</button>
					<button class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fa fa-close"></span> Close</button>
				</span>
            </div>
        </div>
    </div>
</div>
<?php require 'views/modals/application_form_print_modal.php'; ?>
</body>
 <!-- Jquery JS-->
   
    <!-- Bootstrap JS-->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/popper.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="assets/js/slick.min.js">
    </script>
    <script src="assets/js/wow.min.js"></script>
    <script src="assets/js/animsition.min.js"></script>
    <script src="assets/js/bootstrap-progressbar.min.js">
    </script>
    <script src="assets/js/jquery.waypoints.min.js"></script>
    <script src="assets/js/jquery.counterup.min.js">
    </script>
    <script src="assets/js/circle-progress.min.js"></script>
    <script src="assets/js/perfect-scrollbar.js"></script>
    <script src="assets/js/Chart.bundle.min.js"></script>
    <script src="assets/js/select2.min.js"> </script>
    

    <!-- Main JS-->
    <script src="assets/js/main.js"></script>
    
<script>
        $(document).ready( function(){
            
            $("#loginForm").submit(function(e){
            e.preventDefault();
            $("#btn_sign_in").prop("disabled", true);
            $("#btn_sign_in").html("<span class='fas fa-spin fa-spinner'></span> Authenticating");
            var url = 'ajax/auth.php';
            var data = $(this).serialize();
            $.post(url , data , function(data){
                if(data == 1){
                    window.location = 'index.php?page=dashboard';
                }else{
                   setTimeout(function(){
                    $("#showAlert").html("");
                   }, 3000);
                   $("#showAlert").html("<h5 style='color:red;text-align:center;' class='animated shake alert alert-danger'><span class='fas fa-exclamation-triangle'></span> Credentials did not matched.<h5>");
                }
                $("#password").val("");
                $("#username").val("");
                $("#btn_sign_in").prop("disabled", false);
                $("#btn_sign_in").html("<span class='fas fa-check-circle'></span> Sign in");
            });
            });
        });
        function generateRegistrationForm(){
            $.post("student_registration.php",function(data){$(".test").html(data)});
            
        }
        function forgotPassword(){
            $("#modalFP").modal('show');
        }
        function sendEmailtoGetPassword(){
            var emailAdd = $("#emailAdd").val();
            $("#btn_add_type").prop("disabled", true);
            $("#btn_add_type").html("<span class='fa fa-spin fa-spinner'></span> Loading");
            $.post("ajax/sendPasswodtoEmail.php", {
                emailAdd: emailAdd
            }, function(data){
                if(data == 1){
                    $("#cont_wrap").html("<center><h5 style='color: green'> We sent it to your Email, Please Check your Email </h5></center>");

                    
                }else if(data == 2){
                    alert("Email Not Found, Please Contact Administrator");
                }else{
                    alert("Error Sending to email, Please check your internet connection");
                }

                $("#btn_add_type").prop("disabled", false);
                $("#btn_add_type").html("<span class='fa fa-check-circle'></span> Send");
                
            });
        }
    </script>

</html>